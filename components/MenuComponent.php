<?php
namespace app\components;

use yii\base\Component;
use app\modules\admin\modules\modules\models\Modules;
use app\modules\admin\modules\modules\models\ModulesSearch;
use app\modules\admin\modules\roles\models\Roles;
use app\modules\admin\modules\roles\models\RolesSearch;
use app\modules\admin\modules\roles\models\Permission;
use app\modules\admin\modules\modules\models\Menu;

class MenuComponent extends Component{
	public $content;
	public $arr = array(
  array('label' => 'Marketing', 'icon' => 'fa fa-wheelchair-alt','url' => '#','id'=>1, 'parentid'=>0, ),
  array( 'label' => 'CMS', 'icon' => 'fa fa-apple', 'url' => '#','id'=>2, 'parentid'=>1,),
  array( 'label' => 'Banner', 'icon' => 'fa fa-window-close', 'url' => ['/marketing/cms/banner'],'id'=>3, 'parentid'=>2,  ),
);

public function sortmenu(){
  $new = array();
  foreach ($this->arr as $a){
    $new[$a['parentid']][] = $a;
  
  }
  return $this->createTree($new, array($this->arr[0]));
}  

//$tree = createTree($new, array($arr[0]));


	public function init()
    {
		parent::init();
		$this->content= 'Hello Yii 2.0';
	}
	
	public function display($context=null)
    {
		if($contxnt!=null)
        {
			$this->contxnt= $content;
		}
		echo $this->context;
	}


 public function checkpermission($username,$role,$getmodules,$action)
 {
    
    $roles = Roles::find()->where(['rolename'=>$role])->One();      
    $modules = Permission::find()
            ->where(['group_user' =>$username])
            ->andWhere(['role_id' =>(String)$roles['_id']])
            ->all();
      if (($module = Modules::find()->where(['like','modulename',$getmodules])->One()) !==null) 
            {
                 //search role_id
                //echo $module['modulename'];
                  if (($roles = Roles::find()->where(['rolename'=>$role])->One()) !== null) 
                      {

                         $model = Permission::findOne(['group_user'=>$username,
                                                       'module_id'=>(String)$module->_id,
                                                       'role_id'=>(String)$roles->_id,
                                                      ]);
                   
                     //cheack permisstion action
                     switch ($action) {
                     case 'index':
                            if ($model['view']==1)
                                {
                                     return 1;
                                }
                            else
                                {
                                    return 0;
                                } 
                     break;
                        case 'view':
                            if ($model['view']==1)
                                {
                                     return 1;
                                }
                            else
                                {
                                    return 0;
                                }

                        break;
                    case 'create':
                            if ($model['create']==1)
                                {
                                    return 1;
                                }
                                else
                                {
                                    return 0;
                                } 
                        break;
                    case 'update':
                            if ($model['edit']==1)
                                {
                                     return 1;
                                }
                            else
                                {
                                    return 0;
                                }
                        break;
                    case 'delete':
                            if ($model['edit']==1)
                                {
                                     return 1;
                                }
                            else
                                {
                                    return 0;
                                }

                         break;
                       case 'export':
                             if ($model['export']==1)
                                {
                                     return 1;
                                }
                            else
                                {
                                    return 0;
                                }
                         break;
                        case 'approve':
                             if ($model['approve']==1)
                                {
                                     return 1;
                                }
                            else
                                {
                                    return 0;
                                }
                         break;
                     }
                  }

                 else
                 {
                      return 0;        
                 }
             }
             else
             {
                  return 0;        
             } 
          
 }

//  public function Querymenu($role,$username,$parentid)
//  {
      
//             $item=[];
//             $module=Modules::find()->where(["parent_id"=>(String)$parentid])->One();
//             //$this->Querymenu($role,$username,$module['_id']);
//             if($module['modulename']!=null)
//             return  $item['items'][]=['label'=>$module['modulename'],'icon'=>$module['icon'],'url'=>[$module['link']]];   
//             else
//             return $item['items'][]=['label'=>'','icon'=>'#','url'=>'#'];                  	
// }

public function Querymenu($role,$username)
 {
    
   
   if(isset($role)&&isset($username))
   {
           $Menu=[];
           $roles = Roles::find()->where(['rolename'=>$role])->One();

        if($roles!==NUll){

           $module=Modules::find()->OrderBy("num")->all();
           
           foreach ($module as $key=> $value) 
           {

             if(Permission::find()->where(['role_id'=>(String)$roles['_id'],'module_id'=>(String)$value['_id']])->One()!==NUll)
                {
                 $Menu[]=[  
                            'label'=>$value['modulename'],
                            'icon'=>$value['icon'],
                            'url'=>[$value['link']],
                            'id'=>$value['num'],
                            'parent_id'=>$value['parent_id']
                         ];     
                }

            }

            foreach ($Menu as $key => $m)
            {
                $new[$m['parent_id']][] = $m;
            }
         
            
            return $this->createTree($new, $new[0]);
        }
        else
        {
            return [];
        }
    }
    else
    {
            return [];
    }
} 
public function createTree(&$list, $parent){
$tree =[];
    foreach ($parent as $k=>$l){
        if(isset($list[$l['id']])){
            $l['items'] =$this->createTree($list, $list[$l['id']]);
        }
        $tree[] = $l;
    } 
    return $tree;
 
}

}