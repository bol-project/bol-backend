<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use app\models\Menu;
use app\modules\admin\modules\modules\models\Modules;
use app\modules\admin\modules\modules\models\ModulesSearch;
use app\modules\admin\modules\roles\models\Roles;
use app\modules\admin\modules\roles\models\RolesSearch;
use app\modules\admin\modules\roles\models\Permission;

class AdminController extends \yii\web\Controller
{
    public function init()
    {
        parent::init();
 
    }

    public function beforeAction($action)
    {
        $session = Yii::$app->session;
        
        if (parent::beforeAction($action))
        {
            if (($module = Modules::find()->where(['like','modulename',$this->module->id])->One()) !==null) 
            {
                if (($roles = Roles::find()->where(['rolename'=>$session['role']])->One()) !== null) 
                {
                    $model = Permission::findOne(['group_user'=>$session['username'],
                                                  'module_id'=>(String)$module->_id,
                                                  'role_id'=>(String)$roles->_id,
                                                      ]);

            switch ($this->action->id) {
                     case 'index':
                            if ($model['view']==1)
                                {
                                     return true;
                                }
                            else
                                {
                                    return 0;
                                } 
                     break;
                        case 'view':
                            if ($model['view']==1)
                                {
                                     return true;
                                }
                            else
                                {
                                    return 0;
                                }

                        break;
                    case 'create':
                            if ($model['create']==1)
                                {
                                    return true;
                                }
                                else
                                {
                                    return $this->redirect(['/site/pageerror']);
                                } 
                        break;
                    case 'update':
                            if ($model['edit']==1)
                                {
                                     return true;
                                }
                            else
                                {
                                 return $this->redirect(['/site/pageerror']);
                                }
                        break;
                    case 'delete':
                            if ($model['edit']==1)
                                {
                                    return true;
                                }
                            else
                                {
                                    return $this->redirect(['/site/pageerror']);
                                }

                         break;
                       case 'export':
                             if ($model['export']==1)
                                {
                                     return true;
                                }
                            else
                                {
                                    return $this->redirect(['/site/pageerror']);
                                }
                         break;
                        case 'approve':
                             if ($model['approve']==1)
                                {
                                     return true;
                                }
                            else
                                {
                                   return $this->redirect(['/site/pageerror']);
                                }
                         break; 
                      }
                }
                else
                {
                    return $this->redirect(['/site/pageerror']);
                }                
            }  
            else
            {
                return $this->redirect(['/site/pageerror']);
            }
        } 
    }


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
}