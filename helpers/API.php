<?php

namespace app\helpers;


class API
{
      public static function uploadBusinessLogo($model, $api) {
            if(!!$model->logo){
                $file = curl_file_create($model->logo->tempName, $model->logo->type, $model->logo->name);

                $img_file = $model->logo->tempName;

                $imgData = base64_encode(file_get_contents($img_file));
              
                $str_base64 = explode(',', $imgData)[0];
                $extension_index = strrpos($model->logo->name, '.');
                $extension =  substr($model->logo->name, $extension_index+1, strlen($model->logo->name));
           
                
        
                $post = array('image'=>$file, 'extension' => $extension);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $api.'/api/businessLogos/uploadBusinessLogo');
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data', 'x-master-key: smelink1234'));
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result=curl_exec ($ch);
                curl_close ($ch);
                $logo_path = json_decode($result)->result->path;
                $logo_url = $api.$logo_path;
                return $logo_url ;

            } else {
                    return $model->getOldAttribute('logo') ;
             }
    }



    public static function uploadBusinessClaimDocument($documents, $api){
        $url_array = array();
        foreach ($documents as $document) {
                $file = curl_file_create($document->tempName, $document->type, $document->name);

                $img_file = $document->tempName;

                $imgData = base64_encode(file_get_contents($img_file));
        
                $str_base64 = explode(',', $imgData)[0];
                $extension_index = strrpos($document->name, '.');
                $extension =  substr($document->name, $extension_index+1, strlen($document->name));
        
                    
                $post = array('image'=>$file, 'extension' => $extension);
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $api.'/api/business/claim/uploadDocument');
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data', 'x-master-key: smelink1234'));
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result = curl_exec ($ch);

                // print_r($result);
                curl_close ($ch);
                $path = json_decode($result)->result->path;
                $url = $api.$path;
                array_push($url_array, $url);
          }

        return $url_array;
    }




}