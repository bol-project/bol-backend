<?php
use yii\helpers\Html;
$this->title="Error 403";
?>

      <div class="error-page">
        <h2 class="headline text-red"> 403</h2>

        <div class="error-content">
          <h3><i class="fa fa-warning text-red"></i> Forbidden.</h3>

          <p>
              You don't have permission to access [directory] on this server  
          <?= Html::a('return to dashboard.', ['/Dashboard']) ?>
          
          </p>

        </div>
        <!-- /.error-content -->
      </div>