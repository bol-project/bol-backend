<div class="content-wrapper">
    <section class="content-header">
        <?php if (isset($this->blocks['content-header'])) { ?>
            <h1><?= $this->blocks['content-header'] ?></h1>
        <?php } else { ?>
            <h1>
                <?php
                if ($this->title !== null) {
                  //echo \yii\helpers\Html::encode($this->title);
                } else {
                    echo \yii\helpers\Inflector::camel2words(
                        \yii\helpers\Inflector::id2camel($this->context->module->id)
                    );
                    echo ($this->context->module->id !== \Yii::$app->id) ? '<small>Module</small>' : '';
                } ?>
            </h1>
        <?php } ?>

    </section>

    <section class="content">
        <? //=Alert::widget() ?>

        <?php
            echo $content;

            // $session = Yii::$app->session;
        
            //  $menu=Yii::$app->menu->checkpermission($session['username'],$session['role'],$this->context->module->id,$this->context->action->id);
            //  if($menu==1)
            //  {
            //     echo $content;
            //  }
            //  else
            //  {
            //     echo $this->render('error.php');
            //  }

        ?>

      
 </section>
</div>

<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.0
    </div>
    <strong>Copyright &copy; RFL 2016-<?php echo date('Y')?>.</strong> All rights reserved.
</footer>
