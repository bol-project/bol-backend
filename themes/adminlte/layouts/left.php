<?php
use app\modules\admin\modules\modules\models\modules;
use app\modules\admin\modules\modules\models\ModulesSearch;
use app\modules\admin\modules\roles\models\Roles;
use app\modules\admin\modules\roles\models\RolesSearch;
use app\modules\admin\modules\roles\models\Permission;

$session = Yii::$app->session;

?>

<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <!-- <p><strong><?php echo $session['username']?></strong></p> -->
            </div>
        </div>
     
        <?php
            $session = Yii::$app->session;
           
        ?>
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' =>Yii::$app->menu->Querymenu($session['role'],$session['username'])
            ]
        ) ?>

    </section>

</aside>
