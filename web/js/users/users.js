    
    var api_send_actvaite_mail = getConfig().api_send_actvaite_mail;
    
   


    $('.resend').click(function(){
   
        var user_id = $(this).attr("data-value");
        // callAPISendActivateEmail(user_id);   
            $("#myModal .modal-body p").text('Do you want to resend activate email to user.');
            $('.modal-header').css({'background-color': '#3c8dbc', 'height': '100px', 'color': 'white', 'font-size': '40px', 'font-weigth': 'bold'});
            $('.modal-header').html('Please Confirm');
            $('#submit-resend').show();
            $("#myModal").show("normal", function(){
                $('#submit-resend').one('click', function(e){
                    $("#myModal").hide("slow", function(){
                        callAPISendActivateEmail(user_id);   
                    });
                    
                });
                
            });
    });


    function callAPISendActivateEmail(user_id){
       
            $.ajax({
            async: true,
            crossDomain: true,
            url: api_send_actvaite_mail,
            method: 'POST',
            headers: {
                "x-api-key": "smelink1234",
                "content-type": "application/x-www-form-urlencoded",
                "cache-control": "no-cache",
                "postman-token": "798c5794-2efe-a353-3124-fde68b2f0cbc"
            },
            data: {
                "user_id": user_id
            },
            success: function() {
                $("#myModal .modal-body p").text('Send activate email Success');
                $('.modal-header').css({'background-color': '#5cb85c', 'height': '100px', 'color': 'white', 'font-size': '40px', 'font-weigth': 'bold'});
                $('.modal-header').html('200 (OK)');
                $("#myModal").modal('show');
                $('#submit-resend').hide();
                console.log('Success');
                
            },
            error: function() {
                $("#myModal .modal-body p").text('Cannot send activate email');                
                $('.modal-header').css({'background-color': '#dd4b39', 'height': '100px', 'color': 'white', 'font-size': '40px', 'font-weigth': 'bold'});
                $('.modal-header').html('500 (Internal Server Error)');
                console.log('Erorr');
                
                
            }
        });

    }