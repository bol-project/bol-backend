	$(".add-document").click(function () {
		$(this).closest('.col-md-12').find('.input-document').click();
	});

  $('.input-document').change(function(click){
        var upload_area = $(this).closest('.upload-area');
		// upload_area.find('.add-document').remove();
       	readURL(this, upload_area);
           
    });
		
	function readURL(input, upload_area) {

        var filesAmount = input.files.length;
		for( i = 0; i < filesAmount; i++ ){
			// if (input.files && input.files[i]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					upload_area.append(function(){
						var html = '<div class="flex-center add-document relative" style="margin-right: 1.5%; margin-top: 1%; border-radius: 2px; background-image: url('+e.target.result+'); background-size: cover; background-position: 50% 50%">';
						html += '<div class="remove-document flex-center circle" onclick="removeElement(this)" style="width: 25px; height: 25px; background-color: #dbdbdb; font-size: 14px; position: absolute; top: -4px; right: -7px; font-family: Montserrat;">X<div>';
						html += '</div>';

						return html;
					});
				}
				reader.readAsDataURL(input.files[i]);
			// }
		}
	}


	function removeElement(element) {

		$(element).closest('.add-document').remove();
			
	}