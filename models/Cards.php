<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "cards".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $_user_id
 * @property mixed $_business_id
 * @property mixed $officialBusiness
 * @property mixed $officialContact
 * @property mixed $customize
 * @property mixed $viewed
 */
class Cards extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'cards'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            '_user_id',
            '_business_id',
            'officialBusiness',
            'officialContact',
            'customize',
            'viewed',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_user_id', '_business_id', 'officialBusiness', 'officialContact', 'customize', 'viewed'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            '_user_id' => 'User ID',
            '_business_id' => 'Business ID',
            'officialBusiness' => 'Official Business',
            'officialContact' => 'Official Contact',
            'customize' => 'Customize',
            'viewed' => 'Viewed',
        ];
    }
}
