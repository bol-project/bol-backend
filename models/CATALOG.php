<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "CATALOG".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $CATALOG_ID
 * @property mixed $NKEY
 * @property mixed $NAME_TH
 * @property mixed $NAME_EN
 */
class CATALOG extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'CATALOG'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'CATALOG_ID',
            'NKEY',
            'NAME_TH',
            'NAME_EN',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['CATALOG_ID', 'NKEY', 'NAME_TH', 'NAME_EN'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'CATALOG_ID' => 'Catalog  ID',
            'NKEY' => 'Nkey',
            'NAME_TH' => 'Name  Th',
            'NAME_EN' => 'Name  En',
        ];
    }
}
