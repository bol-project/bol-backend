<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "posts".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property \MongoDB\BSON\ObjectID|string $busiess_id
 * @property \MongoDB\BSON\ObjectID|string $sender_id
 * @property string $body
 * @property string $title
 * @property \MongoDB\BSON\UTCDateTime $createAt
 */
class Posts extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'posts'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'business_id',
            'sender_id',
            'body',
            'title',
            'createAt'
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'business_id' => 'Business',
            'sender_id' => 'Sender',
            'body' => 'Body',
            'title' => 'Title',
            'createAt' => 'Created At'
        ];
    }
}