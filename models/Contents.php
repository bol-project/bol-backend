<?php

namespace app\models;

use app\models\Mongodate;
use app\models\GenerateFileName;
use app\modules\marketing\config\services;
use app\modules\marketing\config\config;

use Yii;

/**
 * This is the model class for collection "contents".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $title
 * @property mixed $slug
 * @property mixed $status
 * @property mixed $image
 * @property mixed $url
 * @property mixed $content
 * @property mixed $ranking
 * @property mixed $views
 * @property mixed $likes
 * @property mixed $shares
 * @property mixed $createdAt
 * @property mixed $updatedAt
 * @property mixed $approvedAt
 * @property mixed $createdBy
 * @property mixed $updatedBy
 * @property mixed $approvedBy
 * @property mixed $effectiveDate
 * @property mixed $online
 */
class Contents extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['Content', 'contents'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'title',
            'slug',
            'status',
            'image',
            'url',
            'content',
            'ranking',
            'views',
            'likes',
            'shares',
            'createdAt',
            'updatedAt',
            'approvedAt',
            'createdBy',
            'updatedBy',
            'approvedBy',
            'effectiveDate',
            'online',
            'detail',
            'source',
            'date',
            'link',
            'upload',
            'startDate',
            'endDate',
            'submitType'
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'status' => 'Status',
            'image' => 'Image',
            'url' => 'Url',
            'content' => 'Content',
            'ranking' => 'Ranking',
            'views' => 'Views',
            'likes' => 'Likes',
            'shares' => 'Shares',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'approvedAt' => 'Approved At',
            'createdBy' => 'Created By',
            'updatedBy' => 'Updated By',
            'approvedBy' => 'Approved By',
            'effectiveDate' => 'Effective Date',
            'online' => 'Online',
            'startDate' => 'From',
            'endDate' => 'To'
        ];
    }
}
