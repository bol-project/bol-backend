<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "employees".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $status
 * @property mixed $userId
 * @property mixed $businessId
 */
class Employees extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'employees'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'status',
            'userId',
            'businessId',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'userId', 'businessId'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'status' => 'Status',
            'userId' => 'User ID',
            'businessId' => 'Business ID',
        ];
    }
}
