<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "address_mapping".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $postcode
 * @property mixed $province
 * @property mixed $district
 * @property mixed $subdistrict
 */
class AddressMapping extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'address_mapping'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'postcode',
            'province',
            'district',
            'subdistrict',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['postcode', 'province', 'district', 'subdistrict'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'postcode' => 'Postcode',
            'province' => 'Province',
            'district' => 'District',
            'subdistrict' => 'Subdistrict',
        ];
    }
}
