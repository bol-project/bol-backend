<?php

namespace app\models;

class MongoDate 
{
    public static function mongoDateToString($mongoDate, $onlyDate = false, $format = "d/m/Y H:i:s") {
        if(gettype($mongoDate) == "object" && get_class($mongoDate) == "MongoDB\BSON\UTCDateTime") {
            $format = $onlyDate ? "d/m/Y" : $format;

            $timezone = new \DateTimeZone(date_default_timezone_get());
            $offset = $timezone->getOffset(new \DateTime("now")); // Offset in seconds
            $offset = ($offset < 0 ? '+' : '-').round($offset/3600).'00'; // prints "+1100"

            $datetime = $mongoDate->toDateTime()->format(DATE_RSS);
            $datetime = str_replace("+0000", $offset, $datetime);
            // return $mongoDate->toDateTime()->format($format);
            return gmdate($format, strtotime($datetime));
        }
        else {
            echo "<script>console.warn('wrong date type');</script>";
        }
    }

    public static function now(){
        return new \MongoDB\BSON\UTCDateTime(strtotime('now') * 1000);
    }

    private function isValidTimeStamp($timestamp)
    {
        return ((string) (int) $timestamp === (string) $timestamp) 
            && ($timestamp <= PHP_INT_MAX)
            && ($timestamp >= ~PHP_INT_MAX);
    }

    public static function timestampToMongoDate($timestamp){
        if (self::isValidTimeStamp($timestamp)) {
            return new \MongoDB\BSON\UTCDateTime($timestamp * 1000);
        }
        return null;
    }
}
?>