<?php

namespace app\models;

class Date
{
    public function validateDateFormat($date)
    {
        if (preg_match("/^\d{2}\/\d{2}\/\d{4}$/", $date) && (int)explode('/', $date)[1] <= 12) {
            return true;
        }
        else {
            echo "<script>console.warn('wrong date format(dd/mm/yyy): ".$date."');</script>";
            return false;
        }
        
    }
}

?>