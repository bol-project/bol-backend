<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "links".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $cards
 */
class Links extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'links'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'cards',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cards'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'cards' => 'Cards',
        ];
    }
}
