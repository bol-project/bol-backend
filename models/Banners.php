<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "banners".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $title
 * @property mixed $status
 * @property mixed $type_id
 * @property mixed $link
 * @property mixed $content
 * @property mixed $createdAt
 * @property mixed $updatedAt
 * @property mixed $media
 * @property mixed $upload
 */
class Banners extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['Content', 'banners'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'title',
            'status',
            'type_id',
            'link',
            'content',
            'createdAt',
            'updatedAt',
            'approvedAt',
            'createdBy',
            'updatedBy',
            'approvedBy',
            'effectiveDate',
            'online',
            'media',
            'startDate',
            'endDate',
            'submitType'
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
            'type_id' => 'Type ID',
            'link' => 'Link',
            'content' => 'Content',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'media' => 'Media',
        ];
    }
}