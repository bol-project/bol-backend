<?php

namespace app\models;


use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Links;
use yii\data\ArrayDataProvider;
use app\models\MongoDate;


/**
 * UsersSearch represents the model behind the search form about `app\modules\users\models\Users`.
 */
class LinkSearch extends Links
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'cards'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Links::find()->all(); 
        
       

       
        
    
        
        // add conditions that should always apply here
     $this->load($params); 
        $newData=array(); 
        $registerDate = explode('/', $this->_id); 
    
        if(isset($registerDate[1])){
                                        
                for ($i=0;$i<count($query);$i++) 
                { 
                    $timestamp = hexdec(substr($query[$i]->_id, 0, 8)); 
                    $registerDate[4]=(int)substr($registerDate[2],6,6)+1;
                    $year=substr($registerDate[2],0,4);

                    $maxtime = strtotime($registerDate[1].'/'.$registerDate[4].'/'.$year);  
                    $mintime = strtotime($registerDate[1].'/'.$registerDate[0].'/'.$year);
      
                  
                    if ($maxtime > $timestamp && $timestamp >= $mintime) 
                    { 
                        array_push($newData,$query[$i]); 
                    } 
                }

        }else{
            $newData = $query ;
        }
    
        $dataProvider = new ArrayDataProvider([ 
            'allModels' => $newData, 
            'pagination' => [ 
                'pageSize' => 10, 
            ] 
        ]);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


   
                


        return $dataProvider;
    }
}
