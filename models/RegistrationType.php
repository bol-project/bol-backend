<?php

namespace app\models;

use Yii;

/**
 * This is the model class for collection "registration_type".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $type
 * @property mixed $name
 */
class RegistrationType extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'registration_type'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'type',
            'name',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
        ];
    }
}
