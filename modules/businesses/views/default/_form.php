<?php

//use yii\helpers\Html;
//use yii\widgets\DetailView;
use app\modules\users\models\Users;
use app\modules\claims\models\Claims;
use app\models\CATALOG;
use app\models\Cards;
use app\models\Employees;
use app\models\CompanyStatus;
use app\models\RegistrationType;
use app\models\IndustryGroup;
use app\modules\claims\controllers\RequestStatus;
use app\modules\claims\controllers\ClaimType;
use app\models\MongoDate;
use app\models\AddressMapping;
use kartik\helpers\Html;
use kartik\detail\DetailView;
use yii\bootstrap\Modal;
use kartik\widgets\FileInput;
use yii\helpers\ArrayHelper;





/* @var $this yii\web\View */
/* @var $model app\modules\businesses\models\Businesses */

$this->title = isset($model->name['th']) ? $model->name['th'] : '' ;
$this->params['breadcrumbs'][] = ['label' => 'Businesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

   

<div class="businesses-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box box-info" style="text-align: center;">
       <iframe width="70%" height="400" src="http://www.matchlink.asia/web/?businessId=<?= $model->_id ?>&language=EN" style="margin-top: 0.5%;">
            <p>Your browser does not support iframes.</p>
        </iframe>
    </div>
    <?php if(Yii::$app->session->hasFlash('alert')):?>
        <?= \yii\bootstrap\Alert::widget([
        'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
        'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
        ])?>
    <?php endif; ?>
    <div class="box box-info">
        <?php
        /*
        <div class="box-header with-border">
            
            <p>
                <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Index', ['/businesses/'], ['class'=>'btn btn-success']) ?>
            </p>

        </div>
        */
        
        ?>

        <?php
        echo DetailView::widget([
            'model' => $model,
            'condensed'=>true,
            'hover'=>true,
            'enableEditMode' => false,
            'mode'=>DetailView::MODE_VIEW,
            // 'panel'=>[
            //     'heading'=>'Business # ' . $model->_id,
            //     'type'=>DetailView::TYPE_INFO,
            // ],
            'attributes'=>[
                'columns' => 
                    //------------------------------------------------------------------ Business Name ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Summary',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    
                    [
                        'label' => 'Last Register Date',
                        'value' => call_user_func(function($model){
                                        $card = Cards::find()->orderBy(['_id' => SORT_DESC])->where(['_business_id' => $model->_id])->one();
                                        if(isset($card->_id)){
                                            $timestamp = hexdec(substr($card->_id, 0, 8));
                                            $date = MongoDate::mongoDateToString(MongoDate::timestampToMongoDate($timestamp), false);
                                            return $date;
                                        }else return null;
                                    },$model)
                    ],
                    [
                        'label' => '',
                        'value' => call_user_func(function($model){
                                        $card = Cards::find()->orderBy(['_id' => SORT_DESC])->where(['_business_id' => $model->_id])->one();
                                        if(isset($card->_id)){
                                            $user = Users::find()->where(['_id' => $card->_user_id ])->one();
                                            return isset($user->email) ? $user->email : '';
                                        }else return null;
                                    },$model)
                    ],
                    /*
                    [
                        'label' => 'Last Login',
                        'value' => call_user_func(function($model){
                                        $user = Users::find(['defaultCardId' => $model->_id])->orderBy(['lastLogedin' => SORT_DESC])->one();
                                        $date = MongoDate::mongoDateToString($user->lastLogedin, false);
                                        return $date;
                                    },$model)

                    ],
                    [
                        'label' => '',
                        'value' => call_user_func(function($model){
                                        $user = Users::find(['defaultCardId' => $model->_id])->orderBy(['lastLogedin' => SORT_DESC])->one();
                                        return $user->email;
                                    },$model)

                    ],
                    */
                    [
                        'label' => 'Number of User',
                        'format' => 'raw',
                        'value' => call_user_func(function($model){
                                        $cards = Cards::find()->where(['_business_id' => $model->_id])->all();
                                        Modal::begin([
                                            'header' => '<h3 >List of User</h3>',
                                            'headerOptions' => ['style' => 'color: white; background-color: #3c8dbc;'],
                                            'options' => ['id' => 'user-list'],
                                            'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
                                        ]);
                                        echo '<table class="table table-striped">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>#</th>';
                                                    echo '<th>Avatar</th>';
                                                    echo '<th>First Name</th>';
                                                    echo '<th>Last Name</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $i = 1;
                                                    foreach ($cards as $card) {
                                                        $users = Users::find()->where(['_id' => $card->_user_id])->one();
                                                        if($users != null){
                                                            echo '<tr>';
                                                                echo '<td>'.$i++.'</td>';
                                                                if(!!$users->avatar){
                                                                    echo '<td><img src="'.$users->avatar .'" width = "70px"/></td>';
                                                                }else{
                                                                    echo '<td><img src="/web/images/userdefault.png" width = "70px"/></td>';
                                                                    
                                                                }
                                                                if(isset($users->first_name))
                                                                    echo '<td>'.$users->first_name.'</td>';
                                                                else
                                                                    echo '<td></td>';
                                                                
                                                                if(isset($users->last_name)){
                                                                     echo '<td>'.$users->last_name.'</td>';

                                                                }else{
                                                                     echo '<td></td>';
                                                                }
                                                            echo '</tr>';

                                                        }
                                                            
                                                    }
                                            
                                            echo '</tbody>';
                                        echo '</table>';

                                        Modal::end();
                                        
                                        if(sizeof($cards) == 0){
                                            return sizeof($cards);
                                        }else {
                                             return '<a data-toggle="modal" data-target="#user-list" style="cursor: pointer; ">'.sizeof($cards).'</a>';
                                        }
                                    },$model)


                    ],
                    //------------------------------------------------------------------ Business Name ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Claim Info',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Claim Status',
                        'format'=>'raw',
                        'value' => call_user_func(function($model){
                            $claim = Claims::find()
                                ->where(['claim_type' => (int)ClaimType::$CLAIMER])
                                ->orWhere(['claim_type' => (string)ClaimType::$CLAIMER])
                                ->andWhere(['business_id' => (string)$model->_id, ])
                                ->andFilterCompare('company_registration_docs', '>0')
                                ->andFilterCompare('authorized_director_citizen_cards', '>0')   
                                ->one();
                            if(isset($claim['request_status'])){
                                if($claim && $claim['request_status'] == RequestStatus::$VERIFIED) 
                                    return '<span class="label label-success">'.RequestStatus::toString($claim['request_status']).'</span>';
                                else 
                                    return '<span class="label label-danger">'.RequestStatus::toString($claim['request_status']).'</span>';
                            }else return null;
                        },$model)

                    ],
                    [
                        'label' => 'CS Response',
                        'value' => call_user_func(function($model){
                            $claim = Claims::find()
                                ->where(['claim_type' => (int)ClaimType::$CLAIMER])
                                ->orWhere(['claim_type' => (string)ClaimType::$CLAIMER])
                                ->andWhere(['business_id' => (string)$model->_id, ])
                                ->andWhere(['request_status' => (int)RequestStatus::$VERIFIED, ])
                                ->orWhere(['request_status' => (string)RequestStatus::$VERIFIED, ])
                                ->andFilterCompare('company_registration_docs', '>0')
                                ->andFilterCompare('authorized_director_citizen_cards', '>0')   
                                ->one();
                            if(isset($claim['remarks']['agent_id'])){
                                return $claim['remarks']['agent_id'];
                            }else return 'null';
                        },$model)

                    ],
                    [
                        'label' => 'Claimer',
                        'value' => call_user_func(function($model){
                                $claim = Claims::find()
                                    ->where(['claim_type' => (int)ClaimType::$CLAIMER])
                                    ->orWhere(['claim_type' => (string)ClaimType::$CLAIMER])
                                    ->andWhere(['business_id' => (string)$model->_id, ])
                                    ->andWhere(['request_status' => (int)RequestStatus::$VERIFIED, ])
                                    ->orWhere(['request_status' => (string)RequestStatus::$VERIFIED, ])    
                                    ->andFilterCompare('company_registration_docs', '>0')
                                    ->andFilterCompare('authorized_director_citizen_cards', '>0')
                                    ->one();
                                $user = Users::find()->where(['_id' => $claim['user_id']])->one();
                                if(isset($user['first_name']) && isset($user['last_name'])){
                                    if($user) 
                                        return $user['first_name'].' '.$user['last_name'];
                                    else 
                                        return null;
                                }else return null;
                            },$model)
                    ],
                    [
                        'label' => 'Number of Sub Claimer',
                        'format' => 'raw',
                        'value' => call_user_func(function($model){
                                $claims = Claims::find()
                                    ->where(['claim_type' => (int)ClaimType::$SUB_CLAIMER])
                                    ->orWhere(['claim_type' => (string)ClaimType::$SUB_CLAIMER])
                                    ->andWhere(['business_id' => (string)$model->_id])    
                                    ->andWhere(['request_status' => 1])    
                                    ->all();
                                   
                                    Modal::begin([
                                        'header' => '<h3 >List of Sub Claimer</h3>',
                                        'headerOptions' => ['style' => 'color: white; background-color: #3c8dbc;'],
                                        'options' => ['id' => 'claimer-list'],
                                        'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
                                    ]);
                                    echo '<table class="table table-striped">';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>#</th>';                                            
                                                echo '<th>Avatar</th>';
                                                echo '<th>First Name</th>';
                                                echo '<th>Last Name</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                         $i = 1;                                    
                                            foreach ($claims as $claim) {
                                                $user_id = new \MongoDB\BSON\ObjectID($claim->user_id); 
                                                $users = Users::find()->where(['_id' => $user_id])->one();
                                                if($users != null & $user_id != null){
                                                    echo '<tr>';
                                                        echo '<td>'.$i++.'</td>';
                                                        if(!!$users->avatar){
                                                            echo '<td><img src="'.$users->avatar .'" width = "70px"/></td>';
                                                        }else{
                                                            echo '<td><img src="/web/images/userdefault.png" width = "70px"/></td>';
                                                            
                                                        }
                                                        if(isset($users->first_name))
                                                            echo '<td>'.$users->first_name.'</td>';
                                                        else
                                                            echo '<td></td>';
                                                        
                                                        if(isset($users->last_name)){
                                                                echo '<td>'.$users->last_name.'</td>';

                                                        }else{
                                                                echo '<td></td>';
                                                        }
                                                    echo '</tr>';

                                                }
                                                    
                                            }
                                        
                                        
                                        echo '</tbody>';
                                    echo '</table>';

                                    Modal::end();
                                    
                                            
                                    if($claims) {
                                        return '<a data-toggle="modal" data-target="#claimer-list" style="cursor: pointer; ">'.sizeof($claims).'</a>';
                                    }else{
                                        return 0;
                                    }
                                   
                              
                            },$model)

                    ],
                    [
                        'label' => 'Number of Employee',
                        'format' => 'raw',
                        'value' => call_user_func(function($model){
                                $employees = Employees::find()
                                    ->where(['businessId' => (string)$model->_id, 'status' => 1])    
                                    ->all();
                                    Modal::begin([
                                        'header' => '<h3 >List of Employee</h3>',
                                        'headerOptions' => ['style' => 'color: white; background-color: #3c8dbc;'],
                                        'options' => ['id' => 'employee-list'],
                                        'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
                                    ]);
                                    echo '<table class="table table-striped">';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>#</th>';                                            
                                                echo '<th>Avatar</th>';
                                                echo '<th>First Name</th>';
                                                echo '<th>Last Name</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $i = 1;                                                                            
                                            foreach ($employees as $employee) {
                                                    $user_id = new \MongoDB\BSON\ObjectID($employee->userId); 
                                                    $users = Users::find()->where(['_id' => $user_id])->one();

                                                    if($users != null && $user_id != null){
                                                        echo '<tr>';
                                                            echo '<td>'.$i++.'</td>';
                                                            if(!!$users->avatar){
                                                                echo '<td><img src="'.$users->avatar .'" width = "70px"/></td>';
                                                            }else{
                                                                echo '<td><img src="/web/images/userdefault.png" width = "70px"/></td>';
                                                                
                                                            }
                                                            if(isset($users->first_name))
                                                                echo '<td>'.$users->first_name.'</td>';
                                                            else
                                                                echo '<td></td>';
                                                            
                                                            if(isset($users->last_name)){
                                                                    echo '<td>'.$users->last_name.'</td>';

                                                            }else{
                                                                    echo '<td></td>';
                                                            }
                                                        echo '</tr>';

                                                    }
                                                        
                                                }
                                        
                                        
                                        echo '</tbody>';
                                    echo '</table>';

                                    Modal::end();
                                                    
                                                
                                    if($employees) {
                                        return '<a data-toggle="modal" data-target="#employee-list" style="cursor: pointer; ">'.sizeof($employees).'</a>';
                                    }else{
                                        return 0;
                                    }
                                
                            },$model)

                    ],
            ]
        ]);
        ?>
    
    </div>

 


</div>


<?php

    
        echo DetailView::widget([
            'model' => $model,
            'condensed'=>true,
            'hover'=>true,
            'formOptions' => [
                'options' => ['enctype' => 'multipart/form-data', 'data-pjax' => true]
            ],
            'enableEditMode' => true,
            'mode'=>DetailView::MODE_VIEW,
            'panel'=>[
                'heading'=>'Business # ' . $model->name['en'],
                'type'=>DetailView::TYPE_INFO,
            ],
            'attributes'=>[
                'columns' => 
                    //------------------------------------------------------------------ Business Name ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Business Name',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Business Name',
                        'attribute' => 'name[en]',
                        'value' => isset($model->name['en']) ? $model->name['en'] : null

                    ],
                    [
                        'label' => 'ชื่อบริษัท',
                        'attribute' => 'name[th]',                        
                        'value' => isset($model->name['th']) ? $model->name['th'] : null

                    ],

                    //------------------------------------------------------------------ Search Business Name ------------------------------------------------------------------
                    // [
                    //     'group' => true,
                    //     'label' => 'Search Business Name',
                    //     'rowOptions' => ['class' => 'info'],
                    // ],
                    // [
                    //     'label' => 'Search Business Name',
                    //     'value' => isset($model->s_name['en']) ? $model->s_name['en'] : null

                    // ],
                    // [
                    //     'label' => 'ค้นหาชื่อบริษัท',
                    //     'value' => isset($model->s_name['th']) ? $model->s_name['th'] : null

                    // ],

                    //------------------------------------------------------------------ Image ------------------------------------------------------------------
                    [
                        'group' => true, 
                        'label' => 'Image',
                        'rowOptions' => ['class' => 'info'],
                    ],
                 
                    [
                        'label' => 'Logo',
                        'attribute' => 'logo',
                        'type' => DetailView::INPUT_FILEINPUT,
                        'options' => ['multiple' => true],
                        
                        'widgetOptions' => [
                            'model' => $model,
                            
                            'attribute' => 'logo',
                             'pluginOptions' => [
                                'showPreview' => true,
                                'showCaption' => true,
                                'showRemove' => false,
                                'showUpload' => false,   
                                                   
                                'fileActionSettings' => [
                                    'indicatorNew' => '',
                                ]
                            ],
                            
                            
                        ],
                        'value'=> call_user_func(function($model){
                            $items = array();
                            if(isset($model->logo)){
                                $pic['url'] = $model->logo;
                                $pic['src'] = $model->logo;
                                $pic['options']['title'] = 'Logo';
                                array_push($items,$pic);
                                return dosamigos\gallery\Gallery::widget(['items' => $items]);
                            }else{
                                return 'Not found Documents';
                            }
                            
                        }, $model),
                        'format'=>'raw',
                    ],
                       
                    // [
                    //     'label' => 'Cover',
                    //     'value' => isset($model->cover) ? $model->cover : null
                    // ],

                    //------------------------------------------------------------------ Company Information ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Company Information',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    // '_id',
                    [
                        'label' => 'Company ID',
                        'value' => isset($model->comp_id) ? $model->comp_id : null
                        //'attribute'=>'comp_id'
                    ],
                    [
                        'label' => 'Registration ID',
                        'value' => isset($model->reg_id) ? $model->reg_id : null
                        //'attribute'=>'reg_id'
                    ],
                    [  
                        'label' => 'Registration Date',
                        'value' => MongoDate::mongoDateToString($model->reg_date,true)
                    ],
                    [  
                        'label' => 'Registration Capital',
                        'attribute' => 'reg_capital',
                        'value' => number_format($model->reg_capital)
                    ],
                    [
                        'label' => 'Business Size',
                        'attribute' => 'business_size',
                        'value' =>call_user_func(function($model){
                            $catalog = CATALOG::find()->where(['CATALOG_ID' => $model->business_size])->one();
                            if($catalog) return $catalog['NAME_EN'];
                            else return null;              
                        },$model),
                    ],
                    [
                        'label' => 'Organization Type',
                        'attribute' => 'organization_type',
                        'value' => isset($model->organization_type) ? $model->organization_type : null
                    ],
                    [
                        'label' => 'Industrial Estate',
                        'attribute'=>'industrial_estate'
                    ],
                    [
                        'label' => 'Trade Name',
                        'attribute'=>'trade_name'
                    ],
                    [
                        'label' => 'Product Text',
                        'type'=>DetailView::INPUT_TEXTAREA,
                        'attribute'=>'pd_text'
                    ],
                    [
                        'label' => 'Summary',
                        'type'=>DetailView::INPUT_TEXTAREA,
                        'attribute'=>'summary'
                    ],
                  
           
                    //------------------------------------------------------------------ Registration Type ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Registration Type',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Registration Type/ประเภทการจดทะเบียน',
                        'attribute' => 'reg_type',  
                        'type' => DetailView::INPUT_SELECT2,   
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->where(['CATALOG_ID' => $model->reg_type])->one();
                            if($catalog) return $catalog['NAME_EN'];
                            else return null;              
                        },$model),
                        'widgetOptions' => [
                            'data' => call_user_func(function(){
                                $registration_types = RegistrationType::find()->all();
                                
                                $reg_type = array();
                                 foreach ($registration_types as $registration_type) {
                                     $reg_type[$registration_type->type] = $registration_type->name['en'];
                                }

                                return $reg_type;
                            })
                        ]                

                    ],
             

                    //------------------------------------------------------------------ Company Status ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Company Status',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Company Status/สถานะบริษัท',
                        'attribute' => 'comp_status',   
                        'type' => DetailView::INPUT_SELECT2,     
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->where(['CATALOG_ID' => $model->comp_status])->one();
                            if($catalog) return $catalog['NAME_EN'];
                            else return null;              
                        },$model),                                                                   
                        'widgetOptions' => [
                            'data' => call_user_func(function(){
                                $company_statuses = CompanyStatus::find()->all();
             
                                $comp_status = array();
                                 foreach ($company_statuses as $company_status) {
                                     $comp_status[$company_status->code] = $company_status->name['en'];
                                }

                                return $comp_status;
                            })
                        ]
                    ],
                  

                    //------------------------------------------------------------------ Thailand Standard Industrial Classification ------------------------------------------------------------------
                    // [
                    //     'group' => true,
                    //     'label' => 'Thailand Standard Industrial Classification',
                    //     'rowOptions' => ['class' => 'info'],
                    // ],
                    // [
                    //     'label' => 'TSIC/ประเภทมาตรฐานอุตสาหกรรม',
                    //     'attribute' => 'tsic',  
                    //     'value' => call_user_func(function($model){
                    //         $catalog = CATALOG::find()->where(['CATALOG_ID' => $model->tsic])->one();
                    //         if($catalog) return $catalog['NAME_EN'];
                    //         else return null;              
                    //     },$model),                                                                       
                    //     'type' => DetailView::INPUT_SELECT2,                                                                        
                    //     'widgetOptions' => [
                    //         'data' => call_user_func(function(){
                    //             $catalogs = CATALOG::find()->where(['between', 'CATALOG_ID', 1710000 , 1719999])->all();
                                
                    //             $reg_type = array();
                    //              foreach ($catalogs as $catalog) {
                    //                  $reg_type[$catalog->CATALOG_ID] = $catalog->NAME_EN;
                    //             }

                    //             return $reg_type;
                    //         })
                    //     ]
                    // ],
             

                    //------------------------------------------------------------------ Industry Group ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Industry Group',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Industry Group/กลุ่มอุตสาหกรรม',
                        'attribute' => 'industry_group',
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->where(['CATALOG_ID' => $model->industry_group])->one();
                            if($catalog) return $catalog['NAME_EN'];
                            else return null;              
                        },$model),
                        'type' => DetailView::INPUT_SELECT2,                                                                        
                        'widgetOptions' => [
                            'data' => call_user_func(function(){
                                // $catalogs = CATALOG::find()->where(['between', 'CATALOG_ID', 2010000 , 2019999])->all();
                               $industry_groups = IndustryGroup::find()->all();
                                $industry_group_array = array();
                                 foreach ($industry_groups as $industry_group) {
                                     $industry_group_array[$industry_group->code] = $industry_group->name['en'];
                                }

                                return $industry_group_array;

                            })
                        ]
                    ],
             

                    //------------------------------------------------------------------ Address ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Address',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Address',
                        'attribute' => 'addresses[address]',
                        'value' => isset($model->addresses['address']) ? $model->addresses['address'] : null
                    ],
                    [
                        'label' => 'No.',
                        'attribute' => 'addresses[no]',                        
                        'value' => isset($model->addresses['no']) ? $model->addresses['no'] : null
                    ],
                    [
                        'label' => 'Road',
                        'attribute' => 'addresses[road]',                                                
                        'value' => isset($model->addresses['road']) ? $model->addresses['road'] : null
                    ],
                     [
                        'label' => 'ถนน',
                        'attribute' => 'addresses[road_th]',                                                                                                                                                                                                                                                
                        'value' => isset($model->addresses['road_th']) ? $model->addresses['road_th'] : null
                    ],
                    [
                        'label' => 'Province',
                        'attribute' => 'addresses[province]',                                                                                                                        
                        'value' => call_user_func(function($model){
                            if(isset($model->addresses['province'])){
                                $catalog = CATALOG::find()->where(['CATALOG_ID' => $model->addresses['province']])->one();
                                if($catalog) return $catalog['NAME_EN'];
                                else return null;
                            }else return null;
                        },$model),
                        'type' => DetailView::INPUT_DROPDOWN_LIST,                                                                        
                        'items' => call_user_func(function($provinces){
                            $province = array();
                            if(!!$provinces){
                                foreach ($provinces as $address_mapping) {
                                    $catalog = CATALOG::find()->where(['CATALOG_ID' => $address_mapping])->one();
                                    $province[$catalog->CATALOG_ID] = $catalog->NAME_EN;
                                }
                                
                                return $province;
                            }else{
                                return 'No Province';
                            }
                        }, $provinces),
                
                    ],
                    [
                        'label' => 'District',
                        'attribute' => 'addresses[district]',                                                                        
                        'value' => call_user_func(function($model){
                            if(isset($model->addresses['district'])){
                                $catalog = CATALOG::find()->where(['CATALOG_ID' => $model->addresses['district']])->one();
                                if($catalog) return $catalog['NAME_EN'];
                                else return null;
                            }else return null;
                        },$model),
                        'type' => DetailView::INPUT_DROPDOWN_LIST,                                                                      
                        'items' => call_user_func(function($districts){
                            $district = array();
                        
                                foreach ($districts as $address_mapping) {
                                    if($address_mapping){
                                        $catalog = CATALOG::find()->where(['CATALOG_ID' => $address_mapping])->one();
                                        $district[$catalog->CATALOG_ID] = $catalog->NAME_EN;

                                    }
                                }

                                return $district;

                        }, $districts)
                        
                     
                    ],
                    [
                        'label' => 'Sub District',
                        'attribute' => 'addresses[sub_district]',                                                                                                
                        'value' => call_user_func(function($model){
                            if(isset($model->addresses['sub_district'])){
                                $catalog = CATALOG::find()->where(['CATALOG_ID' => $model->addresses['sub_district']])->one();
                                if($catalog) return $catalog['NAME_EN'];
                                else return null;
                            }else return null;
                        },$model),
                        'type' => DetailView::INPUT_DROPDOWN_LIST,                                                                        
                        'items' => call_user_func(function($sub_districts){
                            
                                $sub_district = array();
                                foreach ($sub_districts as $address_mapping) {
                                    if($address_mapping){
                                        $catalog = CATALOG::find()->where(['CATALOG_ID' => $address_mapping])->one();
                                        $sub_district[$catalog->CATALOG_ID] = $catalog->NAME_EN;

                                    }
                                }

                                return $sub_district;

                        
                        }, $sub_districts)
                        
                   
           
                    ],
               
                    [
                        'label' => 'Country',
                        'attribute' => 'addresses[country]',                                                                                                                                                
                        'value' => isset($model->addresses['country']) ? $model->addresses['country'] : null
                    ],
                      [
                        'label' => 'ประเทศ',
                        'attribute' => 'addresses[country_th]',                                                                                                                                                                                                                                                                                                                                                
                        'value' => isset($model->addresses['country_th']) ? $model->addresses['country_th'] : null
                    ],
                    [
                        'label' => 'Postal Code/รหัสไปรษณีย์',
                        'attribute' => 'addresses[zipcode]',                                                                                                                                                                        
                        'value' => isset($model->addresses['zipcode']) ? $model->addresses['zipcode'] : null
                    ],

                    [
                        'group' => true,
                        'label' => 'Contact',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Telephone',
                        'attribute' => 'telephone[no]',                                                                                                                                                                                                                                                                                                                                                                                                
                        'value' => isset($model->telephone['no']) ? $model->telephone['no'] : null
                    ],
                    [
                        'label' => 'Fax',
                        'attribute' => 'fax',                                                                                                                                                                                                                                                                                                                                                                                                                        
                        'value' => isset($model->fax) ? $model->fax : null
                    ],
                    [
                        'label' => 'Email',
                        'attribute' => 'email',                                                                                                                                                                                                                                                                                                                                                                                                                                                
                        'value' => isset($model->email) ? $model->email : null
                    ],
                    [
                        'label' => 'Url',
                        'attribute' => 'url',                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
                        'value' => isset($model->url) ? $model->url : null
                    ],
                    //------------------------------------------------------------------ Location ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Location',
                        'rowOptions' => ['class' => 'info'],
                    ],
                 
                    [
                        'label' => 'Longitude',
                        'attribute' => 'loc[coordinates][0]',
                        'value' => isset($model->loc['coordinates'][0]) ? $model->loc['coordinates'][0] : null
                    ],
                    [
                        'label' => 'Latitude',
                        'attribute' => 'loc[coordinates][1]',                        
                        'value' => isset($model->loc['coordinates'][1]) ? $model->loc['coordinates'][1] : null
                    ],
                    //------------------------------------------------------------------ Social ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Social',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Facebook',
                        'attribute' => 'social[facebook]',
                        'value' => isset($model->social['facebook']) ? $model->social['facebook'] : null
                    ],
                    [
                        'label' => 'Twitter',
                        'attribute' => 'social[twitter]',                        
                        'value' => isset($model->social['twitter']) ? $model->social['twitter'] : null
                    ],
                    [
                        'label' => 'Youtube',
                        'attribute' => 'social[youtube]',                                                
                        'value' => isset($model->social['youtube']) ? $model->social['youtube'] : null
                    ],
                    [
                        'label' => 'Linkedin',
                        'attribute' => 'social[linkedin]',                                                                        
                        'value' => isset($model->social['linkedin']) ? $model->social['linkedin'] : null
                    ],
                    [
                        'label' => 'Google+',
                        'attribute' => 'social[google+]',                                                                                                
                        'value' => isset($model->social['google+']) ? $model->social['google+'] : null
                    ],
                    [
                        'label' => 'Instagram',
                        'attribute' => 'social[instagram]',                                                                                                                        
                        'value' => isset($model->social['instagram']) ? $model->social['instagram'] : null
                    ],
                
            ]
        ]);
    ?>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC3S93h1PoWYklPkzDPvHbJc0TBiClfEd8&callback"></script>



    
    

    <script type="text/javascript">

        $(function(){
            $('.gallery-item img').css({'width' : '100px','height' : '100px'});
        });

        $('#businesses-addresses-province').click(function(){
           var province_catalog_id = $(this).val();
           callAjaxDistrict(province_catalog_id);
           setTimeout(function(){
                callAjaxSubDistrict($('#businesses-addresses-district').val());
                
            }, 1000);
       
  
        });

        $('#businesses-addresses-district').click(function(){
           var district_catalog_id = $(this).val();
           callAjaxSubDistrict(district_catalog_id);
       
        });

        function callAjaxDistrict(catalog_id){
            $.ajax({
                method: "GET",
                url: "<?php echo Yii::$app->getUrlManager()->createUrl('businesses/default/district-option')  ; ?>",
                data: {
                    province: catalog_id
                },
                success: function (data) {
                    var district = JSON.parse(data);
                    var html = '';
                    $('#businesses-addresses-district').html(function(){
                        for (var i = 0; i < district.length; i++) { 
                            html += '<option value='+district[i].catalog_id+'>'+district[i].district+'</option>';

                        }

                        return html;
                    });
                    
                },
                error: function (exception) {
                    console.log(exception);
                }
            });
        }
        
        function callAjaxSubDistrict(catalog_id){
            $.ajax({
                method: "GET",
                url: "<?php echo Yii::$app->getUrlManager()->createUrl('businesses/default/sub-district-option')  ; ?>",
                data: {
                    district: catalog_id
                },
                success: function (data) {
                    var sub_district = JSON.parse(data);
                    var html = '';
                    $('#businesses-addresses-sub_district').html(function(){
                        for (var i = 0; i < sub_district.length; i++) { 
                           html += '<option value='+sub_district[i].catalog_id+'>'+sub_district[i].sub_district+'</option>';

                        }

                        return html;
                    });
                    
                },
                error: function (exception) {
                    console.log(exception);
                }
            });
        }





    </script>

   