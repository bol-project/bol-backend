<?php

//use yii\helpers\Html;
//use yii\widgets\DetailView;
use app\modules\users\models\Users;
use app\modules\claims\models\Claims;
use app\models\CATALOG;
use app\models\Cards;
use app\models\Employees;
use app\modules\claims\controllers\RequestStatus;
use app\modules\claims\controllers\ClaimType;
use app\models\MongoDate;
use kartik\helpers\Html;
use kartik\detail\DetailView;
use yii\bootstrap\Modal;


/* @var $this yii\web\View */
/* @var $model app\modules\businesses\models\Businesses */

$this->title = $model->name['th'];
$this->params['breadcrumbs'][] = ['label' => 'Businesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="businesses-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box box-info" style="text-align: center;">
       <iframe width="70%" height="400" src="http://www.matchlink.asia/web/?businessId=<?= $model->_id ?>&language=EN" style="margin-top: 0.5%;">
            <p>Your browser does not support iframes.</p>
        </iframe>
    </div>
    <div class="box box-info">
        <?php
        /*
        <div class="box-header with-border">
            
            <p>
                <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('Index', ['/businesses/'], ['class'=>'btn btn-success']) ?>
            </p>

        </div>
        */
        ?>

        <?php
        echo DetailView::widget([
            'model' => $model,
            'condensed'=>true,
            'hover'=>true,
            'enableEditMode' => false,
            'mode'=>DetailView::MODE_VIEW,
            // 'panel'=>[
            //     'heading'=>'Business # ' . $model->_id,
            //     'type'=>DetailView::TYPE_INFO,
            // ],
            'attributes'=>[
                'columns' => 
                    //------------------------------------------------------------------ Business Name ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Summary',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    
                    [
                        'label' => 'Last Register Date',
                        'value' => call_user_func(function($model){
                                        $card = Cards::find()->orderBy(['_id' => SORT_DESC])->where(['_business_id' => $model->_id])->one();
                                        if(isset($card->_id)){
                                            $timestamp = hexdec(substr($card->_id, 0, 8));
                                            $date = MongoDate::mongoDateToString(MongoDate::timestampToMongoDate($timestamp), false);
                                            return $date;
                                        }else return null;
                                    },$model)
                    ],
                    [
                        'label' => '',
                        'value' => call_user_func(function($model){
                                        $card = Cards::find()->orderBy(['_id' => SORT_DESC])->where(['_business_id' => $model->_id])->one();
                                        if(isset($card->_id)){
                                            $user = Users::find()->where(['_id' => $card->_user_id ])->one();
                                            return $user->email;
                                        }else return null;
                                    },$model)
                    ],
                    /*
                    [
                        'label' => 'Last Login',
                        'value' => call_user_func(function($model){
                                        $user = Users::find(['defaultCardId' => $model->_id])->orderBy(['lastLogedin' => SORT_DESC])->one();
                                        $date = MongoDate::mongoDateToString($user->lastLogedin, false);
                                        return $date;
                                    },$model)

                    ],
                    [
                        'label' => '',
                        'value' => call_user_func(function($model){
                                        $user = Users::find(['defaultCardId' => $model->_id])->orderBy(['lastLogedin' => SORT_DESC])->one();
                                        return $user->email;
                                    },$model)

                    ],
                    */
                    [
                        'label' => 'Number of User',
                        'format' => 'raw',
                        'value' => call_user_func(function($model){
                                        $cards = Cards::find()->where(['_business_id' => $model->_id])->all();
                                        Modal::begin([
                                            'header' => '<h3 >List of User</h3>',
                                            'headerOptions' => ['style' => 'color: white; background-color: #3c8dbc;'],
                                            'options' => ['id' => 'user-list'],
                                            'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
                                        ]);
                                        echo '<table class="table table-striped">';
                                            echo '<thead>';
                                                echo '<tr>';
                                                    echo '<th>#</th>';
                                                    echo '<th>Avatar</th>';
                                                    echo '<th>First Name</th>';
                                                    echo '<th>Last Name</th>';
                                                echo '</tr>';
                                            echo '</thead>';
                                            echo '<tbody>';
                                            $i = 1;
                                                    foreach ($cards as $card) {
                                                        $users = Users::find()->where(['_id' => $card->_user_id])->one();
                                                            echo '<tr>';
                                                                echo '<td>'.$i++.'</td>';
                                                                if(!!$users->avatar){
                                                                    echo '<td><img src="'.$users->avatar .'" width = "70px"/></td>';
                                                                }else{
                                                                    echo '<td><img src="/web/images/userdefault.png" width = "70px"/></td>';
                                                                    
                                                                }
                                                                echo '<td>'.$users->first_name.'</td>';
                                                                echo '<td>'.$users->last_name.'</td>';
                                                            echo '</tr>';
                                                            
                                                    }
                                            
                                            echo '</tbody>';
                                        echo '</table>';

                                        Modal::end();
                                        
                                        if(sizeof($cards) == 0){
                                            return sizeof($cards);
                                        }else {
                                             return '<a data-toggle="modal" data-target="#user-list" style="cursor: pointer; ">'.sizeof($cards).'</a>';
                                        }
                                    },$model)


                    ],
                    //------------------------------------------------------------------ Business Name ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Claim Info',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Claim Status',
                        'format'=>'raw',
                        'value' => call_user_func(function($model){
                            $claim = Claims::find()
                                ->where(['claim_type' => (int)ClaimType::$CLAIMER])
                                ->orWhere(['claim_type' => (string)ClaimType::$CLAIMER])
                                ->andWhere(['business_id' => (string)$model->_id, ])
                                ->andFilterCompare('company_registration_docs', '>0')
                                ->andFilterCompare('authorized_director_citizen_cards', '>0')   
                                ->one();
                            if(isset($claim['request_status'])){
                                if($claim && $claim['request_status'] == RequestStatus::$VERIFIED) 
                                    return '<span class="label label-success">'.RequestStatus::toString($claim['request_status']).'</span>';
                                else 
                                    return '<span class="label label-danger">'.RequestStatus::toString($claim['request_status']).'</span>';
                            }else return null;
                        },$model)

                    ],
                    [
                        'label' => 'CS Response',
                        'value' => call_user_func(function($model){
                            $claim = Claims::find()
                                ->where(['claim_type' => (int)ClaimType::$CLAIMER])
                                ->orWhere(['claim_type' => (string)ClaimType::$CLAIMER])
                                ->andWhere(['business_id' => (string)$model->_id, ])
                                ->andWhere(['request_status' => (int)RequestStatus::$VERIFIED, ])
                                ->orWhere(['request_status' => (string)RequestStatus::$VERIFIED, ])
                                ->andFilterCompare('company_registration_docs', '>0')
                                ->andFilterCompare('authorized_director_citizen_cards', '>0')   
                                ->one();
                            if(isset($claim['remarks']['agent_id'])){
                                return $claim['remarks']['agent_id'];
                            }else return 'null';
                        },$model)

                    ],
                    [
                        'label' => 'Claimer',
                        'value' => call_user_func(function($model){
                                $claim = Claims::find()
                                    ->where(['claim_type' => (int)ClaimType::$CLAIMER])
                                    ->orWhere(['claim_type' => (string)ClaimType::$CLAIMER])
                                    ->andWhere(['business_id' => (string)$model->_id, ])
                                    ->andWhere(['request_status' => (int)RequestStatus::$VERIFIED, ])
                                    ->orWhere(['request_status' => (string)RequestStatus::$VERIFIED, ])    
                                    ->andFilterCompare('company_registration_docs', '>0')
                                    ->andFilterCompare('authorized_director_citizen_cards', '>0')
                                    ->one();
                                $user = Users::find()->where(['_id' => $claim['user_id']])->one();
                                if(isset($user['first_name']) && isset($user['last_name'])){
                                    if($user) 
                                        return $user['first_name'].' '.$user['last_name'];
                                    else 
                                        return null;
                                }else return null;
                            },$model)
                    ],
                    [
                        'label' => 'Number of Sub Claimer',
                        'format' => 'raw',
                        'value' => call_user_func(function($model){
                                $claims = Claims::find()
                                    ->where(['claim_type' => (int)ClaimType::$SUB_CLAIMER])
                                    ->orWhere(['claim_type' => (string)ClaimType::$SUB_CLAIMER])
                                    ->andWhere(['business_id' => (string)$model->_id])    
                                    ->andWhere(['request_status' => 1])    
                                    ->all();
                                   
                                    Modal::begin([
                                        'header' => '<h3 >List of Sub Claimer</h3>',
                                        'headerOptions' => ['style' => 'color: white; background-color: #3c8dbc;'],
                                        'options' => ['id' => 'claimer-list'],
                                        'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
                                    ]);
                                    echo '<table class="table table-striped">';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>#</th>';                                            
                                                echo '<th>Avatar</th>';
                                                echo '<th>First Name</th>';
                                                echo '<th>Last Name</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                         $i = 1;                                    
                                            foreach ($claims as $claim) {
                                                $user_id = new \MongoDB\BSON\ObjectID($claim->user_id); 
                                                $users = Users::find()->where(['_id' => $user_id])->one();
                                                    echo '<tr>';
                                                        echo '<td>'.$i++.'</td>';               
                                                        if(!!$users->avatar){
                                                            echo '<td><img src="'.$users->avatar .'" width = "70px"/></td>';
                                                        }else{
                                                            echo '<td><img src="/web/images/userdefault.png" width = "70px"/></td>';
                                                            
                                                        }                                         
                                                        echo '<td>'.$users->first_name.'</td>';
                                                        echo '<td>'.$users->last_name.'</td>';
                                                    echo '</tr>';
                                                    
                                            }
                                        
                                        
                                        echo '</tbody>';
                                    echo '</table>';

                                    Modal::end();
                                    
                                            
                                    if($claims) {
                                        return '<a data-toggle="modal" data-target="#claimer-list" style="cursor: pointer; ">'.sizeof($claims).'</a>';
                                    }else{
                                        return 0;
                                    }
                                   
                              
                            },$model)

                    ],
                    [
                        'label' => 'Number of Employee',
                        'format' => 'raw',
                        'value' => call_user_func(function($model){
                                $employees = Employees::find()
                                    ->where(['businessId' => (string)$model->_id, 'status' => 1])    
                                    ->all();
                                    Modal::begin([
                                        'header' => '<h3 >List of Employee</h3>',
                                        'headerOptions' => ['style' => 'color: white; background-color: #3c8dbc;'],
                                        'options' => ['id' => 'employee-list'],
                                        'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
                                    ]);
                                    echo '<table class="table table-striped">';
                                        echo '<thead>';
                                            echo '<tr>';
                                                echo '<th>#</th>';                                            
                                                echo '<th>Avatar</th>';
                                                echo '<th>First Name</th>';
                                                echo '<th>Last Name</th>';
                                            echo '</tr>';
                                        echo '</thead>';
                                        echo '<tbody>';
                                        $i = 1;                                                                            
                                            foreach ($employees as $employee) {
                                                    $user_id = new \MongoDB\BSON\ObjectID($employee->userId); 
                                                    $users = Users::find()->where(['_id' => $user_id])->one();
                                                        echo '<tr>';
                                                            echo '<td>'.$i++.'</td>';                                                                                                                
                                                            if(!!$users->avatar){
                                                                echo '<td><img src="'.$users->avatar .'" width = "70px"/></td>';
                                                            }else{
                                                                echo '<td><img src="/web/images/userdefault.png" width = "70px"/></td>';
                                                                
                                                            } 
                                                            echo '<td>'.$users->first_name.'</td>';
                                                            echo '<td>'.$users->last_name.'</td>';
                                                        echo '</tr>';
                                                        
                                                }
                                        
                                        
                                        echo '</tbody>';
                                    echo '</table>';

                                    Modal::end();
                                                    
                                                
                                    if($employees) {
                                        return '<a data-toggle="modal" data-target="#employee-list" style="cursor: pointer; ">'.sizeof($employees).'</a>';
                                    }else{
                                        return 0;
                                    }
                                
                            },$model)

                    ],
            ]
        ]);
        ?>
    
    </div>

 


</div>



<?php

    
        echo DetailView::widget([
            'model' => $model,
            'condensed'=>true,
            'hover'=>true,
            'enableEditMode' => true,
            'mode'=>DetailView::MODE_VIEW,
            'panel'=>[
                'heading'=>'Business # ' . $model->_id,
                'type'=>DetailView::TYPE_INFO,
            ],
            'attributes'=>[
                'columns' => 
                    //------------------------------------------------------------------ Business Name ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Business Name',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Business Name',
                        'attribute' => 'name[en]',
                        'value' => isset($model->name['en']) ? $model->name['en'] : null

                    ],
                    [
                        'label' => 'ชื่อบริษัท',
                        'attribute' => 'name[th]',                        
                        'value' => isset($model->name['th']) ? $model->name['th'] : null

                    ],

                    //------------------------------------------------------------------ Search Business Name ------------------------------------------------------------------
                    // [
                    //     'group' => true,
                    //     'label' => 'Search Business Name',
                    //     'rowOptions' => ['class' => 'info'],
                    // ],
                    // [
                    //     'label' => 'Search Business Name',
                    //     'value' => isset($model->s_name['en']) ? $model->s_name['en'] : null

                    // ],
                    // [
                    //     'label' => 'ค้นหาชื่อบริษัท',
                    //     'value' => isset($model->s_name['th']) ? $model->s_name['th'] : null

                    // ],

                    //------------------------------------------------------------------ Image ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Image',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Logo',
                        'value'=> call_user_func(function($model){
                            $items = array();
                            if(isset($model->logo)){
                                $pic['url'] = $model->logo;
                                $pic['src'] = $model->logo;
                                $pic['options']['title'] = 'Logo';
                                array_push($items,$pic);
                                return dosamigos\gallery\Gallery::widget(['items' => $items]);
                            }else{
                                return 'Not found Documents';
                            }
                            
                        }, $model),
                        'format'=>'raw',
                    ],
                    // [
                    //     'label' => 'Cover',
                    //     'value' => isset($model->cover) ? $model->cover : null
                    // ],

                    //------------------------------------------------------------------ Company Information ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Company Information',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    // '_id',
                    // [
                    //     'label' => 'Company ID',
                    //     'attribute'=>'comp_id'
                    // ],
                    // [
                    //     'label' => 'Registration ID',
                    //     'attribute'=>'reg_id'
                    // ],
                    // [  
                    //     'label' => 'Registration Date',
                    //     'value' => MongoDate::mongoDateToString($model->reg_date,true)
                    // ],
                    [  
                        'label' => 'Registration Capital',
                        'value' => number_format($model->reg_capital)
                    ],
                    [
                        'label' => 'Business Size',
                        'value' => isset($model->business_size) ? number_format($model->business_size,2) : null
                    ],
                    [
                        'label' => 'Organization Type',
                        'value' => isset($model->organization_type) ? $model->organization_type : null
                    ],
                    [
                        'label' => 'Industrial Estate',
                        'attribute'=>'industrial_estate'
                    ],
                    [
                        'label' => 'Trade Name',
                        'attribute'=>'trade_name'
                    ],
                    [
                        'label' => 'Product Text',
                        'attribute'=>'pd_text'
                    ],
                    [
                        'label' => 'Summary',
                        'attribute'=>'summary'
                    ],
                  
           
                    //------------------------------------------------------------------ Registration Type ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Registration Type',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Registration Type',
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->reg_type])->one();
                            if($catalog) return $catalog['NAME_EN'];
                            else return null;
                        },$model)

                    ],
                    [
                        'label' => 'ประเภทการลงทะเบียน',
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->reg_type])->one();
                            if($catalog) return $catalog['NAME_TH'];
                            else return null;
                        },$model)

                    ],

                    //------------------------------------------------------------------ Company Status ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Company Status',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Company Status',
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->comp_status])->one();
                            if($catalog) return $catalog['NAME_EN'];
                            else return null;
                        },$model)
                    ],
                    [
                        'label' => 'สถานะบริษัท',
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->comp_status])->one();
                            if($catalog) return $catalog['NAME_TH'];
                            else return null;
                        },$model)
                    ],

                    //------------------------------------------------------------------ Thailand Standard Industrial Classification ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Thailand Standard Industrial Classification',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'TSCI',
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => (int)$model->tsic])->one();
                            if($catalog) return $catalog['NAME_EN'];
                            else return null;
                        },$model)
                    ],
                    [
                        'label' => 'ประเภทมาตรฐานอุตสาหกรรม',
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => (int)$model->tsic])->one();
                            if($catalog) return $catalog['NAME_TH'];
                            else return null;
                        },$model)
                    ],

                    //------------------------------------------------------------------ Industry Group ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Industry Group',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Industry Group',
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => (int)$model->industry_group])->one();
                            if($catalog) return $catalog['NAME_EN'];
                            else return null;
                        },$model)
                    ],
                    [
                        'label' => 'กลุ่มอุตสาหกรรม',
                        'value' => call_user_func(function($model){
                            $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => (int)$model->industry_group])->one();
                            if($catalog) return $catalog['NAME_TH'];
                            else return null;
                        },$model)
                    ],

                    //------------------------------------------------------------------ Address ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Address',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Address',
                        'value' => isset($model->addresses['address']) ? $model->addresses['address'] : null
                    ],
                    [
                        'label' => 'No.',
                        'value' => isset($model->addresses['no']) ? $model->addresses['no'] : null
                    ],
                    [
                        'label' => 'Road',
                        'value' => isset($model->addresses['road']) ? $model->addresses['road'] : null
                    ],
                    [
                        'label' => 'District',
                        'value' => call_user_func(function($model){
                            if(isset($model->addresses['district'])){
                                $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->addresses['district']])->one();
                                if($catalog) return $catalog['NAME_EN'];
                                else return null;
                            }else return null;
                        },$model)
                    ],
                    [
                        'label' => 'Sub District',
                        'value' => call_user_func(function($model){
                            if(isset($model->addresses['sub_district'])){
                                $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->addresses['sub_district']])->one();
                                if($catalog) return $catalog['NAME_EN'];
                                else return null;
                            }else return null;
                        },$model)
                    ],
                    [
                        'label' => 'Province',
                        'value' => call_user_func(function($model){
                            if(isset($model->addresses['province'])){
                                $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->addresses['province']])->one();
                                if($catalog) return $catalog['NAME_EN'];
                                else return null;
                            }else return null;
                        },$model)
                    ],
                    [
                        'label' => 'Country',
                        'value' => isset($model->addresses['country']) ? $model->addresses['country'] : null
                    ],
                    [
                        'label' => 'Zipcode',
                        'value' => isset($model->addresses['zipcode']) ? $model->addresses['zipcode'] : null
                    ],

                    //------------------------------------------------------------------ ที่อยู่ ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'ที่อยู่',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'ที่อยู่',
                        'value' => isset($model->addresses['address']) ? $model->addresses['address'] : null
                    ],
                    [
                        'label' => 'เลขที่',
                        'value' => isset($model->addresses['no']) ? $model->addresses['no'] : null
                    ],
                    [
                        'label' => 'ถนน',
                        'value' => isset($model->addresses['road_th']) ? $model->addresses['road_th'] : null
                    ],
                    [
                        'label' => 'เขต/อำเภอ',
                        'value' => call_user_func(function($model){
                            if(isset($model->addresses['district'])){
                                $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->addresses['district']])->one();
                                if($catalog) return $catalog['NAME_TH'];
                                else return null;
                            }else return null;
                        },$model)
                    ],
                    [
                        'label' => 'แขวง/ตำบล',
                        'value' => call_user_func(function($model){
                            if(isset($model->addresses['sub_district'])){
                                $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->addresses['sub_district']])->one();
                                if($catalog) return $catalog['NAME_TH'];
                                else return null;
                            }else return null;
                        },$model)
                    ],
                    [
                        'label' => 'จังหวัด',
                        'value' => call_user_func(function($model){
                            if(isset($model->addresses['province'])){
                                $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->addresses['province']])->one();
                                if($catalog) return $catalog['NAME_TH'];
                                else return null;
                            }else return null;
                        },$model)
                    ],
                    [
                        'label' => 'ประเทศ',
                        'value' => isset($model->addresses['country_th']) ? $model->addresses['country_th'] : null
                    ],
                    [
                        'label' => 'รหัสไปรษณีย์',
                        'value' => isset($model->addresses['zipcode']) ? $model->addresses['zipcode'] : null
                    ],
                    //------------------------------------------------------------------ Contact ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Contact',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Telephone',
                        'value' => isset($model->telephone['no']) ? $model->telephone['no'] : null
                    ],
                    [
                        'label' => 'Fax',
                        'value' => isset($model->fax) ? $model->fax : null
                    ],
                    [
                        'label' => 'Email',
                        'value' => isset($model->email) ? $model->email : null
                    ],
                    [
                        'label' => 'Url',
                        'value' => isset($model->url) ? $model->url : null
                    ],
                    //------------------------------------------------------------------ Location ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Location',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Longitude',
                        'value' => isset($model->loc['coordinates'][0]) ? $model->loc['coordinates'][0] : null
                    ],
                    [
                        'label' => 'Latitude',
                        'value' => isset($model->loc['coordinates'][1]) ? $model->loc['coordinates'][1] : null
                    ],
                    //------------------------------------------------------------------ Social ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Social',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Facebook',
                        'attribute' => 'social[facebook]',
                        'value' => isset($model->social['facebook']) ? $model->social['facebook'] : null
                    ],
                    [
                        'label' => 'Twitter',
                        'value' => isset($model->social['twitter']) ? $model->social['twitter'] : null
                    ],
                    [
                        'label' => 'Youtube',
                        'value' => isset($model->social['youtube']) ? $model->social['youtube'] : null
                    ],
                    [
                        'label' => 'Linkedin',
                        'value' => isset($model->social['linkedin']) ? $model->social['linkedin'] : null
                    ],
                    [
                        'label' => 'Google+',
                        'value' => isset($model->social['google+']) ? $model->social['google+'] : null
                    ],
                    [
                        'label' => 'Instagram',
                        'value' => isset($model->social['instagram']) ? $model->social['instagram'] : null
                    ],
                    //------------------------------------------------------------------ Edit ------------------------------------------------------------------
                    [
                        'group' => true,
                        'label' => 'Edit',
                        'rowOptions' => ['class' => 'info'],
                    ],
                    [
                        'label' => 'Last Update',
                        'value' => isset($model->last_update) ? MongoDate::mongoDateToString($model->last_update,true) : null
                    ],
                    [
                        'label' => 'Last Modified',
                        'value' => isset($model->lastModified) ? MongoDate::mongoDateToString($model->lastModified,true) : null
                    ],
                    [
                        'label' => 'Card Update',
                        'value' => isset($model->card_update) ? MongoDate::mongoDateToString($model->card_update,true) : null
                    ],
            ]
        ]);
    ?>



    <script type="text/javascript">
        $(function(){
            $('.gallery-item img').css({'width' : '100px','height' : '100px'});
        })
    </script>