<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\CATALOG;
use app\models\MongoDate;
use yii\helpers\ArrayHelper;
use kartik\widgets\Select2;
use kartik\date\DatePicker;
use app\models\CompanyStatus;
use app\models\IndustryGroup;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\businesses\models\BusinessesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Businesses';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="businesses-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div class="box box-danger">
    
    <div class="box-header with-border">
    
    <!--<p>
        <?= Html::a('Create Businesses', ['create'], ['class' => 'btn btn-success']) ?>
    </p>-->

    </div>

    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
                        'firstPageLabel'=>'<<',   
                        'prevPageLabel' =>'<', 
                        'nextPageLabel' =>'>',   
                        'lastPageLabel' =>'>>'
            ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'Business Name',
                'attribute' => 'name',
                'format'=>'html',
                'value' => function($model){
                    return $model->name['en'];
                }
            ],
            [
                'label' => 'Registration ID',
                'attribute' => 'reg_id'
            ],
            [
                'label' => 'Company Status',
                'attribute' => 'comp_status',
                'filter' => 
                            Html::activeDropDownList($searchModel, 'comp_status', ArrayHelper::map(CompanyStatus::find()->all(), 'code','name.en'),
                                                                        ['class'=>'form-control select2','prompt' => 'Select Company Status']
                                                ),
                
                    'value'=> function($model){
                    $catalog = CATALOG::find()->where(['CATALOG_ID' => (int)$model->comp_status])->one();
                    if($catalog) return $catalog['NAME_EN'];
                    else return null;
                }
            ],
            [
                'label' => 'Industry Group',
                'attribute' => 'industry_group',
                'filter' => Html::activeDropDownList($searchModel, 'industry_group', ArrayHelper::map(IndustryGroup::find()->all(), 'code','name.en'),
                                                                        ['class'=>'form-control select2','prompt' => 'Select Industry Group']
                                                ),
                
                'value'=> function($model){
                    $catalog = CATALOG::find()->where(['CATALOG_ID' => (int)$model->industry_group])->one();
                    if($catalog) return $catalog['NAME_EN'];
                    else return null;
                }
                
            ],
            [
                'label' => 'Register Date',
                'attribute' => 'reg_date',
                'headerOptions' => ['class' => 'text-center'],
                'filterInputOptions' => [
                'class'=>'form-control',
                'id' => 'reg_date'
                ],
                'format'=>'html',
                'value' => function($model){
                    return $model->reg_date ? MongoDate::mongoDateToString($model->reg_date,true) : ' ';
                }
            ],
            // [
            //     'label' => 'Business Name TH',
            //     'attribute' => 'name',
            //     'format'=>'html',
            //     'value' => function($model){
            //         if(isset($model->name['th'])){
            //             return $model->name['th'];
            //         }
            //         else
            //             return '';
            //     }
            // ],
            
            // [
            //     'label' => 'Register Type EN',
            //     'attribute' => 'reg_type',
            //     'format'=>'html',
            //     'value' => function($model){
            //         $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->reg_type])->one();
            //         if($catalog) return $catalog['NAME_EN'];
            //         else return null;
            //     }
            // ],
            // [
            //     'label' => 'Register Type TH',
            //     'attribute' => 'reg_type',
            //     'format'=>'html',
            //     'value' => function($model){
            //         $catalog = CATALOG::find()->from('CATALOG')->where(['CATALOG_ID' => $model->reg_type])->one();
            //         if($catalog) return $catalog['NAME_TH'];
            //         else return null;
            //     }
            // ],
            //'_id',
            //'comp_id',
            //'reg_id',
            //'reg_date',
            //'reg_type',
            // 'reg_capital',
            // 'comp_status',
            // 'state_date',
            //'industry_group',
            // 'industry_estate',
            // 'trade_name',
            // 'tsic',
            // 'pd_text',
            // 'addresses',
            // 'loc',
            // 'group_status',
            // 's_name',
            // 'name',
            // 'organization_type',
            // 'telephone',
            // 'fax',
            // 'email',
            // 'url',
            // 'cliam_status',
            // 'cliamer',
            // 'logo',
            // 'cover',
            // 'summary',
            // 'social',
            // 'last_update',
            // 'card_update',
            // 'lastModified',

            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',            
            ],
        ],
    ]); ?>

    </div>
    
    </div>
    
</div>
<script type="text/javascript">
    $(function () {
        $("#reg_date").datepicker({format: "dd/mm/yyyy"}).datepicker();
        $(".select2").select2();

    });
</script>
