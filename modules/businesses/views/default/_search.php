<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\businesses\models\BusinessesSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="businesses-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, '_id') ?>

    <?= $form->field($model, 'comp_id') ?>

    <?= $form->field($model, 'reg_id') ?>

    <?= $form->field($model, 'reg_date') ?>

    <?= $form->field($model, 'reg_type') ?>

    <?php // echo $form->field($model, 'reg_capital') ?>

    <?php // echo $form->field($model, 'comp_status') ?>

    <?php // echo $form->field($model, 'state_date') ?>

    <?php // echo $form->field($model, 'industry_group') ?>

    <?php // echo $form->field($model, 'industry_estate') ?>

    <?php // echo $form->field($model, 'trade_name') ?>

    <?php // echo $form->field($model, 'tsic') ?>

    <?php // echo $form->field($model, 'pd_text') ?>

    <?php // echo $form->field($model, 'addresses') ?>

    <?php // echo $form->field($model, 'loc') ?>

    <?php // echo $form->field($model, 'group_status') ?>

    <?php // echo $form->field($model, 's_name') ?>

    <?php // echo $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'organization_type') ?>

    <?php // echo $form->field($model, 'telephone') ?>

    <?php // echo $form->field($model, 'fax') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'url') ?>

    <?php // echo $form->field($model, 'cliam_status') ?>

    <?php // echo $form->field($model, 'cliamer') ?>

    <?php // echo $form->field($model, 'logo') ?>

    <?php // echo $form->field($model, 'cover') ?>

    <?php // echo $form->field($model, 'summary') ?>

    <?php // echo $form->field($model, 'social') ?>

    <?php // echo $form->field($model, 'last_update') ?>

    <?php // echo $form->field($model, 'card_update') ?>

    <?php // echo $form->field($model, 'lastModified') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
