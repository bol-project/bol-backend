<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\businesses\models\Businesses */

$this->title = 'Update Businesses: ' . $model->name['en'];
$this->params['breadcrumbs'][] = ['label' => 'Businesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name['en'], 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="businesses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'provinces' => $provinces,
        'districts' => $districts,
        'sub_districts' => $sub_districts,

    ]) ?>

</div>
