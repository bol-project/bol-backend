<?php

namespace app\modules\businesses\controllers;

use Yii;
use app\modules\businesses\models\Businesses;
use app\modules\businesses\models\BusinessesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\helpers\API;
use app\models\CATALOG;
use app\models\AddressMapping;
use app\controllers\AdminController;
/**
 * DefaultController implements the CRUD actions for Businesses model.
 */
class DefaultController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Businesses models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BusinessesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Businesses model.
     * @param integer $_id
     * @return mixed
     */
    // public function actionView($id)
    // {
    //     return $this->render('view', [
    //         'model' => $this->findModel($id),
    //     ]);
    // }

    /**
     * Creates a new Businesses model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Businesses();

    //     if ($model->load(Yii::$app->request->post()) && $model->save()) {
    //         return $this->redirect(['view', 'id' => (string)$model->_id]);
    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //         ]);
    //     }
    // }

    /**
     * Updates an existing Businesses model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);      
        $api = Yii::$app->params['config']['api'];
        $provinces = AddressMapping::find()->distinct('province');
        sort($provinces);


        if(isset($model->addresses['province'])){
             $districts = AddressMapping::find()->where(['province' => $model->addresses['province']])->distinct('district');
            sort($districts);

        }else{
            $districts = [0 => ''];
        }
        
        if(isset($model->addresses['district'])){
            $sub_districts = AddressMapping::find()->where(['district' => $model->addresses['district']])->distinct('subdistrict');
            sort($sub_districts);

        }else{
            $sub_districts = [0 => ''];
        }

        if ($model->load(Yii::$app->request->post()) ) {
            
            $model->logo = UploadedFile::getInstance($model, 'logo');
            $model->logo = API::uploadBusinessLogo($model, $api);
            $model->reg_type = (int)$model->reg_type;
            $model->tsic = (int)$model->tsic;
            $model->reg_capital = (int)$model->reg_capital;
            $model->comp_status = (int)$model->comp_status;
            $model->industry_group = (int)$model->industry_group;
            $model->industrial_estate = (int)$model->industrial_estate;

            $adress = $model->addresses;
            $adress["province"] = (int)$model->addresses['province'];
            $adress["no"] = (int)$model->addresses['no'];
           
            if(isset($adress["district"])){
                 $adress["district"] = (int)$model->addresses['district'];
            }

            if(isset($adress["sub_district"])){
                $adress["sub_district"] = (int)$model->addresses['sub_district'];
            }
            $model->addresses = $adress;

            if($model->update()){
                Yii::$app->getSession()->setFlash('alert',[
                    'body'=>'Update success.',
                    'options'=>['class'=>'alert-success']
                ]);
                return $this->redirect(['update', 'id' => (string)$model->_id]);
            }else{
              
                return $this->render('update', [
                    'model' => $model,
                    'provinces' => $provinces,
                    'districts' => $districts,
                    'sub_districts' => $sub_districts,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
                'provinces' => $provinces,
                'districts' => $districts,
                'sub_districts' => $sub_districts,
            ]);
        }
    }

    public function actionDistrictOption($province = 0)
    {
        
      if(!$province == 0){
            $districts = AddressMapping::find()->where(['province' => (int)$province])->distinct('district');
            $district = array();
            foreach ($districts as $address_mapping) {
                $catalog = CATALOG::find()->where(['CATALOG_ID' => $address_mapping])->one();
                array_push($district , ['catalog_id' => $catalog->CATALOG_ID, 'district' => $catalog->NAME_EN]);

            }
            return \yii\helpers\Json::encode($district);
            
      }


    }

    public function actionSubDistrictOption($district = 0)
    {
        
      if(!$district == 0){
        $sub_districts = AddressMapping::find()->where(['district' => (int)$district])->distinct('subdistrict');
        $sub_district = array();
        foreach ($sub_districts as $address_mapping) {
            $catalog = CATALOG::find()->where(['CATALOG_ID' => $address_mapping])->one();
            array_push($sub_district , ['catalog_id' => $catalog->CATALOG_ID, 'sub_district' => $catalog->NAME_EN]);

        }
            
        return \yii\helpers\Json::encode($sub_district);
            
      }else{
          return \yii\helpers\Json::encode([0 => 'No District']);
          
      }


    }


    /**
     * Deletes an existing Businesses model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }

    /**
     * Finds the Businesses model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Businesses the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Businesses::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
