<?php

namespace app\modules\businesses\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\businesses\models\Businesses;
use app\models\MongoDate;

/**
 * BusinessesSearch represents the model behind the search form about `app\modules\businesses\models\Businesses`.
 */
class BusinessesSearch extends Businesses
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'comp_id', 'reg_id', 'reg_date', 'reg_type', 'reg_capital', 'comp_status', 'state_date', 'industry_group', 'industrial_estate', 'trade_name', 'tsic', 'pd_text', 'addresses', 'loc', 'group_status', 's_name', 'name', 'organization_type', 'telephone', 'fax', 'email', 'url', 'cliam_status', 'cliamer', 'logo', 'cover', 'summary', 'social', 'last_update', 'card_update', 'lastModified', 'business_size'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Businesses::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $date='';
        $comp_status=['like','comp_status',$this->comp_status];
        $industry_group=['like','industry_group',$this->industry_group];
        
        if(isset($params['BusinessesSearch']))
        {
            if(isset($params['BusinessesSearch']['comp_status']))
            {
                if($params['BusinessesSearch']['comp_status']>0)
                {
                    $comp_status=['comp_status'=>(float)$this->comp_status];
                }
                else
                {
                    $comp_status=['like','comp_status',$this->comp_status];
                }
            }
            
            if(isset($params['BusinessesSearch']['industry_group']))
            {
                if($params['BusinessesSearch']['industry_group']>0)
                {
                    $industry_group=['industry_group'=>(String)$this->industry_group];
                }
            }

            $reg_date= explode('/', $params['BusinessesSearch']['reg_date']); 
            
            if(isset($reg_date[1]))
            {

                $year=substr($reg_date[2],0,4);
                $reg_date[4]=(int)substr($reg_date[2],6,6)+1;
                $date = $reg_date[1].'/'.$reg_date[0].'/'.$year; 
            
            }
        }


        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'comp_id', $this->comp_id])
            ->andFilterWhere(['like', 'reg_id', $this->reg_id])
            ->andFilterWhere(['reg_date'=>MongoDate::timestampToMongoDate(strtotime($date))])
            ->andFilterWhere(['like', 'reg_capital', $this->reg_capital])
            ->andFilterWhere($comp_status)
            ->andFilterWhere(['like', 'state_date', $this->state_date])
            ->andFilterWhere($industry_group)
            ->andFilterWhere(['like', 'industrial_estate', $this->industrial_estate])
            ->andFilterWhere(['like', 'trade_name', $this->trade_name])
            ->andFilterWhere(['like', 'tsic', $this->tsic])
            ->andFilterWhere(['like', 'pd_text', $this->pd_text])
            ->andFilterWhere(['like', 'addresses', $this->addresses])
            ->andFilterWhere(['like', 'loc', $this->loc])
            ->andFilterWhere(['like', 'group_status', $this->group_status])
            ->andFilterWhere(['like', 's_name', $this->s_name])
            ->andFilterWhere(['like', 'name.en', $this->name])
            ->orFilterWhere(['like', 'name.th', $this->name])
            ->andFilterWhere(['like', 'organization_type', $this->organization_type])
            ->andFilterWhere(['like', 'telephone', $this->telephone])
            ->andFilterWhere(['like', 'fax', $this->fax])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'url', $this->url])
            ->andFilterWhere(['like', 'cliam_status', $this->cliam_status])
            ->andFilterWhere(['like', 'cliamer', $this->cliamer])
            ->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'cover', $this->cover])
            ->andFilterWhere(['like', 'summary', $this->summary])
            ->andFilterWhere(['like', 'social', $this->social])
            ->andFilterWhere(['like', 'last_update', $this->last_update])
            ->andFilterWhere(['like', 'card_update', $this->card_update])
            ->andFilterWhere(['like', 'lastModified', $this->lastModified])
            ->andFilterWhere(['like', 'business_size', $this->business_size]);

        return $dataProvider;
    }
}
