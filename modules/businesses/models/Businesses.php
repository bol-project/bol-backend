<?php

namespace app\modules\businesses\models;

use Yii;

/**
 * This is the model class for collection "businesses".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $comp_id
 * @property mixed $reg_id
 * @property mixed $reg_date
 * @property mixed $reg_type
 * @property mixed $reg_capital
 * @property mixed $comp_status
 * @property mixed $state_date
 * @property mixed $industry_group
 * @property mixed $industrial_estate
 * @property mixed $trade_name
 * @property mixed $tsic
 * @property mixed $pd_text
 * @property mixed $addresses
 * @property mixed $loc
 * @property mixed $group_status
 * @property mixed $s_name
 * @property mixed $name
 * @property mixed $organization_type
 * @property mixed $telephone
 * @property mixed $fax
 * @property mixed $email
 * @property mixed $url
 * @property mixed $cliam_status
 * @property mixed $cliamer
 * @property mixed $logo
 * @property mixed $cover
 * @property mixed $summary
 * @property mixed $social
 * @property mixed $last_update
 * @property mixed $card_update
 * @property mixed $lastModified
 */
class Businesses extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'businesses'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'comp_id',
            'reg_id',
            'reg_date',
            'reg_type',
            'reg_capital',
            'comp_status',
            'state_date',
            'industry_group',
            'industrial_estate',
            'trade_name',
            'tsic',
            'pd_text',
            'addresses',
            'loc',
            'group_status',
            's_name',
            'name',
            'organization_type',
            'telephone',
            'fax',
            'email',
            'url',
            'cliam_status',
            'cliamer',
            'logo',
            'cover',
            'summary',
            'social',
            'last_update',
            'card_update',
            'lastModified',
            'business_size',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comp_id', 'reg_id', 'reg_date', 'reg_type', 'reg_capital', 'comp_status', 'state_date', 'industry_group', 'industrial_estate', 'trade_name', 'tsic', 'pd_text', 'addresses', 'loc', 'group_status', 's_name', 'name', 'organization_type', 'telephone', 'fax', 'email', 'url', 'cliam_status', 'cliamer', 'logo', 'cover', 'summary', 'social', 'last_update', 'card_update', 'lastModified', 'business_size'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'comp_id' => 'Comp ID',
            'reg_id' => 'Reg ID',
            'reg_date' => 'Reg Date',
            'reg_type' => 'Reg Type',
            'reg_capital' => 'Reg Capital',
            'comp_status' => 'Comp Status',
            'state_date' => 'State Date',
            'industry_group' => 'Industry Group',
            'industrial_estate' => 'Industrial Estate',
            'trade_name' => 'Trade Name',
            'tsic' => 'Tsic',
            'pd_text' => 'Pd Text',
            'addresses' => 'Addresses',
            'loc' => 'Loc',
            'group_status' => 'Group Status',
            's_name' => 'Business Name EN',
            'name' => 'Business Name TH',
            'organization_type' => 'Organization Type',
            'telephone' => 'Telephone',
            'fax' => 'Fax',
            'email' => 'Email',
            'url' => 'Url',
            'cliam_status' => 'Cliam Status',
            'cliamer' => 'Cliamer',
            'logo' => 'Logo',
            'cover' => 'Cover',
            'summary' => 'Summary',
            'social' => 'Social',
            'last_update' => 'Last Update',
            'card_update' => 'Card Update',
            'lastModified' => 'Last Modified',
            'business_size' => 'Business Size'
        ];
    }
}
