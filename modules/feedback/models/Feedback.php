<?php

namespace app\modules\feedback\models;

use Yii;

/**
 * This is the model class for collection "feedback".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $topic
 * @property mixed $sender_name
 * @property mixed $sender_email
 * @property mixed $message
 * @property mixed $images
 */
class Feedback extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'feedback'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'topic',
            'sender_name',
            'sender_email',
            'message',
            'images',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['topic', 'sender_name', 'sender_email', 'message', 'images'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'topic' => 'Topic',
            'sender_name' => 'Sender Name',
            'sender_email' => 'Sender Email',
            'message' => 'Message',
            'images' => 'Images',
        ];
    }
}
