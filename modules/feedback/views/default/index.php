<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\feedback\controllers\Topic;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\feedback\models\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Feedbacks';
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="feedback-index">
   
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box box-danger">
    
    <div class="box-header with-border">

    <p>
        <?= Html::a('Create Feedback', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>

    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
                        'firstPageLabel'=>'<<',   
                        'prevPageLabel' =>'<', 
                        'nextPageLabel' =>'>',   
                        'lastPageLabel' =>'>>'
            ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'topic',
                'format'=>'html',
                'value' => function($model){
                    return Topic::toString($model->topic);
                }
            ],
            //'topic',
            'sender_name',
            'sender_email',
            //'message',
            // 'images',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view}'],
        ],
    ]); ?>
    </div>
    </div>
</div>
