<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\modules\feedback\controllers\Topic;
/* @var $this yii\web\View */
/* @var $model app\modules\feedback\models\Feedback */

$this->title =Topic::toString($model->topic);
$this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$url = 'http://smelink.animation-genius.com:3003/?username='.$session['username'].'&role='.$session['role'].'&room_id='.$roomId;
echo $url;
$iframe = '<iframe 
                width="100%" height="600"
                src='.$url.'
                <p>Your browser does not support iframes.</p>
            </iframe>';
?>

<div class="feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="box box-danger">

        <!--<div class="box-header with-border">
        
            <p>
                <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </p>

        </div>-->
    
        <div class="box-body">
            <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                                        [
                                                        'label' => 'Topic',
                                                        'value' =>  Topic::toString($model->topic),
                                                        ],
                                                        'sender_name',
                                                        'sender_email',
                                                        'message',
                                                        //'images',
                                                    ],
                                    ]) ?>
            <?php
                echo    '<span>
                            <label>Images</label>
                        </span>
                        <div style="background-color: white;border-radius: 4px;border: 1px;border-style: solid;border-color: #d2d6de;margin-bottom: 15px;min-height: 35px;line-height: 35px; padding:10px">';
                $items = array();
                if(sizeof($model->images)){
                    foreach ($model->images as $item) {
                        $pic['url'] = $item;
                        $pic['src'] = $item;
                        $pic['options']['title'] = 'Company Registration Docs';
                        array_push($items,$pic);
                    }
                    echo dosamigos\gallery\Gallery::widget(['items' => $items]);
                }else{
                    echo 'Not found Images';
                }
                echo '</div>';
            ?>

        </div>
    
    </div>
    

    <?php
        if($roomId)
            echo $iframe
    ?>

</div>

<script type="text/javascript">
    $(function(){
        $('.gallery-item img').css({'width' : '100px','height' : '100px'});
        //$('.blueimp-gallery').css({'display':'none'});
    })
</script>
