<?php

namespace app\modules\feedback\controllers;

class Topic {
    private static $enum = array(0 => "Feedback/Suggestions", 1 => "Problem", 2 => "Buy Report", 3 => "Buy Ads");

    public static $FEEDBACK = 0;
    public static $PROBLEM = 1;
    public static $REPORT = 2;
    public static $ADS = 3;

    public function getArrayData(){
        return self::$enum;
    }

    public static function toString($ordinal) {
        return self::$enum[$ordinal];
    }
}
