<?php

namespace app\modules\feedback\controllers;

use Yii;
use app\modules\feedback\models\Feedback;
use app\modules\feedback\models\FeedbackSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\users\models\Users;
use app\modules\feedback\controllers\Topic;

/**
 * DefaultController implements the CRUD actions for Feedback model.
 */
class DefaultController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Feedback models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FeedbackSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Feedback model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        $feedback = Feedback::find() 
                    ->where(['_id' => $id]) 
                    ->one();

        $roomId = substr(md5($feedback->sender_email.$feedback['topic']), 0, 24);
        
        $room = $this->findRoomId($roomId);

        if(isset($room)){
            $room = json_decode($room, true);
            if($room['success']){
            }else{
                $user = Users::find()->where(['email' => $feedback['sender_email']])->one();
                $senderEmail = $feedback['sender_email'];
                $senderId = (String)$user['_id'];
                $topic = $feedback['topic'];
                $responseCreateChatRoom = $this->requestCreateChatRoom($roomId,$senderEmail,$senderId,$topic);
                if(json_decode($responseCreateChatRoom, true)['success']){  }
                else{
                    $roomId = null;
                }
            }
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'session' => Yii::$app->session,
            'roomId' => $roomId 
        ]);
    }

    /**
     * Creates a new Feedback model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Feedback();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Feedback model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Feedback model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Feedback model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Feedback the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Feedback::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function requestCreateChatRoom($roomId,$senderEmail,$senderId,$topic){
        $data = array(
            "room" => [
                "_id"=> $roomId,
                "name"=> 'CS '.Topic::toString($topic),
                "type"=> 2,
                "members"=> [[  '_id'=> $senderId,
                                'username'=> $senderEmail, ]]
            ]
        );

        $data = str_replace("\\","",json_encode($data));

        $curl = curl_init();
        curl_setopt_array($curl, array(
        //CURLOPT_PORT => "3003",
        CURLOPT_URL => Yii::$app->params['config']['apiChat']."/api/group/private_group/create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "x-api-key: smelink-chat1234"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return null;
        } else {
            return $response;
        }
    }

    public function findRoomId($roomId){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        //CURLOPT_PORT => "3003",
        CURLOPT_URL => Yii::$app->params['config']['apiChat']."/api/chatroom/?room_id=".$roomId,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "x-api-key: smelink-chat1234"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return null;
        } else {
            return $response;
        }
    }
}
