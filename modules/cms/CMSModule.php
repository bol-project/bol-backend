<?php

namespace app\modules\cms;

/**
 * cms module definition class
 */
class CMSModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\cms\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'banner' => [
                'class' => 'app\modules\banner\BannerModule',
            ],
            'explore' => [
                'class' => 'app\modules\marketing\modules\cms\modules\explore\ExploreModule',
            ],
            'news' => [
                'class' => 'app\modules\marketing\modules\cms\modules\news\NewsModule',
            ],
            'howto' => [
                'class' => 'app\modules\marketing\modules\cms\modules\howto\HowtoModule',
            ],
            'support' => [
                'class' => 'app\modules\marketing\modules\cms\modules\support\SupportModule',
            ],
            'template' => [
                'class' => 'app\modules\marketing\modules\cms\modules\template\TemplateModule',
            ],
        ];
        // custom initialization code goes here
    }
}
