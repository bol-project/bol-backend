<?php

namespace app\modules\admin;

/**
 * AdminModule module definition class
 */
class AdminModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
         $this->modules = [
        
            'role' => [
                'class' => 'app\modules\admin\modules\roles\roles',
            ],
            'modules' => [
                'class' => 'app\modules\admin\modules\modules\modules',
            ], 
          

        ];
    }
}
