<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\modules\roles\models\Roles */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="roles-form">

	<div class="box box-danger">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="box-body">
    
    <?= $form->field($model, 'rolename') ?>

    <?= $form->field($model, 'descripiton') ?>
    
    </div>

    <div class="box box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
     	<?= Html::a('Cancel', ['/admin/role'], ['class'=>'btn btn-danger']) ?>    
     
    </div>

    <?php ActiveForm::end(); ?>
    </div>
    
    </div>

</div>
