<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\modules\admin\modules\roles\models\Roles */

$this->title = 'Update Roles: ' . $model->rolename.'_Admin';
$this->params['breadcrumbs'][] = ['label' => 'Roles', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->rolename, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="roles-form">

    <h1><?= Html::encode($this->title) ?></h1>


    <div class="box box-danger">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="box-body">
    
    <?= $form->field($model, 'rolename') ?>

    <?= $form->field($model, 'descripiton') ?>

    <table class="table table-striped table-bordered">
    <thead>
          <tr rowspan="2" >
                <th>Modulename</th>
                <th colspan="5">
                 <center>member</center>
                </th>
          </tr>

          <tr>
                <th></th>
                <th>view</th>
                <th>Create</th>
                <th>Edit</th>
                <th>Export</th>
                <th>Approve</th>
          </tr>

    </thead>
    <tbody>

            <?php
              foreach ($dataProvider->models as $model)
              {
            ?>
            <tr>

                    <td>
                    <?= $model->modulename;?>
                    </td>
                    
                    <td> 
                    <?=Html::checkbox('role['.$model->_id.'][admin][View]',
                    (isset($roledata[$model->modulename]['admin']['View'])&&$roledata[$model->modulename]['admin']['View']==1),
                    array('class' => 'test','value'=>1));?>           
                    </td>
                    
                    <td> 
                    <?=Html::checkbox('role['.$model->_id.'][admin][Create]',
                        (isset($roledata[$model->modulename]['admin']['Create'])&&$roledata[$model->modulename]['admin']['Create']==1),
                        array('class' => 'test','value'=>1));?>           
                    </td>
                    
                    <td> 
                    <?=Html::checkbox('role['.$model->_id.'][admin][Edit]',
                    (isset($roledata[$model->modulename]['admin']['Edit'])&&$roledata[$model->modulename]['admin']['Edit']==1),
                    array('class' => 'test','value'=>1));?>           
                    </td>

                    <td> 
                    <?=Html::checkbox('role['.$model->_id.'][admin][Export]',
                    (isset($roledata[$model->modulename]['admin']['Export'])&&$roledata[$model->modulename]['admin']['Export']==1),
                    array('class' => 'test','value'=>1));?>           
                    </td>
                    
                    <td> 
                    <?=Html::checkbox('role['.$model->_id.'][admin][Approve]',
                    (isset($roledata[$model->modulename]['admin']['Approve'])&&$roledata[$model->modulename]['admin']['Approve']==1),
                    array('class' => 'test','value'=>1));?>           
                    </td>

            </tr>
            <?php 
              }
             ?>


    </tbody>
    </table>

    
    </div>




    <div class="box box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?= Html::a('Cancel', ['/admin/role'], ['class'=>'btn btn-danger']) ?>
    </div>

    </div>

    <?php ActiveForm::end(); ?>
    
    </div>
