<?php

namespace app\modules\admin\modules\roles\controllers;

use Yii;
use app\modules\admin\modules\modules\models\Modules;
use app\modules\admin\modules\modules\models\ModulesSearch;
use app\modules\admin\modules\roles\models\Roles;
use app\modules\admin\modules\roles\models\RolesSearch;
use app\modules\admin\modules\roles\models\Permission;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\AdminController;
use yii\data\ActiveDataProvider;


/**
 * DefaultController implements the CRUD actions for Roles model.
 */
class DefaultController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Roles models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RolesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Roles model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Roles model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Roles();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Roles model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
 		$searchModel = new modulesSearch();
        //$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider = new ActiveDataProvider([
                                                'query' => Modules::find(),
                                                'pagination' => array('pageSize' => 50),
                                              ]);


  		$Permission=$this->findModelPermission($id);      
       
        foreach ($Permission as $value) {
        
        	$module=Modules::find()->where(['_id' => $value['module_id']])->one();
		   	
        	$roledata[$module['modulename']][$value['group_user']] = [
					        									 	'View'=>$value['view'],
					        										'Create'=>$value['create'],
					        										'Edit'=>$value['edit'],
					        										'Export'=>$value['export'],
					        										'Approve'=>$value['approve'],	
        										                  ];
          
        }
      
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
       		
       
        Permission::deleteAll(["role_id"=>$id]);
        if(isset($_POST['role'])){
         foreach ($_POST['role'] as $modules => $role) {
      				
         	foreach ($role as $group_user => $value) {

         		 	$modelpms = new Permission();
    	   			$modelpms->role_id=$id;
    	   			$modelpms->module_id=$modules;
    	   			$modelpms->group_user=$group_user;
    	   			
    	   		
    	   			if(isset($value['View']))
    	   			$modelpms->view=1;
    	   			else
    	   			$modelpms->view=0;

	    	   		
    	   			if(isset($value['Create']))
	    	   		$modelpms->create=1;
    	   			else
	    	   		$modelpms->create=0;


    	   			if(isset($value['Edit']))
    	   			$modelpms->edit=1;
    	   			else
    	   			$modelpms->edit=0;


    	   			if(isset($value['Export']))
    	   			$modelpms->export=1;
    	   			else
    	   			$modelpms->export=0;

    	   			if(isset($value['Approve']))
    	   			$modelpms->approve=1;
    	   			else
    	   			$modelpms->approve=0;

            		
    	   			$modelpms->save();
           		}
         	
         	}
        }
         	return $this->redirect(['view', 'id' => (string)$id]);
      
        } else {
        	
    
            return $this->render('update', [
                'model' => $model,
                'searchModel' => $searchModel,
            	'dataProvider' => $dataProvider,
            	'roledata'=>isset($roledata)?$roledata:[]
            ]);
        }
    }


    /**
     * Deletes an existing Roles model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */


    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    /**
     * Finds the Roles model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Roles the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Roles::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findModelPermission($id){
    	 if (($model = Permission::findAll(['role_id'=>$id])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }	
    }
}
