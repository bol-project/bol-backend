<?php

namespace app\modules\admin\modules\roles;

/**
 * roles module definition class
 */
class roles extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\admin\modules\roles\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
