<?php

namespace app\modules\admin\modules\roles\models;

use Yii;

/**
 * This is the model class for collection "Permission".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $role_id
 * @property mixed $module_id
 * @property mixed $group_user
 * @property mixed $view
 * @property mixed $create
 * @property mixed $update
 * @property mixed $export
 * @property mixed $approve
 */
class Permission extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['Content', 'permission'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'role_id',
            'module_id',
            'group_user',
            'view',
            'create',
            'edit',
            'export',
            'approve',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_id', 'module_id', 'group_user', 'view', 'create', 'edit', 'export', 'approve'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'role_id' => 'Role ID',
            'module_id' => 'Module ID',
            'group_user' => 'Group User',
            'view' => 'View',
            'create' => 'Create',
            'edit' => 'Edit',
            'export' => 'Export',
            'approve' => 'Approve',
        ];
    }
}
