<?php

namespace app\modules\admin\modules\roles\models;

use Yii;

/**
 * This is the model class for collection "roles".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $rolename
 * @property mixed $descripiton
 */
class Roles extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['Content', 'roles'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'rolename',
            'descripiton',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rolename',], 'required'],
            [['rolename', 'descripiton'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'rolename' => 'Rolename',
            'descripiton' => 'Descripiton',
        ];
    }
}
