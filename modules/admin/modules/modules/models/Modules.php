<?php

namespace app\modules\admin\modules\modules\models;

use Yii;

/**
 * This is the model class for collection "modules".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $modulename
 * @property mixed $descripiton
 * @property mixed $icon
 * @property mixed $link
 */
class Modules extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['Content', 'modules'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'modulename',
            'slug',
            'descripiton',
            'icon',
            'link',
            'parent_id',
            'num'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['modulename', 'descripiton', 'icon', 'link'], 'safe'],
            [['modulename', 'icon','slug', 'link'], 'required']
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            
            if ($this->isNewRecord) {
            
            $num = Modules::find()->orderBy("num DESC")->one();
            $this->num = $num['num'] + 1;

             // $get_it= new CActiveDataProvider(get_class($this), 
             // array('criteria'=>$criteria,));

             // $got_it=$get_it->getData();

             // $num=$got_it[0]['Num'];

             // $this->num=(int)$num+1;
            }
               return true;
            
     }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'modulename' => 'Modulename',
            'slug'=>'Slug',
            'descripiton' => 'Descripiton',
            'icon' => 'Icon',
            'link' => 'Link',
        ];
    }
}
