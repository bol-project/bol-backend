<?php

namespace app\modules\admin\modules\modules\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\admin\modules\modules\models\Modules;

/**
 * modulesSearch represents the model behind the search form about `app\modules\Admin\modules\modulesmanagement\models\modules`.
 */
class ModulesSearch extends modules
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id','slug', 'modulename', 'descripiton','icon','link'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = modules::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'modulename', $this->modulename])
            ->andFilterWhere(['like', 'descripiton', $this->descripiton])
            ->andFilterWhere(['like', 'icon', $this->icon])
            ->andFilterWhere(['like', 'link', $this->link]);
        return $dataProvider;
    }
}
