<?php

namespace app\modules\Admin\modules\modules\models;

use Yii;

/**
 * This is the model class for collection "menu".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $name
 * @property mixed $slug
 * @property mixed $icon
 * @property mixed $link
 * @property mixed $parent_id
 */
class Slug
{
     public static function getActivateSlug() {
         $activate_slug = [
            ['value' => 'cms', 'label' => 'cms'],
            ['value' => 'admin', 'label' => 'admin'],
            ['value' => 'marketing', 'label' => 'marketing'],
        ];

        return $activate_slug;
    }

}
