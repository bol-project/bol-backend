<?php

namespace app\modules\Admin\modules\modules\models;

use Yii;

/**
 * This is the model class for collection "menu".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $name
 * @property mixed $slug
 * @property mixed $icon
 * @property mixed $link
 * @property mixed $parent_id
 */
class Menu extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'menu'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'name',
            'slug',
            'icon',
            'link',
            'parent_id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'slug', 'icon', 'link', 'parent_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'icon' => 'Icon',
            'link' => 'Link',
            'parent_id' => 'Parent ID',
        ];
    }
}
