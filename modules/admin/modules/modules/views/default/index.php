<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\Admin\modules\modules\models\moduleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Modules';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="modules-index">

   
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
   
    <div class="box box-danger">

    <div class="box-header with-border"> 
    <p>
        <?= Html::a('Create Modules', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
                        'firstPageLabel'=>'<<',   
                        'prevPageLabel' =>'<', 
                        'nextPageLabel' =>'>',   
                        'lastPageLabel' =>'>>'
            ],
      
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'modulename',
            'slug',
            'descripiton',
            'icon',
            'link',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
    </div>
</div>
