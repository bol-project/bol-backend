<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\modules\admin\modules\modules\models\Slug;
use kartik\select2\Select2;
use yii\web\JsExpression;
/* @var $this yii\web\View */
/* @var $model app\modules\Admin\modules\modules\models\modules */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="modules-form">
	
	<div class="box box-danger">

    <div class="box-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'modulename') ?>

    <?= $form->field($model, 'descripiton') ?>

   
    <?php
        $data =  ArrayHelper::map(Slug::getActivateSlug(), 'value','label');
        echo $form->field($model, 'slug')->widget(Select2::classname(), [
            'data' => $data,
            'options' => ['placeholder' => 'Select a slug...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
    ?>

    <?= $form->field($model, 'icon') ?>

    <?= $form->field($model, 'link') ?>



    </div>
    
    <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
             <?= Html::a('Cancel', ['/admin/modules'], ['class'=>'btn btn-danger']) ?>
    </div>


    <?php ActiveForm::end(); ?>
    
    </div>
    </div>

</div>
