<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Json;
use app\models\MongoDate;
use app\modules\users\models\Users;
use app\modules\admin\modules\modules\models\Slug;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\modules\log\models\ActionType;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\log\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs';
$this->params['breadcrumbs'][] = $this->title;
?>

  

<div class="log-index">

    <!-- <?php echo $this->render('_search', ['model' => $searchModel]); ?>-->
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box box-danger">
    
    <div class="box-header with-border">
    <p>
     <!--   <?= Html::a('Create Log', ['create'], ['class' => 'btn btn-success']) ?>  -->
    </p>
    </div>
    <div class="box-body">
    <?= GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
                        'firstPageLabel'=>'<<',   
                        'prevPageLabel' =>'<', 
                        'nextPageLabel' =>'>',   
                        'lastPageLabel' =>'>>'
            ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
            'attribute' => 'action',
            'filter' => Html::activeDropDownList($searchModel, 'action', ArrayHelper::map(ActionType::getActionType(), 'value','label'),
                                                                        ['class'=>'form-control','prompt' => 'Select Action']
                                                ),
            ],
            [
                'label' => 'Name',
                'attribute' => 'user_id',
                'format'=>'html',
                'value' => function($model){
                    $user = Users::findOne($model->user_id);
                    if($user){
                        $name = $user['first_name'].' '.$user['last_name'];
                        return $name;
                    }else{
                        return null;
                    }
                }
            ],
            'useragent',
            'browser',
            [ 
                 'label' => 'geoIp',
                 'attribute' => 'geoIp',
                 'format'=>'html',
                 'value'=> function($model){
                    
                   if($model->geoIp){
                   return substr($model->geoIp,0,20)."..."; // 12345
                  }
                 }
             ],
            'os',
            'platform',
            'version',
             [ 
                 'label' => 'deviceInfo',
                 'attribute' => 'deviceInfo',
                 'format'=>'html',
                 'value'=> function($model)
                 {        
                   if($model->deviceInfo)
                    {
                        return substr($model->deviceInfo,0,20)."...";
                    }
                 }
            ],
            [ 
                'label' => 'Time',
                'attribute' => 'datetime',
                'format'=>'html',
                'value'=> function($model){
                    
                    return Mongodate::mongoDateToString($model->datetime, false);
                }
            ],
            ['class' => 'yii\grid\ActionColumn','template'=>'{view}']
        ]
    ]); ?>
    </div>
    </div>
</div>
