<?php

use yii\helpers\Html;
use app\models\MongoDate;
use yii\widgets\DetailView;
use app\modules\users\models\Users;

/* @var $this yii\web\View */
/* @var $model app\modules\log\models\Log */

$this->title = $model->useragent;
$this->params['breadcrumbs'][] = ['label' => 'Logs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="log-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="box box-danger">

    <div class="box-body">

    <p>
        <!--
        <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>     
        <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        -->    
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'action',
            'useragent',
            [         
            'label' => 'Name',
            'value' => isset(Users::findOne($model->user_id)->first_name) ?
                       Users::findOne($model->user_id)->first_name.' '.Users::findOne($model->user_id)->last_name : null
            ],
            'browser',
            'geoIp',
            'os',
            'platform',
            'version',
            [         
            'label' => 'Datetime',
            'value' => isset($model->datetime) ? MongoDate::mongoDateToString($model->datetime) : ''
            ],
            'deviceInfo'
        ],
    ]) ?>

    </div>
    </div>

</div>
