<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\log\models\Log */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="log-form">

    <?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'useragent') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'browser') ?>

    <?= $form->field($model, 'os') ?>

    <?= $form->field($model, 'platform') ?>

    <?= $form->field($model, 'version') ?>

    <?= $form->field($model, 'action') ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
