<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\MongoDate;
use app\modules\users\models\Users;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use app\modules\log\models\ActionType;
use kartik\export\ExportMenu;
//use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\log\models\LogSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Logs report';
$this->params['breadcrumbs'][] = $this->title;
$gridColumns=[
            ['class' => 'yii\grid\SerialColumn'],
            [
            'attribute' => 'action',
            'filter' => Html::activeDropDownList($searchModel, 'action', ArrayHelper::map(ActionType::getActionType(), 'value','label'),
                                                                        ['class'=>'form-control','prompt' => 'Select Action']
                                                ),
            ],
            [
                'label' => 'Name',
                'attribute' => 'user_id',
                'format'=>'html',
                'value' => function($model){
                    $user = Users::findOne($model->user_id);
                    if($user){
                        $name = $user['first_name'].' '.$user['last_name'];
                        return $name;
                    }else{
                        return null;
                    }
                }
            ],
            'useragent',
            'browser',
            [ 
                 'label' => 'geoIp',
                 'attribute' => 'geoIp',
                 'format'=>'html',
                 'value'=> function($model){
                    
                   if($model->geoIp){
                   return substr($model->geoIp,0,20)."..."; // 12345
                  }
                 }
             ],
            'os',
            'platform',
            'version',
             [ 
                 'label' => 'deviceInfo',
                 'attribute' => 'deviceInfo',
                 'format'=>'html',
                 'value'=> function($model)
                 {        
                   if($model->deviceInfo)
                    {
                        return substr($model->deviceInfo,0,20)."...";
                    }
                 }
            ],
            [ 
                'label' => 'Time',
                'attribute' => 'datetime',
                'format'=>'html',
                'value'=> function($model){
                    
                    return Mongodate::mongoDateToString($model->datetime, false);
                }
            ],
            ['class' => 'yii\grid\ActionColumn','template'=>'{view}']
            ];
?>

  

<div class="log-index">

    <!-- <?php echo $this->render('_search', ['model' => $searchModel]); ?>-->
    <h1><?= Html::encode($this->title) ?></h1>

    <div class="box box-danger">
    
    <?php 
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export All',
                'class' => 'btn btn-default'
            ]
        ])
    ?>

    
    <div class="box-header with-border">
    
    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>
    </div>
    </div>
</div>
