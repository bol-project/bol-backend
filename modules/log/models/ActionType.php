<?php
namespace app\modules\log\models;


class ActionType 
{
	public static function getActionType(){
          $actiontype = 
				        [
				            ['value' => 'LOGIN_LOG', 'label' => 'LOGIN_LOG'],
				            ['value' => 'LOGOUT_LOG', 'label' => 'LOGOUT_LOG'],
				            ['value' => 'ACTIVATED_EMAIL', 'label' => 'ACTIVATED_EMAIL'],
							['value' => 'SHARE_BUSINESS', 'label' => 'SHARE_BUSINESS'],
							['value' => 'SHARE_CARD', 'label' => 'SHARE_CARD'],
				        ];

        return $actiontype;
    }
}
?>