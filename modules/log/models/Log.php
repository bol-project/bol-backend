<?php

namespace app\modules\log\models;

use Yii;
/**
 * This is the model class for collection "log".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $action
 * @property mixed $useragent
 * @property mixed $user_id
 * @property mixed $browser
 * @property mixed $geoIp
 * @property mixed $os
 * @property mixed $platform
 * @property mixed $version
 */
class Log extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'log'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'action',
            'useragent',
            'user_id',
            'browser',
            'geoIp',
            'os',
            'platform',
            'version',
            'datetime',
            'deviceInfo'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['action', 'useragent', 'user_id', 'browser', 'geoIp', 'os', 'platform', 'version'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'action' => 'Action',
            'useragent' => 'Useragent',
            'user_id' => 'User ID',
            'browser' => 'Browser',
            'geoIp' => 'Geo Ip',
            'os' => 'OS',
            'platform' => 'Platform',
            'version' => 'Version',
        ];
    }
}
