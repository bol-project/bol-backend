<?php

namespace app\modules\log\models;

use Yii;
use yii\base\Model;
use app\modules\log\models\Log;
use app\models\MongoDate;
use yii\data\ArrayDataProvider;
use yii\data\ActiveDataProvider;
use app\modules\users\models\Users;
use app\modules\users\models\UsersSearch;
/**
 * LogSearch represents the model behind the search form about `app\modules\log\models\Log`.
 */
class LogSearch extends Log
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'action', 'useragent', 'user_id', 'browser', 'geoIp', 'os', 'platform', 'version','datetime','deviceInfo'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Log::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $newData=array(); 

        $minLogDate = '';
        $maxLogDate = '';
        $user_id=[];

       if(isset($params['LogSearch']))
       {           
            $LogDate = explode('/', $params['LogSearch']['datetime']); 
            if(isset($LogDate[1]))
            {
                $minLogDate = $LogDate[1].'/'.$LogDate[0].'/'.$LogDate[2]; 
               // $maxLogDate = $LogDate[1].'/'.(intval($LogDate[0]) + 1).'/'.$LogDate[2];
            }

             $Logname = explode('/', $params['LogSearch']['user_id']);
          
         
            if($Logname[0]!=NUll)
            {
                $nameUser = Users::find()
                            ->Where(['or',
                                         ['like', 'first_name',$Logname[0]],
                                         ['like', 'last_name',$Logname[0]] 
                                    ])
                            ->all();
                
                foreach($nameUser as $key => $value)
                { 
                    $user_id[]=(String)$value['_id'];  
                }
                  
                 
            }
        }





        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'action', (String)$this->action])
            ->andFilterWhere(['like', 'useragent', $this->useragent])
            ->andFilterWhere(['user_id'=>$user_id])
            ->andFilterWhere(['like', 'browser', $this->browser])
            ->andFilterWhere(['like', 'geoIp', $this->geoIp])
            ->andFilterWhere(['like', 'os', $this->os])
            ->andFilterWhere(['like', 'platform', $this->platform])
            ->andFilterWhere(['like', 'version', $this->version])
            ->andFilterWhere(['like', 'deviceInfo', $this->deviceInfo])
            ->andFilterWhere(['>', 'datetime', MongoDate::timestampToMongoDate(strtotime($minLogDate))]);
            //->andFilterWhere(['<', 'datetime', MongoDate::timestampToMongoDate(strtotime($maxLogDate))]);

        return $dataProvider;
    }
}
