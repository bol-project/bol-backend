<?php

namespace app\modules\contactUs_mkt\controllers;

use Yii;
use app\modules\contactUs_mkt\models\Post;
use app\modules\contactUs_mkt\models\PostSearch;
use app\modules\claims\controllers\RequestStatus;
use app\modules\claims\models\Claims;
use app\modules\users\models\Users;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class DefaultController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionChat($post_id)
    {
        $post = $this->findPostModel($post_id);
        $user = $this->findUserModel($post->sender_id);

        $roomId = null;

        if (isset($post->roomId)) {
            $roomId = $post->roomId;
        }
        else {
            $roomId = substr(md5($user->email.$post->title), 0, 24);
        }
        
        $room = $this->findRoomId($roomId);

        if($room->success){
            $members = $room->result[0]->members;
        } 
        else {
            $post->roomId = $roomId;
            $post->update();

            $members = array();
            array_push($members, [
                '_id'=> (string)$post->sender_id,
                'username'=> $user->email,
                'avatar'=> $user->avatar
            ]);

            // claim data
            $claim = Claims::find()
                ->where(['request_status' => (int)RequestStatus::$VERIFIED])
                ->andWhere(['business_id' => (string)$post->business_id ])
                ->all();
            if (count($claim) > 0) {
                for ($j = 0; $j < count($claim); $j++) { 
                    $user = $this->findUserModel($claim[$j]->user_id);
                    array_push($members, [
                        '_id'=> (string)$user->_id,
                        'username'=> $user->email,
                        'avatar'=> $user->avatar
                    ]);
                }
            }

            $createChatRoom = $this->requestCreateChatRoom($roomId,$post->title,$members);
        }

        return $this->render('chat', [
            'model' => $post,
            'session' => Yii::$app->session,
            'roomId' => $roomId,
            'members' => $members
        ]);
    }
    
    public function actionIndex()
    {
        $searchModel = new PostSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Contact Us'
        ]);
    }

    protected function findPostModel($id)
    {
        if (($model = Post::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findUserModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function requestCreateChatRoom($roomId,$chat_name,$members){
        $data = array(
            "room" => [
                "_id"=> $roomId,
                "name"=> $chat_name,
                "type"=> 2,
                "members"=> $members
            ]
        );

        $data = str_replace("\\","",json_encode($data));

        $curl = curl_init();
        curl_setopt_array($curl, array(
        //CURLOPT_PORT => "3003",
        CURLOPT_URL => Yii::$app->params['config']['apiChat']."/api/group/private_group/create",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/json",
            "x-api-key: smelink-chat1234"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return null;
        } else {
            return json_decode($response);
        }
    }

    public function findRoomId($roomId){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        //CURLOPT_PORT => "3003",
        CURLOPT_URL => Yii::$app->params['config']['apiChat']."/api/chatroom/?room_id=".$roomId,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "postman-token: c0a7b45b-c751-caaf-10f6-fd02ec710428",
            "x-api-key: smelink-chat1234"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return null;
        } else {
            return json_decode($response);
        }
    }
}