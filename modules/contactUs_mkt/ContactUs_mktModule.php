<?php

namespace app\modules\contactUs_mkt;

class ContactUs_mktModule extends \yii\base\Module
{
    public $controllerNamespace = 'app\modules\contactUs_mkt\controllers';

    public function init()
    {
        parent::init();
    }
}
