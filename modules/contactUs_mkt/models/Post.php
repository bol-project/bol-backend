<?php

namespace app\modules\contactUs_mkt\models;

use Yii;
use app\models\Posts;

class Post extends Posts
{
    public function attributes()
    {
        $attribute = Posts::attributes();
        array_push($attribute, 'business_name_th', 'business_name_en', 'user_name', 'roomId');
        return $attribute;
    }
    // /**
    //  * @inheritdoc
    //  */
    // public function attributeLabels()
    // {
    //     return [
    //         'business_name' => 'Business Name'
    //     ];
    // }
}