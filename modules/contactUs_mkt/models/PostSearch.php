<?php

namespace app\modules\contactUs_mkt\models;

use Yii;
use yii\base\Model;
use app\modules\contactUs_mkt\models\Post;
use app\modules\businesses\models\Businesses;
use app\modules\users\models\Users;
use app\models\MongoDate;
use app\models\Date;

use yii\data\ArrayDataProvider;

class PostSearch extends Post
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [Post::attributes(), 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Post::find()->all();
        $newData = array();
        $removeData = array();
        
        for ($i = 0; $i < count($query); $i++) { 
            // add business name to each row
            $business = Businesses::findOne($query[$i]->business_id);
            if(isset($business) && isset($business['name'])){
                $business_name_th = $business['name']['th'];
                $business_name_en = $business['name']['en'];
            }
            $query[$i]->business_name_th = isset($business_name_th) ? $business_name_th : '';
            $query[$i]->business_name_en = isset($business_name_en) ? $business_name_en : '';

            // add user full name to each row
            $user = Users::findOne($query[$i]->sender_id);
            if(isset($user) && isset($user['first_name']) && isset($user['last_name'])){
                $user_name = $user['first_name'].' '.$user['last_name'];
            }
            $query[$i]->user_name = isset($user_name) ? $user_name : '';

            // edit date
            $query[$i]->createAt = Mongodate::mongoDateToString($query[$i]->createAt, true);
            array_push($newData, $query[$i]);
            if (count($params) > 1) {
                $this->load($params);
                $filters = $params['PostSearch'];
                foreach ($filters as $key => $value) {
                    if (strlen($value) > 0) {
                        if ($key == 'title' || $key == 'business_name_th' || $key == 'business_name_en' || $key == 'user_name') {
                            if (!preg_match("/".$value."/i", $query[$i]->$key)) {
                                $removeData[$i] = '';
                                break;
                            }
                        }
                        if ($key == 'createAt') {
                            list($start_date, $end_date) = explode(' - ', $value);
                            if (!(Date::validateDateFormat($start_date) && Date::validateDateFormat($end_date))) {
                                continue;
                            }
                            $start_date = explode('/', $start_date);
                            $start_date = strtotime($start_date[1].'/'.$start_date[0].'/'.$start_date[2]);

                            $this_date = explode('/', $query[$i]->createAt);
                            $this_date = strtotime($this_date[1].'/'.$this_date[0].'/'.$this_date[2]);

                            $end_date = explode('/', $end_date);
                            $end_date = strtotime($end_date[1].'/'.(intval($end_date[0]) + 1).'/'.$end_date[2]);

                            if (!($this_date >= $start_date && $this_date < $end_date)) {
                                $removeData[$i] = '';
                                break;
                            }
                        }
                    }
                }
            }
        }
        if (count($removeData) > 0) {
            $newData = array_diff_key($newData, $removeData);
        }
        $dataProvider = new ArrayDataProvider([ 
            'allModels' => $newData, 
            'sort' => [
                'attributes' => Post::attributes(),
            ],
            'pagination' => [ 
                'pageSize' => 10, 
            ] 
        ]);

        return $dataProvider;
    }
}