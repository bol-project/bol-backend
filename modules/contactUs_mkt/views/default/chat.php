<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\bootstrap\Modal;

$this->title = $model->title;
// $this->params['breadcrumbs'][] = ['label' => 'Feedbacks', 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;

?>
<div class="feedback-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="box box-danger">

        <div class="box-header with-border">

        </div>
    
    <div class="box-body">
    
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            [
                'label' => 'Sender E-mail',
                'value' =>  $members[0]->username,
            ],
            'body',
            [
                'label' => 'Number of Clamer and Sub-claimer',
                'format' => 'raw',
                'value' => call_user_func(function($model,$members){
                        Modal::begin([
                            'header' => '<h3 >List of Clamer and Sub-claimer</h3>',
                            'headerOptions' => ['style' => 'color: white; background-color: #3c8dbc;'],
                            'options' => ['id' => 'claimer-list'],
                            'footer' => '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>'
                        ]);
                            echo '<table class="table table-striped">';
                                echo '<thead>';
                                    echo '<tr>';
                                        echo '<th>#</th>';                                            
                                        echo '<th>Avatar</th>';
                                        echo '<th>E-mail</th>';
                                    echo '</tr>';
                                echo '</thead>';
                                echo '<tbody>';                               
                                    for ($i = 1; $i < count($members); $i++) {
                                        echo '<tr>';
                                            echo '<td>'.$i.'</td>';               
                                            if(!!$members[$i]->avatar){
                                                echo '<td><img src="'.$members[$i]->avatar.'" width = "70px"/></td>';
                                            }else{
                                                echo '<td><img src="/web/images/userdefault.png" width = "70px"/></td>';
                                            }                                         
                                            echo '<td>'.$members[$i]->username.'</td>';
                                        echo '</tr>';
                                    }
                                echo '</tbody>';
                            echo '</table>';

                        Modal::end();
                        
                                
                        if(count($members) > 1) {
                            return '<a data-toggle="modal" data-target="#claimer-list" style="cursor: pointer; ">'.(count($members) - 1).'</a>';
                        }else{
                            return 0;
                        }
                        
                },$model,$members)

            ],
        ],
    ]) ?>

    </div>
    
    </div>
    <iframe 
        width="100%" height="600"
        src="http://smelink.animation-genius.com:3003/?username=<?= $session['username'] ?>&role=<?= $session['role'] ?>&room_id=<?= $roomId ?>">
        <p>Your browser does not support iframes.</p>
    </iframe>


</div>
