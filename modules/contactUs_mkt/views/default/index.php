<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use jino5577\daterangepicker\DateRangePicker;
use app\models\MongoDate;
use app\modules\businesses\models\Businesses;

$this->title = $title;
// $this->params['breadcrumbs'][] = $this->title;
// $this->params['breadcrumbs'] = ["Marketing", "CMS", $this->title];
?>
<div class="contents-index">
    <section>
        <h1 style="margin:0 0 10px 0;"><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </section>

    <div class="box box-danger">
    
        <div class="box-header with-border">
            <!--create button-->
        </div>

        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'title',
                    ],
                    [
                        'attribute' => 'createAt',
                        'format'=>'html',
                        'filter'=>DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'createAt',
                            'pluginOptions' => [
                                'autoUpdateInput' => false,
                                'locale' => [
                                    'format' => 'DD/MM/YYYY'
                                ]
                            ]
                        ])
                    ],
                    [
                        'label' => 'Business Name(th)',
                        'attribute' => 'business_name_th',
                    ],
                    [
                        'label' => 'Business Name(en)',
                        'attribute' => 'business_name_en'
                    ],
                    [
                        'label' => 'User Name',
                        'attribute' => 'user_name',
                    ],
                    [
                        'label' => 'Message',
                        'attribute' => 'body',
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{chat}',
                        'buttons' => [
                            'chat' => function ($url, $model) {     
                                return Html::a('<i class="fa fa-weixin" aria-hidden="true"></i>', $url, [
                                    'title' => Yii::t('yii', 'Chat'),
                                ]);
                            }
                        ],
                        'urlCreator' => function ($action, $model, $key, $index) {
                            if ($action === 'chat') {
                                // if (isset($model->roomId)) {
                                //     $url = Url::to(['chat', 'id' => $model->roomId]); // your own url generation logic
                                // }
                                // else {
                                //     $url = Url::to(['init-chat']); // your own url generation logic
                                // }
                                $url = Url::to(['chat', 'post_id' => (string)$model->_id]);
                                return $url;
                            }
                        }
                    ]
                ]
            ]); ?>
        </div>
    </div>
</div>