<?php

use app\models\MongoDate;
use app\modules\claims\controllers\RequestStatus;
use app\modules\claims\controllers\ClaimType;
use app\modules\users\models\Users;
use app\modules\businesses\models\Businesses;

use kartik\export\ExportMenu;
use kartik\grid\GridView;

use jino5577\daterangepicker\DateRangePicker;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
//use yii\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\claims\models\ClaimsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Claims Reports';
$this->params['breadcrumbs'][] = $this->title;
$gridColumns = [
            ['class' => 'kartik\grid\SerialColumn'],
            
            [
                'attribute' => 'request_date',
                'format'=>'html',
                'filter'=>DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'request_date',
                            'pluginOptions' => [
                                'autoUpdateInput' => false,
                                'locale' => [
                                    'format' => 'DD/MM/YYYY'
                                ]
                            ]
                ]),
                'value' => function($model){
                    return MongoDate::mongoDateToString($model->request_date, true);
                }
            ],
            [
                'label' => 'Request Time',
                'format'=>'html',
                'value' => function($model){
                    return MongoDate::mongoDateToString($model->request_date, false, 'H:i:s');
                }
            ],
            [
                'label' => 'Username',
                'attribute' => 'user_id',
                'format'=>'html',
                'value' => function($model){
                    $user = Users::findOne($model->user_id);
                    if(isset($user)){
                        $name = $user['email'];
                        return $name;
                    }else{
                        return null;
                    }
                }
            ],
            [
                'label' => 'First Name',
                'format'=>'html',
                //'attribute' => 'firstname',
                'value' => function($model){
                    $user = Users::findOne($model->user_id);
                    if(isset($user)){
                        $name = $user['first_name'];
                        return $name;
                    }else{
                        return null;
                    }
                }
            ],
            [
                'label' => 'Last Name',
                'format'=>'html',
                //'attribute' => 'lastname',
                'value' => function($model){
                    $user = Users::findOne($model->user_id);
                    if(isset($user)){
                        $name = $user['last_name'];
                        return $name;
                    }else{
                        return null;
                    }
                }
            ],
            // [
            //     'label' => 'Company Name(TH)',
            //     'format'=>'html',
            //     'value' => function($model){
            //         $business = Businesses::findOne($model->business_id);
            //         if(isset($business)){
            //             $name = $business['name']['th'];
            //             return $name;
            //         }else{
            //             return null;
            //         }

            //     }
            // ],
            [
                'label' => 'Company Name(EN)',
                'format'=>'html',
                'attribute'=>'business_id',
                'value' => function($model){
                    $business = Businesses::findOne($model->business_id);
                    if(isset($business)){
                        $name = $business['name']['en'];
                        return $name;
                    }else{
                        return null;
                    }

                }
            ],
            [
                'label' => 'RG no.',
                'format'=>'html',
                'value' => function($model){
                    $business = Businesses::findOne($model->business_id);
                    if(isset($business)){
                        $name = $business['reg_id'];
                        return $name;
                    }else{
                        return null;
                    }

                }
            ],
            // [
            //     'label' => 'Email',
            //     'attribute' => 'user_id',
            //     'format'=>'html',
            //     'value' => function($model){
            //         $user = Users::findOne($model->user_id);
            //         if(isset($user)){
            //             $name = $user['email'];
            //             return $name;
            //         }else{
            //             return null;
            //         }
            //     }
            // ],
            [
                'attribute' => 'request_status',
                'format'=>'html',
                'filter' => Html::activeDropDownList($searchModel, 'request_status', ArrayHelper::map(RequestStatus::getStatus(), 'value','label'),
                                                                        ['class'=>'form-control','prompt' => 'Select Status']
                                                ),
 
                'value' => function($model){
                    if(isset($model->request_status)){
                        if($model->request_status == RequestStatus::$VERIFIED){
                           return 
                                '<small class="label bg-green">'.RequestStatus::toString($model->request_status).'</small>'; 
                        } else if($model->request_status == RequestStatus::$PENDING) {
                            return 
                                '<small class="label bg-yellow">'.RequestStatus::toString($model->request_status).'</small>'; 
                        } else if($model->request_status == RequestStatus::$EXPIRE) {
                            return 
                                '<small class="label bg-gray">'.RequestStatus::toString($model->request_status).'</small>'; 
                        } else {
                            return 
                                '<small class="label bg-red">'.RequestStatus::toString($model->request_status).'</small>'; 
                        }
                    }
                    else return null;
                }
            ],
  
        

            ['class' => 'kartik\grid\ActionColumn', 'urlCreator'=>function(){return '#';},
                'template'=>''],]
?>
<div class="claims-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div class="box box-danger">

    <?php 
    /*
    <div class="box-header with-border">
    <p>
        <?= Html::a('Create Claims', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
    */ 
    ?>

    <?php 
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'target' => ExportMenu::TARGET_BLANK,
            'filename' => 'Claim_report' . date('Y-m-d H:i'),
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export All',
                'class' => 'btn btn-default'
            ]
        ])
    ?>

    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns,
    ]); ?>
    </div>

    </div>
</div>

