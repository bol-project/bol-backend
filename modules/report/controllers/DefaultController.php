<?php

namespace app\modules\report\controllers;

use Yii;
use yii\web\Controller;
use app\modules\users\models\Users;
use app\modules\users\models\UsersSearch;
use app\models\Link;
use app\models\LinkSearch;
use app\modules\claims\models\Claims;
use app\modules\claims\models\ClaimsSearch;
/**
 * Default controller for the `report` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionRegister_report()
    {
  

        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
     
        return $this->render('register_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    

    }

    public function actionLink_report()
    {
        $searchModel = new LinkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
        return $this->render('link_report',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClaim_report()
    {
        $searchModel = new ClaimsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
     
        return $this->render('claim_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);

        
    }

}
