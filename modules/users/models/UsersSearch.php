<?php

namespace app\modules\users\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\users\models\Users;
use app\models\MongoDate;
use app\models\Cards;
use app\modules\claims\models\Claims;
use app\modules\businesses\models\businesses;
use yii\db\Expression;


/**
 * UsersSearch represents the model behind the search form about `app\modules\users\models\Users`.
 */
class UsersSearch extends Users
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'email', 'password', 'gender', 'first_name', 'last_name', 'mobile', 'country_code', 'avatar', 'birthday', 'highest_education', 'institute', 'major', 'occupation', 'facebookId', 'deviceTokens', 'registerDate', 'follower', 'sharedBy', 'defaultCardId', 'linkeds', 'link_requests', 'trash_link_requests', 'status', 'online_status', 'lastModified', 'lastLogedin', 'location','PositionTitle','Claim_Type','login_info',
                'activate','user_info','business_profile','role_contact','business_card','otp'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Users::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $minRegisterDate = '';
        $maxRegisterDate = '';
        $minlastLogedin = '';
        $maxlastLogedin = '';
        $username=['like','email',$this->email];
        $password=['like','email',$this->password];
        $gender=['like','gender',$this->gender];
        $birthday=['like','birthday',$this->birthday];
        $user_info=0;
        $defaultCardId=[];
        $Id=[];
        if(isset($params['UsersSearch'])){
           
            $registerDate = explode('/', $this->registerDate); 
            if(isset($registerDate[1]))
            {
                
                $year=substr($registerDate[2],0,4);
                $registerDate[4]=(int)substr($registerDate[2],6,6)+1;

                $minRegisterDate = $registerDate[1].'/'.$registerDate[0].'/'.$year; 
                $maxRegisterDate = $registerDate[1].'/'.$registerDate[4].'/'.$year;

            }

            $lastLogedin = explode('/', $this->lastLogedin); 
            if(isset($lastLogedin[1]))
            {
                
                $year=substr($lastLogedin[2],0,4);
                $lastLogedin[4]=(int)substr($lastLogedin[2],6,6)+1;

                $minlastLogedin = $lastLogedin[1].'/'.$lastLogedin[0].'/'.$year; 
                $maxlastLogedin = $lastLogedin[1].'/'.$lastLogedin[4].'/'.$year;

            }

            $birthday = explode('/', $this->birthday); 
            if(isset($birthday[1]))
            {
                
                $year=substr($birthday[2],0,4);
                $birthday[4]=(int)substr($birthday[2],6,6)+1;

                $minbirthday = $birthday[1].'/'.$birthday[0].'/'.$year; 
                echo $minbirthday;
            }




            $login_info = $this->login_info;
            if(isset($login_info[0])){
            
                if($login_info[0]==1){
                $password=['password'=>['$exists' => false]];
                }
            }
        
              // $otp = $params['UsersSearch']['otp'];
              // if(isset($otp[0])){
              //   if($otp[0]==1){
              //       echo "string";
              //   }
              // }
            $businesses_profile=$this->business_profile;
            $role_contact=$this->role_contact;
            
            $conditions_bs=['!=','officialBusiness.business_id',null];
            $conditions_rc=['!=','officialContact.officeEmail',null];
            $business_profile[1]=0;
             if(isset($businesses_profile[0])&&($businesses_profile[0]==0))
             {       
                $business_profile[1]=1;
             }else if(isset($businesses_profile[0])&&($businesses_profile[0]==1)) {
                $business_profile[1]=2;
             }else{
                $business_profile[1]=0;
             }
           
            //0 active
            //1 In active
    
        if(isset($role_contact[0])&&($role_contact[0]==0))
        {   

            if($business_profile[1]==1)
                {

                           $id=Users::find()->andWhere(['!=','defaultCardId',null])->all();
                            foreach ($id as $key => $value) 
                                    {
                                         $Cards=Cards::find()
                                            ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
                                            ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
                                            ->andWhere(['!=','officialContact.positionTitle',null])                                           
                                            ->andWhere(['!=','officialContact.officeEmail',null])
                                            ->andWhere(['!=','officialBusiness.business_id',null])
                                            ->one();

                                        $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);                                              
                                    }

                }
                else if($business_profile[1]==2)
                {

                            $id=Users::find()->andWhere(['!=','defaultCardId',null])->all();
                            foreach ($id as $key => $value) 
                                    {
                                         $Cards=Cards::find()
                                            ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
                                            ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
                                           ->andWhere(['=','officialBusiness.business_id',null])
                                            ->one();

                                        $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
                                     }
                                      
               }
              else if($business_profile[1]==0) 
               {


                            $id=Users::find()->andWhere(['!=','defaultCardId',null])->all();
                            foreach ($id as $key => $value) 
                                    {
                                         $Cards=Cards::find()
                                            ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
                                            ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
                                            ->andWhere(['!=','officialContact.positionTitle',null])
                                            ->andWhere(['!=','officialContact.officeEmail',null])
                                            ->one();

                                        $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
                                     }
               }
 
        }
        elseif (isset($role_contact[0])&&($role_contact[0]==1)) 
        { 
            if($business_profile[1]==1)
            {  
                              $id=Users::find()->andWhere(['!=','defaultCardId',null])->all();
                            foreach ($id as $key => $value) 
                                    {
                                         $Cards=Cards::find()
                                            ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
                                            ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
                                            ->andWhere(['!=','officialContact.positionTitle',null])
                                            ->andWhere(['!=','officialContact.officeEmail',null])
                                            ->andWhere(['=','officialBusiness.business_id',null])
                                            ->one();

                                        $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
                                     }
            }
            else if($business_profile[1]==2)
            {
                $defaultCardId=['$exists'=>0];                                                     
            
            }else if($business_profile[1]==0)
            {    
                $defaultCardId=['$exists'=>0];     
            }  
        
        }
        else if($business_profile[1]==1)
            {
                            $id=Users::find()->all();
                            foreach ($id as $key => $value) 
                                    {
                                         $Cards=Cards::find()
                                            ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
                                            ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
                                            ->andWhere(['!=','officialContact.positionTitle',null])
                                            ->andWhere(['!=','officialContact.officeEmail',null])
                                            ->one();

                                        $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
                                     }

            }
        else if($business_profile[1]==2)
            {

                        $id=Users::find()->all();
                            foreach ($id as $key => $value) 
                                    {
                                         $Cards=Cards::find()
                                            ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
                                            ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
                                            ->andWhere(['=','officialBusiness.business_id',null])
                                            ->one();

                                        $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
                                     }
                                                                      
            }
        else if(isset($params['UsersSearch']['PositionTitle'])&&$params['UsersSearch']['PositionTitle']!=""){

                         $Cards=Cards::find()
                                ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
                                ->all();
                        if($Cards)
                        {
                                            
                             foreach ($Cards as $key => $value) 
                                    {      
                                        $defaultCardId[]=new \MongoDB\BSON\ObjectID($value['_id']);                                 
                                    }
                        }

        }
       
        }
       
         $query->andFilterWhere([ '_id'=>$Id])
            ->andFilterWhere(['like','email',$this->email])
            //->andFilterWhere($password)
            ->andFilterWhere(['gender'=>(String)$this->gender])
            ->andFilterWhere(['like','birthday',$this->birthday])
            ->andFilterWhere(['like','first_name',$this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
           //  //->andFilterWhere(['=','country_code',""])
           //  ->andFilterWhere(['like','mobile', $this->mobile])
           
           //  // ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like','highest_education',$this->highest_education])
           //  // ->andFilterWhere(['highest_education'=>$this->highest_education])

           //  // ->andFilterWhere(['like', 'institute', $this->institute])
           //  // ->andFilterWere(['like', 'major', $this->major])
           ->andFilterWhere(['like', 'occupation', $this->occupation])
           //  // ->andFilterWhere(['like', 'facebookId', $this->facebookId])
           //  // ->andFilterWhere(['like', 'deviceTokens', $this->deviceTokens])
            ->andFilterWhere(['>=', 'registerDate', MongoDate::timestampToMongoDate(strtotime($minRegisterDate))])
            ->andFilterWhere(['<', 'registerDate', MongoDate::timestampToMongoDate(strtotime($maxRegisterDate))])
           //  // ->andFilterWhere(['like', 'follower', $this->follower])
           //  // ->andFilterWhere(['like', 'sharedBy', $this->sharedBy])
           ->andFilterWhere(['defaultCardId'=>$defaultCardId])

            // ->andFilterWhere(['like', 'linkeds', $this->linkeds])
            // ->andFilterWhere(['like', 'link_requests', $this->link_requests])
            // ->andFilterWhere(['like', 'trash_link_requests', $this->trash_link_requests])
            ->andFilterWhere(['status'=>isset($this->status)&&($this->status!="")?(int)$this->status:$this->status])
            // ->andFilterWhere(['like', 'online_status', $this->online_status])
            // //->andFilterWhere(['lastModified'=>array('$exists' => false)])
            ->andFilterWhere(['>=', 'lastLogedin', MongoDate::timestampToMongoDate(strtotime($minlastLogedin))])
            ->andFilterWhere(['<', 'lastLogedin', MongoDate::timestampToMongoDate(strtotime($maxlastLogedin))]);

        return $dataProvider; 
        }
}

    //logic search $business_card and $role_contact and $business_profile 
    //     if(isset($business_card[0])&&($business_card[0]==0))
        //     {

        //         if(isset($role_contact[0])&&($role_contact[0]==0))
        //         {


        //                 if($business_profile[1]==1)
        //                 { 
                        
        //                     $Cards=Cards::find()
        //                             ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                             ->all();

        //                             foreach ($Cards as $key => $value) 
        //                             {
        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($value['_id']);
        //                             }
        //                 }
        //                 else if($business_profile[1]==2)
        //                 {
        //                      $id=Users::find()->andWhere(['!=','defaultCardId',null])->all();
        //                     foreach ($id as $key => $value) 
        //                     {
        //                                  $Cards=Cards::find()
        //                                     ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
        //                                     ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                                     ->andWhere(['officialBusiness.business_id'=>['$exists' => 0]])             
        //                                     ->one();

        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
        //                     }
                         
        //                 }
        //                 else if($business_profile[1]==0)
        //                 {
        //                     $id=Users::find()->andWhere(['!=','defaultCardId',null])->all();
        //                     foreach ($id as $key => $value) 
        //                     {
        //                                  $Cards=Cards::find()
        //                                     ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
        //                                     ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                                     ->andWhere(['officialBusiness.business_id'=>['$exists' => 0]])
                                         
        //                                     ->one();

        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
        //                     }
          
        //                 }

           
        //         }
        //         elseif (isset($role_contact[0])&&($role_contact[0]==1)) 
        //         { 
        //                 if($business_profile[1]==1)
        //                 {
        //                     $id=Users::find()->andWhere(['!=','defaultCardId',null])->all();
        //                     foreach ($id as $key => $value) 
        //                     {
        //                                  $Cards=Cards::find()
        //                                     ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
        //                                     ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                                     ->andWhere(['officialContact.positionTitle'=>['$exists' => 0]])
        //                                     ->andWhere(['officialContact.officeEmail'=>['$exists' => 0]])
        //                                     ->one();

        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                            

        //                     }
        //                 } 
        //                 else if($business_profile[1]==2)
        //                 {
        //                      $id=Users::find()->andWhere(['!=','defaultCardId',null])->all();
        //                     foreach ($id as $key => $value) 
        //                     {
        //                                  $Cards=Cards::find()
        //                                     ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
        //                                     ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                                     ->andWhere(['officialContact.positionTitle'=>['$exists' => 0]])
        //                                     ->andWhere(['officialContact.officeEmail'=>['$exists' => 0]])
        //                                     ->andWhere(['officialBusiness.business_id'=>['$exists' => 0]])
        //                                     ->one();

        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
        //                     }
                            
        //                 }                                     
        //         } 
        //         else if($business_profile[1]==1)
        //         {
        //                 $Cards=Cards::find()
        //                         ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                         ->all();

        //                     foreach ($Cards as $key => $value) 
        //                     {
                       
        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($value['_id']);
                                                                      
        //                     }
                       

        //         }elseif ($business_profile[1]==2) {
        //              $defaultCardId=['$exists'=>0];
        //         }
               
        //     }
        //     else if(isset($business_card[0])&&($business_card[0]==1))
        //     { 
                           
        //         if(isset($role_contact[0])&&($role_contact[0]==0))
        //         {       if($business_profile[1]==1)
        //                 {
                        
        //                    $id=Users::find()->andWhere(['defaultCardId'=>['$exists'=>0]])->all();
        //                     foreach ($id as $key => $value) 
        //                     {
        //                                  $Cards=Cards::find()
        //                                     ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
        //                                     ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                                     ->one();

        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
        //                     }
                            
        //                 }
        //                 else if($business_profile[1]==2)
        //                 {

                  
        //                    $id=Users::find()->all();
        //                     foreach ($id as $key => $value) 
        //                     {
        //                                  $Cards=Cards::find()
        //                                     ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
        //                                     ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                                     ->andWhere(['!=','officialContact.positionTitle',null])                                           
        //                                     ->andWhere(['!=','officialContact.officeEmail',null])
        //                                     ->andWhere(['officialBusiness.business_id'=>['$exists' => 0]])
                                           
        //                                     ->one();

        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);

                                                                      
        //                     }
        //                     echo "string";
                            
        //                 }
        //                 else if($business_profile[1]==0){

                            
        //                    $id=Users::find()->andWhere(['=','defaultCardId',null])->all();
        //                     foreach ($id as $key => $value) 
        //                     {
        //                                  $Cards=Cards::find()
        //                                     ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
        //                                     ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                                     ->one();

        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
        //                     }
                          
        //                 }
                              
        //         }
        //         else if (isset($role_contact[0])&&($role_contact[0]==1))
        //         {
           
        //                         if($business_profile[1]==1)
        //                         {
                              

        //                            $id=Users::find()->andWhere(['defaultCardId'=>['$exists'=>0]])->all();
        //                             foreach ($id as $key => $value) 
        //                                     {
                                                
        //                                         $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);                                              
        //                                     }
        //                         } 
        //                         else if($business_profile[1]==2)
        //                         {
        //                              $defaultCardId=['$exists'=>0];   
        //                         }
        //                         else if($business_profile[1]==0)
        //                         {
        //                           $id=Users::find()->andWhere(['!=','defaultCardId',null])->all();
        //                             foreach ($id as $key => $value) 
        //                             {
        //                                  $Cards=Cards::find()
        //                                     ->andWhere(['_id'=>new \MongoDB\BSON\ObjectID($value['defaultCardId'])])
        //                                     ->andWhere(['like','officialContact.positionTitle',(String)$params['UsersSearch']['PositionTitle']])
        //                                     ->andWhere(['=','officialContact.positionTitle',null])                                           
        //                                     ->andWhere(['=','officialContact.officeEmail',null])
        //                                     ->andWhere(['officialBusiness.business_id'=>['$exists' => 0]])
        //                                     ->one();

        //                                 $defaultCardId[]=new \MongoDB\BSON\ObjectID($Cards['_id']);
                                                                      
        //                             }
        //                         }
        //         }
        //         else
        //         {
        //          $defaultCardId=['$exists'=>0];   
                
        //         }
        // }        