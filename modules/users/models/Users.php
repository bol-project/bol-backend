<?php

namespace app\modules\users\models;

use Yii;
use app\models\Links;
use app\models\Cards;


/**
 * This is the model class for collection "users".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $email
 * @property mixed $password
 * @property mixed $gender
 * @property mixed $first_name
 * @property mixed $last_name
 * @property mixed $mobile
 * @property mixed $country_code
 * @property mixed $avatar
 * @property mixed $birthday
 * @property mixed $highest_education
 * @property mixed $institute
 * @property mixed $major
 * @property mixed $occupation
 * @property mixed $facebookId
 * @property mixed $deviceTokens
 * @property mixed $registerDate
 * @property mixed $follower
 * @property mixed $sharedBy
 * @property mixed $defaultCardId
 * @property mixed $linkeds
 * @property mixed $link_requests
 * @property mixed $trash_link_requests
 * @property mixed $status
 * @property mixed $online_status
 * @property mixed $lastModified
 * @property mixed $lastLogedin
 * @property mixed $location
 */
class Users extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'users'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'email',
            'password',
            'gender',
            'first_name',
            'last_name',
            'mobile',
            'country_code',
            'avatar',
            'birthday',
            'highest_education',
            'institute',
            'major',
            'occupation',
            'facebookId',
            'deviceTokens',
            'registerDate',
            'follower',
            'sharedBy',
            'defaultCardId',
            'linkeds',
            'link_requests',
            'trash_link_requests',
            'status',
            'online_status',
            'lastModified',
            'lastLogedin',
            'location',
            'PositionTitle',
            'Claim_Type',
            'login_info',
            'activate',
            'user_info',
            'business_profile',
            'role_contact',
            'business_card',
            'otp'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'password', 'gender', 'first_name', 'last_name', 'facebookId','mobile', 'country_code', 'birthday', 'highest_education',  'occupation',  'deviceTokens', 'registerDate', 'follower', 'sharedBy', 'defaultCardId', 'linkeds', 'link_requests', 'trash_link_requests', 'status', 'online_status', 'lastModified', 'lastLogedin', 'location'], 'safe'],
            [['email', 'password', 'gender', 'first_name', 'last_name', 'mobile',  'birthday'], 'required'],
            [['email'], 'unique']
           
            
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'email' => 'Email',
            'password' => 'Password',
            'gender' => 'Gender',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'mobile' => 'Mobile',
            'country_code' => 'Country Code',
            'avatar' => 'Avatar',
            'birthday' => 'Birthday',
            'highest_education' => 'Highest Education',
            'institute' => 'Institute',
            'major' => 'Major',
            'occupation' => 'Occupation',
            'facebookId' => 'Facebook ID',
            'deviceTokens' => 'Device Tokens',
            'registerDate' => 'Register Date',
            'follower' => 'Follower',
            'sharedBy' => 'Shared By',
            'defaultCardId' => 'Default Card ID',
            'linkeds' => 'Linkeds',
            'link_requests' => 'Link Requests',
            'trash_link_requests' => 'Trash Link Requests',
            'status' => 'Status',
            'online_status' => 'Online Status',
            'lastModified' => 'Last Modified',
            'lastLogedin' => 'Last Logedin',
            'location' => 'Location',
        ];
    }

    public static function findLinkedInUser($model){
        $link_in_user = array();
        for ($i=0; $i < sizeof($model->linkeds); $i++) { 
             $link = Links::find()->from('links')->where(['_id' => $model->linkeds[$i]])->one();

             for ($j=0; $j < sizeof($link) ; $j++) { 
                $card = Cards::find()->from('cards')->where(['_id' => $link->cards[$j]['id']])->one();

                if($card->_user_id != $model->_id) {
                    $user = Users::find()->from('users')->where(['_id' => $card->_user_id])->one();  
                    if (isset($user))
                        array_push($link_in_user, $user->first_name.' '.$user->last_name);
                }
             }
        }
        return $link_in_user;
    }






}
