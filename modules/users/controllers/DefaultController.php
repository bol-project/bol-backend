<?php

namespace app\modules\users\controllers;

use Yii;
use app\modules\users\models\Users;
use app\modules\users\models\UsersSearch;
use app\models\LinkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\MongoDate;
use app\models\Links;
use app\models\Cards;
use yii\web\UploadedFile;
use yii\data\ActiveDataProvider;
use app\modules\claims\models\Claims;
use app\modules\businesses\models\Businesses;
use app\controllers\AdminController;





/**
 * DefaultController implements the CRUD actions for Users model.
 */
class DefaultController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Users models.
     * @return mixed
     */

    public function actionIndex($gender="",$status="")
    {
        $searchModel = new UsersSearch();
        
        $params=Yii::$app->request->queryParams;
         
        if(isset($params['UsersSearch']))
         {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         }
         else
         {
            $dataProvider = $searchModel->search([$searchModel->formName() => ['gender' => $gender,'status'=>$status]]);

         }
       
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    }

   
    /**
     * Displays a single Users model.
     * @param integer $_id
     * @return mixed
     */
    // public function actionView($id)
    // {
    //     $model = $this->findModel($id);

        
    //     if (!!$model->location){
    //         $location_type = $model->location['type'];
    //         $longitude = $model->location['coordinates'][0];
    //         $latitude = $model->location['coordinates'][1];
    //         $location_card_id = $model->location['card_id'] ;
        
    //     } else{
    //         $location_type = '';
    //         $longitude = '';
    //         $latitude = '';   
    //         $location_card_id = '';
            
    //     }

      
    //     $link_in_user  = Users::findLinkedInUser($model);


    //     return $this->render('view', [
    //         'model' => $model,
    //         'location_type' => $location_type,
    //         'longitude' => $longitude,
    //         'latitude' => $latitude,
    //         'location_card_id' => $location_card_id,
    //         'link_in_user' => $link_in_user
    //     ]);
    // }

    /**
     * Creates a new Users model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    // public function actionCreate()
    // {
    //     $model = new Users();
    //     $location_type = '';
    //     $longitude = '';
    //     $latitude = '';  
    //     $location_card_id = '' ;

    //     $model->registerDate = Mongodate::now();
    //     if ($model->load(Yii::$app->request->post())) {
    //         $model->avatar = UploadedFile::getInstance($model, 'avatar');
    //         $model->avatar = $this->uploadAvatar($model);
           
    //        if ($model->save()){
    //             return $this->redirect(['view', 'id' => (string)$model->_id]);
    //        }else {
    //            $model->avatar = '';
    //             return $this->render('create', [
    //             'model' => $model,
    //             'location_type' => $location_type,
    //             'longitude' => $longitude,
    //             'latitude' => $latitude,
    //             'location_card_id' => $location_card_id,
                
    //         ]);
    //        }

    //     } else {
    //         return $this->render('create', [
    //             'model' => $model,
    //             'location_type' => $location_type,
    //             'longitude' => $longitude,
    //             'latitude' => $latitude,
    //             'location_card_id' => $location_card_id,
                
    //         ]);
    //     }
    // }

    /**
     * Updates an existing Users model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $oldPassword = $model->password;

        if (!!$model->location){
            $location_type = $model->location['type'];
            $longitude = $model->location['coordinates'][0];
            $latitude = $model->location['coordinates'][1];
            $location_card_id = $model->location['card_id'] ;
            
            
        } else{
            $location_type = '';
            $longitude = '';
            $latitude = ''; 
            $location_card_id = '' ;
            
        }

        $model->lastModified = Mongodate::now();
        $claims = Claims::find()->where(['user_id' => (String)$model->_id])->all();
        
             
        if ($model->load(Yii::$app->request->post())) {
            if ($model->password != $oldPassword) {
                $model->password = md5($model->password);
            }
            
            $model->avatar = UploadedFile::getInstance($model, 'avatar');
            $model->avatar = $this->uploadAvatar($model);
      
            Yii::$app->getSession()->setFlash('alert',[
                'body'=>'Update success.',
                'options'=>['class'=>'alert-success']
            ]);
      
            if($model->update()){
                return $this->redirect([
                    'update', 'id' => (string)$model->_id,]);

            }
        
        } else {
            return $this->render('update', [
                'model' => $model,
                'location_type' => $location_type,
                'longitude' => $longitude,
                'latitude' => $latitude,
                'location_card_id' => $location_card_id,
                'claims' => $claims,
                           
            ]);
        }
    }



    /**
     * Deletes an existing Users model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    // public function actionDelete($id)
    // {
    //     $this->findModel($id)->delete();

    //     return $this->redirect(['index']);
    // }
    public function actionRegister_report()
    {
  

        $searchModel = new UsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
     
        return $this->render('register_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);
    

    }

    public function actionLink_report(){
        $searchModel = new LinkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
        return $this->render('link_report',[
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function uploadAvatar($model) {
            
            if(!!$model->avatar){
                $api = Yii::$app->params['config']['api'];
                $file = curl_file_create($model->avatar->tempName, $model->avatar->type, $model->avatar->name);
                $post = array('file'=>$file);

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $api.'/users/uploadPicture');
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: multipart/form-data', 'x-master-key: smelink1234'));
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $result=curl_exec ($ch);
                curl_close ($ch);
                $avatar_path = json_decode($result)->result->path;
                $avatar_url = $api.$avatar_path;

                return $avatar_url ;

            } else {
                    return $model->getOldAttribute('avatar') ;
             }
    }



    /**
     * Finds the Users model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Users the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Users::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
