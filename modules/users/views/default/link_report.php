<?php

use app\models\MongoDate;
use yii\helpers\Html;
//use yii\grid\GridView;
use app\models\Cards;
use app\models\CATALOG;
use app\modules\users\models\Users;

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use jino5577\daterangepicker\DateRangePicker;
use app\modules\businesses\models\Businesses;
use yii\helpers\ArrayHelper;
use app\modules\users\helpers\Choice;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\claims\models\ClaimsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Link Report';
$this->params['breadcrumbs'][] = $this->title;
$gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
            [ 
                    'label' => 'Date',
                    'attribute' => '_id', 
                    'filter'=>DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => '_id',
                            'pluginOptions' => [
                                'autoUpdateInput' => false,
                                'locale' => [
                                    'format' => 'DD/MM/YYYY'
                                ]
                            ]
                    ]),
                    'value'=> function($model){
                        $link = $model['_id'];
                        $timestamp = hexdec(substr($link, 0, 8));
                        $date = MongoDate::mongoDateToString(MongoDate::timestampToMongoDate($timestamp), true);
                        return $date;

                        
                    }
                ],
                [ 
                    'label' => 'Time',
                    'value'=> function($model){
                        $link = $model['_id'];
                        $timestamp = hexdec(substr($link, 0, 8));
                        $date = MongoDate::mongoDateToString(MongoDate::timestampToMongoDate($timestamp), false, 'H:i:s');
                        return $date;

                        
                    }
                ],
                [ 
                    'label' => 'Gender',
                    'value'=> function($model){
                        // print_r($model->cards);
                        $card = Cards::findOne($model->cards[0]['id']);
                        $user = Users::findOne($card['_user_id']);
                        return isset($user['gender']) ? $user['gender'] : ''  ;
                        

                        
                    }
                ],
        
                [
                    'label' => 'First Name',               
                    'value' => function($model){
                        $card = Cards::findOne($model->cards[0]['id']);
                        $user = Users::findOne($card['_user_id']);

                        return isset($user['first_name']) ? $user['first_name'] : '' ;
                    }
                ],
                [
                    'label' => 'Last Name',                               
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        $user = Users::findOne($card['_user_id']);

                        return isset($user['last_name']) ? $user['last_name'] : '' ;
                    }
                ],
    
                [
                    'label' => 'Birth Day', 
                    'filterInputOptions' => [
                    'class'=>'form-control',
                    'id' => 'birth_bay'
                    ],                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        $user = Users::findOne($card['_user_id']);

                        return isset($user['birthday']) ? $user['birthday'] : '' ;
                    }
                ],
                [
                    'label' => 'Highest Education', 
                                                               
                    'value' => function($model){
                        $card = Cards::findOne($model->cards[0]['id']);
                        $user = Users::findOne($card['_user_id']);

                        return isset($user['highest_education']) ? $user['highest_education'] : '' ;
                    }
                ],
                [
                    'label' => 'Occupation',                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        $user = Users::findOne($card['_user_id']);

                        return isset($user['occupation']) ? $user['occupation'] : '' ;
                    }
                ],
    
                [
                    'label' => 'Mobile Phone',                                             
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        $user = Users::findOne($card['_user_id']);

                        return isset($user['mobile']) ? $user['mobile'] : '' ;
                    }
                ],
                [
                    'label' => 'Email',                                                          
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        $user = Users::findOne($card['_user_id']);

                        return isset($user['email']) ? $user['email'] : '' ;
                    }
                ],
                [
                    'label' => 'Business Name',
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);

                        return isset($card['officialBusiness']['business_name']) ? $card['officialBusiness']['business_name'] : '' ;
                    }
                ],
                [
                    'label' => 'Register ID',
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                         $business = Businesses::findOne($card['_business_id']);
                        return isset($business['reg_id']) ? $business['reg_id'] : '' ;
                    }
                ],
                [
                    'label' => 'No.',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        

                        return isset($card['officialBusiness']['no']) ? $card['officialBusiness']['no'] : '' ;
                    }
                ],
                [
                    'label' => 'Soi',
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        return isset($card['officialBusiness']['soi']) ? $card['officialBusiness']['soi'] : '' ;
                        

                        
                    }
                ],

                [
                'label' => 'Building',                                                              
                'value' => function($model){
                        $card = Cards::findOne($model->cards[0]['id']);
                                   
                        return isset($card['officialBusiness']['building']) ? $card['officialBusiness']['building'] : '';
                    }
                ],

                [
                    'label' => 'Floor',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);

                        return isset($card['officialBusiness']['floor']) ? $card['officialBusiness']['floor'] : '' ;
                        

                        
                    }
                ],
             
                [
                    'label' => 'Industrial Estate',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                         
                        return isset($card['officialBusiness']['industrial_estate']) ? $card['officialBusiness']['industrial_estate'] : '' ;
                        

                        
                    }
                ],
                [
                    'label' => 'Road',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialBusiness']['road']) ? $card['officialBusiness']['road'] : ''  ;
                         
                        
                    }
                ],
                [
                    'label' => 'Country',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialBusiness']['country']) ? $card['officialBusiness']['country'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Province',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);



                         if(isset($card['officialBusiness']['province'])){
                             $card_province = $card['officialBusiness']['province'];
                             $catalog = CATALOG::findOne(['CATALOG_ID' => $card_province])['NAME_EN'];
                             
                             return $catalog;
                         }else{
                             return '';
                         }

                        
                    }
                ],
                [
                    'label' => 'District',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                         

                        if(isset($card['officialBusiness']['district'])){
                            $district = $card['officialBusiness']['district'];
                            $catalog = CATALOG::findOne(['CATALOG_ID' => $district])['NAME_EN'];
                        
                            return $catalog;
                        }else{
                            return '';
                        }

                        

                        
                    }
                ],
                [
                    'label' => 'Postcode',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialBusiness']['postal_code']) ? $card['officialBusiness']['postal_code'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Level',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialContact']['level']) ? $card['officialContact']['level'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Position Title',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialContact']['positionTitle']) ? $card['officialContact']['positionTitle'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Department',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialContact']['department']) ? $card['officialContact']['department'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Office Phone',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialContact']['officePhone']) ? $card['officialContact']['officePhone'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Office Fax',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialContact']['officeFax']) ? $card['officialContact']['officeFax'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Office Email',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialContact']['officeEmail']) ? $card['officialContact']['officeEmail'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Office Mobile',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[0]['id']);
                        
                        return isset($card['officialContact']['officeMobile']) ? $card['officialContact']['officeMobile'] : '' ;

                        
                    }
                ],





                 [ 
                    'label' => 'Gender',
                    'value'=> function($model){
                        // print_r($model->cards);
                        $card = Cards::findOne($model->cards[1]['id']);
                        $user = Users::findOne($card['_user_id']);

                        return isset($user['gender']) ? $user['gender'] : ''  ;

                        
                    }
                ],
        
                [
                    'label' => 'First Name',              
                    'value' => function($model){
                        $card = Cards::findOne($model->cards[1]['id']);
                        $user = Users::findOne($card['_user_id']);
                        return isset($user['first_name']) ? $user['first_name'] : '' ;
                        
                    }
                ],
                [
                    'label' => 'Last Name',                               
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        $user = Users::findOne($card['_user_id']);
                        return isset($user['last_name']) ? $user['last_name'] : '' ;

                    }
                ],
    
                [
                    'label' => 'Birth Day',                                               
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        $user = Users::findOne($card['_user_id']);
                        return isset($user['birthday']) ? $user['birthday'] : '' ;
                        
                    }
                ],
                [
                    'label' => 'Highest Education',                                               
                    'value' => function($model){
                        $card = Cards::findOne($model->cards[1]['id']);
                        $user = Users::findOne($card['_user_id']);
                        return isset($user['highest_education']) ? $user['highest_education'] : '' ;
                        
                    }
                ],
                [
                    'label' => 'Occupation',                                             
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        $user = Users::findOne($card['_user_id']);
                        return isset($user['occupation']) ? $user['occupation'] : '' ;
                        
                    }
                ],
    
                [
                    'label' => 'Mobile Phone',                                               
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        $user = Users::findOne($card['_user_id']);
                        return isset($user['mobile']) ? $user['mobile'] : '' ;
                        
                    }
                ],
                [
                    'label' => 'Email',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        $user = Users::findOne($card['_user_id']);
                        return isset($user['email']) ? $user['email'] : '' ;

                    }
                ],
                [
                    'label' => 'Business Name',
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);

                        return $card['officialBusiness']['business_name'] ;
                    }
                ],
                [
                    'label' => 'Register ID',
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                         $business = Businesses::findOne($card['_business_id']);
                        return isset($business['reg_id']) ? $business['reg_id'] : '' ;
                    }
                ],
                [
                    'label' => 'No.',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialBusiness']['no']) ? $card['officialBusiness']['no'] : '' ;
                         
                    }
                ],
                [
                    'label' => 'Soi',
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        return isset($card['officialBusiness']['soi']) ? $card['officialBusiness']['soi'] : '' ;
                        

                        
                    }
                ],

                [
                'label' => 'Building',                                                              
                'value' => function($model){
                        $card = Cards::findOne($model->cards[1]['id']);
                                   
                        return isset($card['officialBusiness']['building']) ? $card['officialBusiness']['building'] : '';
                    }
                ],

                [
                    'label' => 'Floor',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);

                        return isset($card['officialBusiness']['floor']) ? $card['officialBusiness']['floor'] : '' ;
                        

                        
                    }
                ],
             
                [
                    'label' => 'Industrial Estate',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                         
                        return isset($card['officialBusiness']['industrial_estate']) ? $card['officialBusiness']['industrial_estate'] : '' ;
                        

                        
                    }
                ],
                [
                    'label' => 'Road',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialBusiness']['road']) ? $card['officialBusiness']['road'] : ''  ;
                         
                        
                    }
                ],
                [
                    'label' => 'Country',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialBusiness']['country']) ? $card['officialBusiness']['country'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Province',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);



                         if(isset($card['officialBusiness']['province'])){
                            $card_province = $card['officialBusiness']['province'];
                             
                             $catalog = CATALOG::findOne(['CATALOG_ID' => $card_province])['NAME_EN'];
                             
                             return $catalog;
                         }else{
                             return '';
                         }

                        
                    }
                ],
                [
                    'label' => 'District',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        

                        if(isset($card['officialBusiness']['district'])){
                            $district = $card['officialBusiness']['district'];
                            $catalog = CATALOG::findOne(['CATALOG_ID' => $district])['NAME_EN'];
                        
                            return $catalog;
                        }else{
                            return '';
                        }

                        

                        
                    }
                ],
                [
                    'label' => 'Postcode',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialBusiness']['postal_code']) ? $card['officialBusiness']['postal_code'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Level',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialContact']['level']) ? $card['officialContact']['level'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Position Title',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialContact']['positionTitle']) ? $card['officialContact']['positionTitle'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Department',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialContact']['department']) ? $card['officialContact']['department'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Office Phone',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialContact']['officePhone']) ? $card['officialContact']['officePhone'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Office Fax',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialContact']['officeFax']) ? $card['officialContact']['officeFax'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Office Email',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialContact']['officeEmail']) ? $card['officialContact']['officeEmail'] : '' ;

                        
                    }
                ],
                [
                    'label' => 'Office Mobile',                                                              
                    'value' => function($model){
                         $card = Cards::findOne($model->cards[1]['id']);
                        
                        return isset($card['officialContact']['officeMobile']) ? $card['officialContact']['officeMobile'] : '' ;

                        
                    }
                ],
  
        

            ['class' => 'kartik\grid\ActionColumn', 'urlCreator'=>function(){return '#';},
                'template'=>''],]
?>
<div class="claims-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div class="box box-danger">


<!--    
    <div class="box-header with-border">
    <p>
        <h3>LINK REQUESTER																									</h3>
    </p>
    </div>
    -->
    <?php 
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export All',
                'class' => 'btn btn-default'
            ]
        ])
    ?>


     <div class="box-body">
     
     <div class='table-responsive'>
        <?= GridView::widget([
        
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => $gridColumns,
        ]); ?>
     
     </div>
    </div>

    </div>
</div>

<script type="text/javascript">
    // alert($('#contents-slug').val());
    $('#id').daterangepicker({
                                locale: {
                                  format: 'DD/MM/YYYY'
                                },
                            });

 
</script>