<?php

use yii\helpers\Html;
use app\modules\users\models\Users;


/* @var $this yii\web\View */
/* @var $model app\modules\users\models\Users */

$this->title = 'Update Users: ' . $model->first_name.' '.$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->first_name.' '.$model->last_name, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="users-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'location_type' => $location_type,
        'longitude' => $longitude,
        'latitude' => $latitude,
        'location_card_id' => $location_card_id,
        'claims' => $claims,
        
    ]) ?>

</div>
