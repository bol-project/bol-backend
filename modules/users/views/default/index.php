<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\Cards;
use app\modules\users\helpers\Status;
use app\modules\users\models\Users;
use app\modules\claims\models\Claims;
use app\modules\claims\controllers\ClaimType;
use app\modules\businesses\models\businesses;
use app\models\MongoDate;
use yii\helpers\ArrayHelper;
use jino5577\daterangepicker\DateRangePicker;
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\users\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;

 $active= [
            ['value' => 0, 'label' => 'Active'],
            ['value' => 1, 'label' => 'In Active'],
        ];

?>



<div class="users-index">
    
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box box-danger">
   
    <div class="box-body">
     <div class='table-responsive'>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'tableOptions' => ['style' => 'font-size: 13px; width: 100%; ', 'class' => 'table-striped'],
            'pager' => [
                        'firstPageLabel'=>'<<',   
                        'prevPageLabel' =>'<', 
                        'nextPageLabel' =>'>',   
                        'lastPageLabel' =>'>>'
            ],
            'columns' => [
                [
                    'class' => 'yii\grid\SerialColumn',
                    'headerOptions' => ['style' => 'width: 3%; text-align: left; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-left' ],
                    
               
                ],

                [ 
                    'label' => 'Email',
                    'headerOptions' => ['style' => 'width: 15%; text-align: left; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-left' ],
                    'attribute' => 'email',
                    'format'=>'html',
                    'value'=> function($model){
                        return $model->email;
                    }
                ],
                [ 
                    'label' => 'Register Date',
                    'headerOptions' => ['style' => 'width: 7%; text-align: left; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-left' ],
                    'attribute' => 'registerDate',
                    'filter'=>DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'registerDate',
                            'pluginOptions' => [
                                'autoUpdateInput' => false,
                                'locale' => [
                                    'format' => 'DD/MM/YYYY'
                                ]
                            ]
                    ]),
                    'format'=>'html',
                    'value'=> function($model){
                        
                        return Mongodate::mongoDateToString($model->registerDate, true);
                    }
                ],
                [ 
                    'label' => 'Account Status',
                    'headerOptions' => ['style' => 'width: 7%; text-align: left; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-left' ],
                    'attribute' => 'status',
                    'filter' => Html::activeDropDownList($searchModel, 'status', ArrayHelper::map(Status::getActivateStatus(), 'value','label'),
                                                                        ['class'=>'form-control','prompt' => 'Select Status']
                                                ),
                    'format'=>'html',
                    'value'=> function($model){
                        $activate_status = Status::checkActivateStatus($model->status);
                        if ($activate_status == 'Deactivate') return '<span style="color: red;">'.$activate_status.'</span>';
                        else if ($activate_status == 'Activated') return '<span style="color: green;">'.$activate_status.'</span>';
                        else if ($activate_status == 'Pending') return '<span style="color: orange;">'.$activate_status.'</span>';
                    }
                ],
                [ 
                    'label' => 'Claim Type',
                    'format'=>'html',
                    'headerOptions' => ['style' => 'width: 6%; text-align: left; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-left' ],
                    'value' => function($users){
                        $default_card = Cards::findOne($users->defaultCardId);                                                                        
                        $business_id = $default_card['_business_id'];
                        $claims = Claims::findOne(['user_id' => (string)$users->_id, 'business_id' => (string)$business_id]);
                     
                        if(isset($claims['claim_type']))
                            return ClaimType::toString($claims['claim_type']);
                        else return null;

                    
                    }
                    
                
                ],
                [ 
                    'label' => 'Position Title',
                    'format'=>'html',
                    'attribute' => 'PositionTitle',
                    'headerOptions' => ['style' => 'width: 8%; text-align: left; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-left' ],
                    'value' => function($users){
                        $default_card = Cards::findOne($users->defaultCardId);                                                
                        

                        return $default_card['officialContact']['positionTitle'];
                    
                    }
                
                ],
                [ 
                    'label' => 'Login Info',
                    'format'=>'html',
                    //'attribute' => 'login_info',
                    // 'filter' => Html::activeDropDownList($searchModel, 'login_info', ArrayHelper::map($active, 'value','label'),
                    //                                                     ['class'=>'form-control','prompt' => 'Select Login Info']
                    //                             ),
                    'headerOptions' => ['style' => 'width: 6%; text-align: center; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-center' ],
                    'value' => function($users){
                        if(!!$users->email && !!$users->password){
                            return '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="color: green;"></span>'; 
                        } else {
                            return '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true" style="color: red;"></span>'; 
                            
                        }
        
                    }
                    
                
                ],
                [ 
                    'label' => 'User Info',
                    'format'=>'html',
                    'headerOptions' => ['style' => 'width: 6%; text-align: center; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-center'],
                    'value' => function($users){
                    if(!!$users->gender && !!$users->first_name && !!$users->last_name && !!$users->birthday && !!$users->highest_education && !!$users->occupation){
                        return '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="color: green;"></span>'; 
                    } else {
                        return '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true" style="color: red;"></span>'; 
                        
                    }
        
                }
                
                ],
                [ 
                    'label' => 'Business Profile',
                    'format'=>'html',
                    'headerOptions' => ['style' => 'width: 6%; text-align: center; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-center'],
                    'attribute' => 'business_profile',
                    'filter' => Html::activeDropDownList($searchModel,'business_profile', ArrayHelper::map($active, 'value','label'),
                                                                        ['class'=>'form-control','prompt' => 'Select Status']
                                                        ),
                    'value' => function($users){
                        
                        $default_card = Cards::findOne($users->defaultCardId);                                                
                        $check_user_business = isset($default_card['officialBusiness']['business_id']);
                     
                        if($check_user_business){
                            return '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="color: green;"></span>'; 
                            
                        }else {
                            return '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true" style="color: red;"></span>'; 
                            
                        }

                            
                    }
                
                ],
                [ 
                    'label' => 'Role & Contact',
                    'format'=>'html',
                    'headerOptions' => ['style' => 'width: 6%; text-align: center; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-center'],
                    'attribute' => 'role_contact',
                    'filter' => Html::activeDropDownList($searchModel,'role_contact', ArrayHelper::map($active, 'value','label'),
                                                                        ['class'=>'form-control','prompt' => 'Select Status']
                                                        ),
                    'value' => function($users){
                    
                        $default_card = Cards::findOne($users->defaultCardId);                        
                        $position_title = isset($default_card['officialContact']['positionTitle']);
                        $office_email = isset($default_card['officialContact']['officeEmail']);

                 
                    
                        if($position_title && $office_email){
                            return '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="color: green;"></span>'; 
                            
                        }else {
                            return '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true" style="color: red;"></span>'; 
                            
                        }     
                }
                    
                
                ],
                [ 
                    'label' => 'Business Card',
                    'format'=>'html',
                    'label' => 'Business_card',
                    'headerOptions' => ['style' => 'width: 6%; text-align: center; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-center'],
                    // 'attribute' => 'business_card',
                    // 'filter' => Html::activeDropDownList($searchModel,'business_card', ArrayHelper::map($active, 'value','label'),
                    //                                                     ['class'=>'form-control','prompt' => 'Select Status']
                    //                                     ),
                    'value' => function($users){
                        $default_card = Cards::findOne($users->defaultCardId);
         
                        if(isset($default_card)){
                            return '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="color: green;"></span>'; 
                            
                        }else {
                            return '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true" style="color: red;"></span>'; 
                            
                        }
                    }
                
                ],
                [ 
                    'label' => 'OTP',
                    'format'=>'html',
                    'headerOptions' => ['style' => 'width: 3%; text-align: center; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-center'],
                    // 'attribute' => 'otp',
                    // 'filter' => Html::activeDropDownList($searchModel,'otp', ArrayHelper::map($active, 'value','label'),
                    //                                                     ['class'=>'form-control','prompt' => 'Select Status']
                    //                                     ),
                    'value' => function($users){
                    
                         if(isset($users->mobile) && isset($users->country_code)){
                           return '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="color: green;"></span>'; 
                            
                        }else {
                         return '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true" style="color: red;"></span>'; 
                            
                         }
                    }
                
                ],
                [ 
                    'label' => 'Activate Email',
                    'format'=>'raw',
                    'headerOptions' => ['style' => 'width: 6%; text-align: center; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-center'],
                    'value' => function($model){

                        return '<a  class="resend" data-value='.$model->_id.' style="cursor: pointer; " data-toggle="modal" data-target="#myModal">resend</a>';

                    }

                ],
                [ 
                    'label' => 'Activate User',
                    'format'=>'html',
                    'headerOptions' => ['style' => 'width: 5%; text-align: center; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-center'],
                    //'attribute' => 'status',
                    // 'filter' => Html::activeDropDownList($searchModel, 'status', ArrayHelper::map(Status::getActivateStatus(), 'value','label'),
                    //                                                     ['class'=>'form-control','prompt' => 'Select Status']
                    //                             ),
                     'value' => function($users){
                        if(!!$users->status){
                            return '<span class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="color: green;"></span>'; 
                            
                        }else {
                            return '<span class="glyphicon glyphicon-remove-circle" aria-hidden="true" style="color: red;"></span>'; 
                            
                        }

                    }
                
                ],
                [ 
                    'label' => 'Last Login',
                    'format'=>'html',
                    'attribute'=>'lastLogedin',
                    'filter'=>DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'lastLogedin',
                            'pluginOptions' => [
                                'autoUpdateInput' => false,
                                'locale' => [
                                    'format' => 'DD/MM/YYYY'
                                ]
                            ]
                    ]),
                    'headerOptions' => ['style' => 'width: 6%; text-align: center; white-space: normal;'],
                    'contentOptions' => ['class' => 'text-center'],
                    'value' => function($model){
                        return Mongodate::mongoDateToString($model->lastLogedin,true);
                    }
                ],
        
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template'=>'{update}',
                    'headerOptions' => ['style' => 'width: 10%; text-align: center;'],
                    'contentOptions' => ['class' => 'text-center']
                ],
            ],
        ]); ?>
    </div>
    </div>
    </div>
        <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            <p></p>
            <div class='text-right'>
                <button type="button" id='submit-resend' class="btn btn-info " data-dismiss="modal" style='background-color: #3c8dbc;'>Resend</button>
                <button type="button" id='close' class="btn btn-default " data-dismiss="modal">Close</button>
            </div>
            </div>
            
        </div>
        
        </div>
    </div>
</div>

<?php $this->registerJsFile('@web/js/config.js'); ?>
<?php $this->registerJsFile('@web/js/users/users.js'); ?>

<script>
     $('.form-control').css({'width': '90%'});
    $('table tbody tr').css({'height': '60px'});
</script>

