<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\users\models\Users;
use yii\helpers\ArrayHelper;
use app\models\Cards;
use app\models\MongoDate;
use app\modules\businesses\models\Businesses;
use app\modules\claims\controllers\RequestStatus;
use app\modules\claims\controllers\ClaimType;
use app\modules\users\helpers\Status;





/* @var $this yii\web\View */
/* @var $model app\modules\users\models\Users */
/* @var $form yii\widgets\ActiveForm */

?>


<div class="users-form">
<?php if(Yii::$app->session->hasFlash('alert')):?>
    <?= \yii\bootstrap\Alert::widget([
    'body'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'body'),
    'options'=>ArrayHelper::getValue(Yii::$app->session->getFlash('alert'), 'options'),
    ])?>
<?php endif; ?>

<?php if(!$model->isNewRecord) { ?>
    <div class='box box-danger'>
    
        <div class='box-body'>   
            <div class='row'>
                <div class='col-md-4'>
                        <div class='form-group'>
                            <strong>Register Info</strong>
                        </div>
                        <div style='padding-left: 4%;'>
                            <div class='form-group' style='display: flex;'>   
                                <label>Register Date : </label>
                                <?php 
                                if($model->registerDate){
                                    echo Html::tag('div', Mongodate::mongoDateToString($model->registerDate), ['style' => 'padding-left: 2%; width: 40%;']);
                                }
                                else{
                                    echo '<div style="padding-left: 2%;">No Date</div>';
                                }
                                ?>
                            </div>

                            <div class='form-group' style='display: flex;'>   
                                <label>Register Company : </label>
                                <?php 
                                $Cards = Cards::find()
                                        ->where(
                                                ['_user_id' =>$model->_id]
                                                )
                                        ->all();
                             
                                ?>
                                <ul class='list-unstyled'>
                            
                                <?php
                                    foreach ($Cards as $key => $value) {
                                   $businesses=Businesses::find()
                                    ->where(['_id'=>$value['_business_id']])
                                    ->one();

                                    if(isset($businesses['name']))
                                    {
                                        if(isset($businesses['name']['en']))
                                        {
                                            echo '<li>'.$businesses['name']['en'].'</li>';
                                        }
                                        else
                                        {
                                            echo '<li>'.$businesses['name']['th'].'</li>';
                                        }
                                    }
                                    else
                                    {
                                            echo '<li>Business name error!!.<li>';    
                                    }
                                }
                            
                                ?>
                                </ul>

                            </div>

                            <div class='form-group' style='display: flex;'>   
                                <label>Send Activate Status : </label>
                                <div style='padding-left: 2%; width: 20%;'>
                                    <span class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="color: green;"></span>
                                     <a  class="resend" data-value=<?= $model->_id; ?> style="cursor: pointer; " data-toggle="modal" data-target="#myModal">resend</a>
                                    
                                </div>
                            </div>

                            <div class='form-group' style='display: flex;'>
                                <label>Acount Status : </label>
                                <?php 
                                    $activate_status = Status::checkActivateStatus($model->status);
                                    if ($activate_status == 'Deactivate') echo '<span style="color: red; padding-left: 2%;">'.$activate_status.'</span>';
                                    else if ($activate_status == 'Activated') echo '<span style="color: green; padding-left: 2%;">'.$activate_status.'</span>';
                                    else if ($activate_status == 'Pending') echo '<span style="color: orange; padding-left: 2%;">'.$activate_status.'</span>';
                                    else echo ' <div style="padding-left: 2%;">No Date</div>';  
                                ?>
                            </div>
                            <div class='form-group' style='display: flex;'>
                                <label>Last Login : </label>
                                <?php
                                    if($model->lastLogedin){
                                        echo  Html::tag('div', Mongodate::mongoDateToString($model->lastLogedin), ['style' => 'padding-left: 2%; width: 40%;']);               
                                    }else{
                                        echo '<div style="padding-left: 2%; width: 40%;">No Date</div>';
                                    }
                                ?>
                                
                            </div>
                        
                        </div>
                    </div>
                    <div class='col-md-8'>
                        <div class='form-group'>
                            <strong>Claim Info</strong>
                        </div>
                        <div style='padding-left: 1%'>
                            <div class='form-group'>
                                <table class='table' style='font-size: 13px;' >
                                    <thead>
                                        <tr>
                                            <th>Claim Type</th>
                                            <th>Business Name</th>
                                            <th>Claim Status</th>
                                            <th>CS Response</th>
                                            <th>Approve Date</th>
                                        </tr>

                                    </thead>
                                    <tbody>
                                        
                                        <?php 
                                            foreach ($claims as $claim) {
                                                
                                                    $businesses = Businesses::find()->where(['_id' => $claim->business_id])->one();
                                                    echo '<tr>';
                                                    if(isset($claim->claim_type)) 
                                                        echo '<td>'.ClaimType::toString($claim->claim_type).'</td>' ;
                                                    else
                                                        echo '<td>No Claim type</td>';
                                                    if(isset($businesses['name'])){
                                                        if(isset($businesses['name']['en'])){
                                                            echo '<td>'.$businesses['name']['en'].'</td>';
                                                        }else{
                                                            echo '<td>'.$businesses['name']['th'].'</td>';
                                                        }
                                                    }else{
                                                        echo 'Business name error!!.';    
                                                    }

                                                    if(isset($claim->request_status)){
                                                        echo '<td>'.RequestStatus::toString($claim->request_status).'</td>';
                                                    }else{
                                                        echo '<td>No Status</td>';
                                                    }

                                                    if(isset($claim->remark->agent_id)){
                                                        echo '<td>'.$claim->remark->agent_id.'</td>' ;
                                                    }
                                                    else {
                                                        echo '<td>No Agent ID</td>' ;
                                                    }
                                                    if(isset($claim->approve_date)){
                                                       echo '<td>'.Mongodate::mongoDateToString($claim->approve_date).'</td>';
                                                    }else{
                                                         echo '<td>No Date</td>' ;
                                                        
                                                    }

                                                    echo '</tr>';
                                             
                                            }
                                        ?>                                

                                    </tbody>
                                </table>
                            </div>
                      
                        </div>
            
                    </div>
             

                </div>
            
            </div>

            </div>
        </div>
    </div>
<?php } ?>


<div class="box box-danger">

    <div class='box-header'>
        <strong>User Profile</strong>
        
    </div>
    <div class="box-body">
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <div class='row'>
                <div class='col-md-4'>
                
                    <?= $form->field($model, 'email')->textInput(['placeholder' => 'Enter Your First Name', 'disabled' => '']) ?>
                    
                
                     <div class='form-group'>
                        <?php 
                        if(!$model->isNewRecord) { 
                            echo '<label>Register Channel : </label>';
                            if($model->facebookId != ''){
                                echo ' Facebook';
                            }else {
                                echo ' Email';
                            }
                        }
                        ?>
                    </div>
                     <div class="form-group field-users-avatar" >
                        <?php 
                            if(!!$model->avatar) 
                                echo Html::img($model->avatar, ['style' => 'width: 50%;', 'class' => 'avatar-upload']) ;
                            else 
                                echo Html::img('@web/images/userdefault.png', ['style' => 'width: 50%;', 'class' => 'avatar-upload']); ?>
                        <div class="help-block"></div>
                        <?= $form->field($model, 'avatar')->fileInput()->label(false) ?>  
                    </div>
                </div>


                <div class='col-md-4'>
                    <?= $form->field($model, 'first_name')->textInput(['placeholder' => 'Enter Your First Name']) ?>
                    <?= $form->field($model, 'last_name')->textInput(['placeholder' => 'Enter Your Last name']) ?>
                    <?= $form->field($model, 'mobile')->textInput(['placeholder' => 'Enter Your Mobile']) ?>

                    <div class='form-group'>   
                        <label>Highest Education</label>
                        <div class="input-group" style="width: 100%;">
                            <?= Html::activeDropDownList(
                                $model, 
                                'highest_education', 
                                [ 
                                'Lower than Bachelor Degree' => 'Lower than Bachelor Degree', 
                                'Bachelor Degree' => 'Bachelor Degree', 
                                'Master Degree' => 'Master Degree', 
                                'Doctorate' => 'Doctorate'
                                ], ['class' => 'form-control']) ?>
                            
                        </div>
                    </div>
                 
                </div>

                <div class='col-md-4'>
                       <div class='form-group'>   
                        <label>Gender</label>
                        <div class="input-group" style="width: 100%;">
                            <?= Html::activeDropDownList(
                                $model, 
                                'gender', 
                                [ 
                                'Male' => "Male",
                                'Female' => "Female"
                                ], ['class' => 'form-control']) ?>
                        </div>
                    </div>
                    <div class='form-group field-users-birthday required'>   
                        <label class="control-label" for="users-birthday">Birthday</label>
                        <div class="input-group">
                            <?= Html::activeInput('text', $model, 'birthday', ['class' => 'form-control datepicker', 'id' => 'users-birthday', 'name'=>'Users[birthday]','placeholder' => "Enter Your Birthday"]) ?>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar "></i></span>
                        </div>
                        <div class="help-block"></div>    
                    </div>
                    <div class='form-group'>   
                        <label>Activate Status</label>
                        <div class="input-group" style="width: 100%;">
                            <?= Html::activeDropDownList($model, 'status', ArrayHelper::map(Status::getActivateStatus(), 'value', 'label'),['class' => 'form-control']) ?>
                        
                        </div>
                    </div>
                </div>
         </div>
    </div>

 
</div><!-- box-info -->
 <div class="row">
    <div class='col-md-12'>
        <div class="form-group text-right">
            <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            <?= Html::a('Cancel', ['/users'], ['class'=>'btn btn-danger']) ?>
        </div>
    </div>
    </div>
</div>

    <!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <p>Do you want to resend activate email to user.</p>
        <div class='text-right'>
            <button type="button" id='submit-resend' class="btn btn-info " data-dismiss="modal" style='background-color: #3c8dbc;'>Resend</button>
            <button type="button" id='close' class="btn btn-default " data-dismiss="modal">Close</button>
        </div>
        </div>
        
    </div>
    
    </div>
</div>


<?php ActiveForm::end(); ?>
<?php $this->registerJsFile('@web/js/config.js'); ?>
<?php $this->registerJsFile('@web/js/users/users.js'); ?>

<script type="text/javascript">
    $(function () {
        $('.datepicker').datepicker({dateFormat: "d/m/yy"});
    });

    $('#users-avatar').css({'width': '100%'});

    $('#users-avatar').change(function(click){
        
       readURL(this);
           
    });

    
    

    function readURL(input) {

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.avatar-upload').attr('src', e.target.result);
                // console.log(e.target.result);
    
            }

            reader.readAsDataURL(input.files[0]);
            console.log(input.files[0]);
            
        }
    }

    function checkEmtyInputAvatar(){
        if(!$('.avatar-upload').attr('src') == '/web/images/userdefault.png'){

        }
    }

    
        
   
</script>
