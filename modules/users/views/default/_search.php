<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\users\models\UsersSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="users-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, '_id') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'password') ?>

    <?= $form->field($model, 'gender') ?>

    <?= $form->field($model, 'first_name') ?>

    <?php // echo $form->field($model, 'last_name') ?>

    <?php // echo $form->field($model, 'mobile') ?>

    <?php // echo $form->field($model, 'country_code') ?>

    <?php // echo $form->field($model, 'avatar') ?>

    <?php // echo $form->field($model, 'birthday') ?>

    <?php // echo $form->field($model, 'highest_education') ?>

    <?php // echo $form->field($model, 'institute') ?>

    <?php // echo $form->field($model, 'major') ?>

    <?php // echo $form->field($model, 'occupation') ?>

    <?php // echo $form->field($model, 'facebookId') ?>

    <?php // echo $form->field($model, 'deviceTokens') ?>

    <?php // echo $form->field($model, 'registerDate') ?>

    <?php // echo $form->field($model, 'follower') ?>

    <?php // echo $form->field($model, 'sharedBy') ?>

    <?php // echo $form->field($model, 'defaultCardId') ?>

    <?php // echo $form->field($model, 'linkeds') ?>

    <?php // echo $form->field($model, 'link_requests') ?>

    <?php // echo $form->field($model, 'trash_link_requests') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'online_status') ?>

    <?php // echo $form->field($model, 'lastModified') ?>

    <?php // echo $form->field($model, 'lastLogedin') ?>

    <?php // echo $form->field($model, 'location') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
