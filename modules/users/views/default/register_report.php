<?php

use app\models\MongoDate;
use yii\helpers\Html;
//use yii\grid\GridView;

use kartik\export\ExportMenu;
use kartik\grid\GridView;
use jino5577\daterangepicker\DateRangePicker;
use app\modules\users\helpers\Choice;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\claims\models\ClaimsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Register Report';
$this->params['breadcrumbs'][] = $this->title;
$gridColumns = [
        ['class' => 'kartik\grid\SerialColumn'],
            [ 
                'label' => 'Date',
                'attribute' => 'registerDate',
                'format'=>'html',
                'filter'=>DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'registerDate',
                            'pluginOptions' => [
                                'autoUpdateInput' => false,
                                'locale' => [
                                    'format' => 'DD/MM/YYYY'
                                ]
                            ]
                ]),
                'value'=> function($model){
                    
                    return Mongodate::mongoDateToString($model->registerDate, true);
                }
            ],
            [ 
                'label' => 'Time',
                'format'=>'html',
                'value'=> function($model){
                    
                    return Mongodate::mongoDateToString($model->registerDate, false, 'H:i:s');
                }
            ],
            
      
            [
                'label' => 'First Name',
                'attribute' => 'first_name',                
                'value' => function($model){
                    return $model->first_name;
                }
            ],
            [
                'label' => 'Last Name',
                'attribute' => 'last_name',                                
                'value' => function($model){
                    return $model->last_name;
                }
            ],
  
            [
                'label' => 'Birth Day',
                'attribute' => 'birthday',
                // 'filterInputOptions' => [
                //     'class'=>'form-control',
                //     'id' => 'birthbay'
                //     ],                                                
                'value' => function($model){
                    return $model->birthday;
                }
            ],
            [
                'label' => 'Highest Education',
                'attribute' => 'highest_education',   
                'attribute'=>'highest_education',
                'filter' => Html::activeDropDownList($searchModel, 'highest_education', ArrayHelper::map(Choice::getActivateEducationChoice(), 'value','label'),
                                                                        ['class'=>'form-control','prompt' => 'Select Education']
                                                ),                                             
                'value' => function($model){
                    return $model->highest_education;
                }
            ],
            [
                'label' => 'Occupation',
                'attribute' => 'occupation',
                'filter' => Html::activeDropDownList($searchModel, 'occupation', ArrayHelper::map(Choice::getActivateOccupation(), 'value','label'),
                                                                        ['class'=>'form-control','prompt' => 'Select occupation']
                                                ),                                                
                'value' => function($model){
                    return $model->occupation;
                }
            ],
  
            [
                'label' => 'Mobile Phone',
                'attribute' => 'mobile',                                                
                'value' => function($model){
                    return $model->mobile;
                }
            ],
            [
                'label' => 'Email',
                'attribute' => 'email',                                                                
                'value' => function($model){
                    return $model->email;
                }
            ],
  
        

            ['class' => 'kartik\grid\ActionColumn', 'urlCreator'=>function(){return '#';},
                'template'=>''],]
?>
<div class="claims-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div class="box box-danger">

    <?php 
    /*
    <div class="box-header with-border">
    <p>
        <?= Html::a('Create Claims', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
    */ 
    ?>

    <?php 
        echo ExportMenu::widget([
            'dataProvider' => $dataProvider,
            'columns' => $gridColumns,
            'fontAwesome' => true,
            'dropdownOptions' => [
                'label' => 'Export All',
                'class' => 'btn btn-default'
            ]
        ])
    ?>

    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => $gridColumns
    ]); ?>
    </div>

    </div>
</div>

<script type="text/javascript">
    $(function () {
        $("#birthbay").datepicker({format: "dd/mm/yyyy"}).datepicker();
    });
</script>
