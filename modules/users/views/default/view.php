<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\MongoDate;
use app\modules\users\helpers\Status;
use yii\helpers\ArrayHelper;




/* @var $this yii\web\View */
/* @var $model app\modules\users\models\Users */

$this->title = $model->first_name.' '.$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-view">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="box box-info">

    <div class="box-header with-border">
    <p>
        <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
         <?= Html::a('Index', ['/users'], ['class'=>'btn btn-success']) ?>
    </p>
    </div>
    <div class='box-body'>   
        <div class='row'>
            <div class='col-md-4'>
                    <div class='form-group'>
                        <strong>Register Info</strong>
                    </div>
                    <div style='padding-left: 4%;'>
                        <div class='form-group' style='display: flex;'>   
                            <label>Register Date : </label>
                            <?php 
                            if($model->registerDate){
                                echo Html::tag('div', Mongodate::mongoDateToString($model->registerDate), ['style' => 'padding-left: 2%; width: 40%;']);
                            }
                            else{
                                echo '<div style="padding-left: 2%;">No Date</div>';
                            }
                            ?>
                        </div>

                        <div class='form-group' style='display: flex;'>   
                            <label>Send Activate Status : </label>
                            <div style='padding-left: 2%; width: 40%;'>
                                <span class="glyphicon glyphicon-ok-circle" aria-hidden="true" style="color: green;"></span>
                                <a  class="resend" data-value=<?= $model->_id; ?> style="cursor: pointer; " data-toggle="modal" data-target="#myModal">resend</a>
                            </div>
                        </div>

                        <div class='form-group' style='display: flex;'>
                            <label>Acount Status : </label>
                            <?php 
                                $activate_status = Status::checkActivateStatus($model->status);
                                if ($activate_status == 'Deactivate') echo '<span style="color: red; padding-left: 2%;">'.$activate_status.'</span>';
                                else if ($activate_status == 'Activated') echo '<span style="color: green; padding-left: 2%;">'.$activate_status.'</span>';
                                else if ($activate_status == 'Pending') echo '<span style="color: yellow; padding-left: 2%;">'.$activate_status.'</span>';
                                else echo ' <div style="padding-left: 2%;">No Date</div>';  
                            ?>
                        </div>
                        <div class='form-group' style='display: flex;'>
                            <label>Last Login : </label>
                            <?php
                                if($model->lastLogedin){
                                     echo  Html::tag('div', Mongodate::mongoDateToString($model->lastLogedin), ['style' => 'padding-left: 2%; width: 40%;']);               
                                }else{
                                    echo '<div style="padding-left: 2%; width: 40%;">No Date</div>';
                                }
                            ?>
                            
                        </div>
                    
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class='form-group'>
                        <strong>Claim Info</strong>
                    </div>
                    <div style='padding-left: 4%'>
                        <div class='form-group'>
                            <label>Claimer : </label>
                        </div>
                        <div class='form-group'>
                            <label>Claim Status : </label>
                        </div>
                        <div class='form-group'>
                            <label>CS Response : </label>
                        </div>
                    </div>
                    <div class='form-group'>
                        <strong>Agreement : </strong>
                    </div>
                    <div style='padding-left: 4%'>
                        <div class='form-group'>
                            <label>No. of Agreement : </label>
                        </div>
                       
                    </div>
                </div>

                <div class='col-md-4'>
                    <div class='form-group'>
                        <strong>Activity : </strong>
                    </div>
                    <div style='padding-left: 4%'>
                        <div class='form-group'>
                            <label>Abuse Message : </label>
                        </div>
                        <div class='form-group'>
                            <label>Business Loan : </label>
                        </div>
                        <div class='form-group'>
                            <table class='table table-bordered'>
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Request Date</th>
                                        <th>Amount</th>
                                        <th>Loan Type</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </tbody>
                                <tr></tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
         
        </div>

        </div>
    </div>
    <div class="box box-info">

        <div class='box-header'>
            <strong>User Profile</strong>
            
        </div>
        <div class="box-body">
                <div class='row'>
                    <div class='col-md-4'>
                        <div class='form-group' style='display: flex;'>
                            <label>email : </label>   
                            <?= Html::tag('div', $model->email, ['style' => 'padding-left: 2%;']); ?>
                    
                        </div>
                            <div class="form-group field-users-avatar">
                            <?php 
                                if(!!$model->avatar) 
                                    echo Html::img($model->avatar, ['width' => '200']) ;
                                else 
                                    echo Html::img('@web/images/userdefault.png', ['width' => '200px']); ?>
                        </div>
                    </div>

                    <div class='col-md-4'>
                        <div class='form-group' style='display: flex;'>
                            <label>First Name : </label>   
                            <?= Html::tag('div', $model->first_name, ['style' => 'padding-left: 2%;']); ?>
                        </div>
                        <div class='form-group' style='display: flex;'>
                            <label>Last Name : </label>   
                            <?= Html::tag('div', $model->last_name, ['style' => 'padding-left: 2%;']); ?>
                        </div>
                        <div class='form-group' style='display: flex;'>
                            <label>Mobile : </label>   
                            <?= Html::tag('div', $model->mobile, ['style' => 'padding-left: 2%;']); ?>
                        </div>
        
                        <div class='form-group' style='display: flex;'>   
                            <label>Highest Education : </label>
                            <?= Html::tag('div', $model->highest_education, ['style' => 'padding-left: 2%;']); ?>
                            
                        </div>
                        <div class='form-group' style='display: flex;'>   
                            <label>Gender : </label>
                            <?= Html::tag('div', $model->gender, ['style' => 'padding-left: 2%;']); ?>
                        </div>
                        <div class='form-group' style='display: flex;'>   
                            <label>Birthday : </label>
                            <?= Html::tag('div', $model->birthday, ['style' => 'padding-left: 2%;']); ?>
                        </div>
                    
                    </div>

                    <div class='col-md-4'>
                        <div class='form-group' style='display: flex;'>   
                            <label>Register Date : </label>
                            <?php 
                            if($model->registerDate){
                                echo Html::tag('div', Mongodate::mongoDateToString($model->registerDate), ['style' => 'padding-left: 2%;']);
                            }
                            else{
                                echo '<div style="padding-left: 2%;">No Date</div>';

                            }
                            ?>
                        </div>

                        <div class='form-group' style='display: flex;'>   
                            <label>Activate Status : </label>
                             <?php 
                                $activate_status = Status::checkActivateStatus($model->status);
                                if ($activate_status == 'Deactivate') echo '<span style="color: red; padding-left: 2%;">'.$activate_status.'</span>';
                                else if ($activate_status == 'Activated') echo '<span style="color: green; padding-left: 2%;">'.$activate_status.'</span>';
                                else if ($activate_status == 'Pending') echo '<span style="color: orange; padding-left: 2%;">'.$activate_status.'</span>';
                            ?>
                        </div>
                    </div>
            </div>
        </div>

    </div>

           <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
        
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
            <p></p>
            <div class='text-right'>
                <button type="button" id='submit-resend' class="btn btn-info " data-dismiss="modal" style='background-color: #3c8dbc;'>Resend</button>
                <button type="button" id='close' class="btn btn-default " data-dismiss="modal">Close</button>
            </div>
            </div>
            
        </div>
        
        </div>
    </div>

</div><!-- box-info -->

<?php $this->registerJsFile('@web/js/config.js'); ?>
<?php $this->registerJsFile('@web/js/users/users.js'); ?>
