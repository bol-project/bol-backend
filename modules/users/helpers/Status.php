<?php

namespace app\modules\users\helpers;


class Status 
{
    public static function getActivateStatus() {
         $activate_status = [
            ['value' => '0', 'label' => 'Deactivate'],
            ['value' => '1', 'label' => 'Activate'],
            ['value' => '2', 'label' => 'Pending'],
        ];

        return $activate_status;
    }

    public static function checkActivateStatus($activate_status) {
        if($activate_status == '0')
            return 'Deactivate';
        else if($activate_status == '1')
            return 'Activated';
        else if($activate_status == '2')
            return 'Pending';
    }

    public static function getOnlineStatus()
    {
          $online_status = [
            ['value' => 'false', 'label' => 'Offline'],
            ['value' => 'true', 'label' => 'Online'],
        ];

        return $online_status;
    }

    public static function checkOnlineStatus($online_status) {
        if($online_status == 'false')
            return 'Offline';
        else if($online_status == 'true')
            return 'Online';

    }
}
