<?php

namespace app\modules\users\helpers;


class Choice 
{
	 public static function getActivateEducationChoice() 
	 {
         $EducationChoices = [
            ['value' => 'Lower than Bachelor Degree', 'label' => 'Lower than Bachelor Degree'],
            ['value' => 'Bachelor Degree', 'label' => 'Bachelor Degree'],
            ['value' => 'Master Degree', 'label' => 'Master Degree'],
            ['value' => 'Doctorate', 'label' => 'Doctorate'],
        ];

        return $EducationChoices;
    }

    public static function getActivateOccupation()
    {
    	$Occupation = [
    		['value'=>'Owner','label'=>'Owner'],
    		['value'=>'Gov. Officer','label'=>'Gov. Officer'],
    		['value'=>'Employee','label'=>'Employee'],
    		['value'=>'Self-Employed/Freelance','label'=>'Self-Employed/Freelance'],
    		['value'=>'Students','label'=>'Students'],
    		['value'=>'Unemployed/Retired','label'=>'Unemployed/Retired'],
    	];
    	return $Occupation;
    }
}
?>