<?php

namespace app\modules\claims\controllers;

class ClaimType {
    private static $enum = array(0 => "Claimer", 1 => "Sub Claimer");

    public static $CLAIMER = 0;
    public static $SUB_CLAIMER = 1;

    public function getArrayData(){
        return self::$enum;
    }

    public static function toString($ordinal) {
        return self::$enum[$ordinal];
    }
}
