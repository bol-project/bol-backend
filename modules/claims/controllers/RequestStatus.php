<?php

namespace app\modules\claims\controllers;

class RequestStatus {
    private static $enum = array(0 => "Reject", 1 => "Verified", 2 => "Pending", 3 => "Expire");
    
    public static $REJECT = 0;
    public static $VERIFIED = 1;
    public static $PENDING = 2;
    public static $EXPIRE = 3;
    public static $status =[
                            ['value'=>0,'label'=>'REJECT'],
                            ['value'=>1,'label'=>'VERIFIED'],
                            ['value'=>2,'label'=>'PENDING'],
                            ['value'=>3,'label'=>'EXPIRE'],
                           ];
    public function getArrayData(){
        return self::$enum;
    }

    public function getStatus(){
        return self::$status;
    }
    public static function toString($ordinal) {
        return self::$enum[$ordinal];
    }
}
