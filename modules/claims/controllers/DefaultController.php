<?php

namespace app\modules\claims\controllers;

use Yii;
use app\models\MongoDate;
use app\modules\claims\models\Claims;
use app\modules\claims\models\ClaimsSearch;
use app\modules\users\models\Users;
use app\modules\businesses\models\Businesses;
use yii\mongodb\Query;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use GuzzleHttp\Promise\Promise;
use app\modules\claims\controllers\RequestStatus;
use app\modules\claims\controllers\ClaimType;
use yii\web\UploadedFile;
use app\helpers\API;
use app\controllers\AdminController;

/**
 * DefaultController implements the CRUD actions for Claims model.
 */
class DefaultController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Claims models.
     * @return mixed
     */
    public function actionIndex($request_status ="",$approve_datequery="",$request_query="")
    {

        $searchModel = new ClaimsSearch();
         $params=Yii::$app->request->queryParams;
         
        if(isset($params['ClaimsSearch']))
         {
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
         }
         else
         {
            $dataProvider = $searchModel->search([$searchModel->formName() => [
                                                                                'request_status'=>$request_status,
                                                                                'approve_datequery'=>$approve_datequery,
                                                                                'request_query'=>$request_query
                                                                              ]
                                                ]);

         }
        
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }

    /**
     * Displays a single Claims model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Claims model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Claims();
        $session = Yii::$app->session['username'];
        $model->request_date = Mongodate::now();
        $model->claim_type = 0;
        $api = Yii::$app->params['config']['api'];

        if ($model->load(Yii::$app->request->post())) {
            
            $model->request_status = 2;
            $model->company_registration_docs  = UploadedFile::getInstances($model, 'company_registration_docs');
            $model->company_registration_docs = Claims::uploadCompanyRegistrationDocs($model, $api);

            $model->authorized_director_citizen_cards  = UploadedFile::getInstances($model, 'authorized_director_citizen_cards');
            $model->authorized_director_citizen_cards = Claims::uploadAuthorizedDirectorCitizenCards($model, $api);

            $model->attorney_citizen_cards  = UploadedFile::getInstances($model, 'attorney_citizen_cards');
            $model->attorney_citizen_cards = Claims::uploadAttorneyCitizenCards($model, $api);

            $model->authority_docs  = UploadedFile::getInstances($model, 'authority_docs');
            $model->authority_docs = Claims::uploadAuthorityDocs($model, $api);

            if($model->save()){
                return $this->redirect(['index']);
            }

        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Claims model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'update';

        $oldRequestDate = $model->request_date;

        $session = Yii::$app->session;
    

        if ($model->load(Yii::$app->request->post())) { 
            if(isset($_POST['reject']))
            {
                $model->approve_date = null;
                $this->apiClaim('rejectClaim',
                                        (string)$model->_id,
                                        $session['username'],
                                        $model->remarks['remark']
                                    );

            }else{
                $model->approve_date = Mongodate::now();
                $this->apiClaim('approveClaim',
                                        (string)$model->_id,
                                        $session['username'],
                                        $model->remarks['remark']
                                    );
            }

            $model->request_date = $oldRequestDate;
            $model->lastEdit_date = Mongodate::now();
            $model->remarks = (object) [
                                "agent_id" => $session['username'],
                                "remark" => $model->remarks['remark'],
                                "date" => Mongodate::now(),
                            ];

            if($model->update())
                return $this->redirect(['index']);
                //return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Claims model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Claims model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Claims the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Claims::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionBusinessList($q = null, $id = null) 
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {

            $businessDatas = Businesses::find()->select(['_id','name.en'])->where(['like', 's_name.en', $q])->all();
           
            $promise = new Promise();
            if(sizeof($businessDatas) > 0)
            {
                $index = 0;
                $newDatas = [];
                foreach($businessDatas as $businessData)
                {
                    $newDatas[$index]['id'] = (string)$businessData['_id'];
                    $newDatas[$index]['text'] = $businessData['name']['en'];
                    $index++;
                    if($index == sizeof($businessDatas)){
                        $promise->resolve($newDatas);
                    }
                }
            }else{
                $promise->resolve([]);
            }
 
            $out['results'] = $promise->wait();

        }
        else if ($id > 0) {
            $out['results'] = ['id' => $_id, 'text' => Businesses::find($id)->name['en']];
        }
        return $out;
    }

    public function actionUserList($q = null, $id = null) 
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {

            $userDatas = Users::find()->where(['like', 'email', $q])->all();

            $promise = new Promise();
            if(sizeof($userDatas) > 0)
            {
                $index = 0;
                $newDatas = [];
                foreach($userDatas as $userData)
                {
                    $newDatas[$index]['id'] = (string)$userData['_id'];
                    $newDatas[$index]['text'] = $userData['email'];
                    $index++;
                    if($index == sizeof($userDatas)){
                        $promise->resolve($newDatas);
                    }
                }
            }else{
                $promise->resolve([]);
            }
 
            $out['results'] = $promise->wait();

        }
        else if ($id > 0) {
            $out['results'] = ['id' => $_id, 'text' => Businesses::find($id)->name['en']];
        }
        return $out;
    }

    public function actionUserInfo($email){
        $user = Users::find()->where(['email' => $email])->one();
        return \yii\helpers\Json::encode($user);

    }
    public function actionBusinessInfo($business_id){
        $business_object_id = new \MongoDB\BSON\ObjectID($business_id); 

        $business = Businesses::find()->where(['_id' => $business_object_id])->one();

        return \yii\helpers\Json::encode($business);

    }

    private function apiClaim($typeClaim,$claimId,$agentId,$reason){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => Yii::$app->params['config']['apiClaim'].$typeClaim,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "claimId=". $claimId ."&agent_id=". $agentId ."&reason=". $reason,
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
            "x-api-key: smelink1234"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
        echo $response;
        }
    }

    public function actionClaim_report()
    {
        $searchModel = new ClaimsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
     
        return $this->render('claim_report', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,

        ]);

        
    }
}
