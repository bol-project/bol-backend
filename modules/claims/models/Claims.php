<?php

namespace app\modules\claims\models;

use Yii;
use app\helpers\API;


/**
 * This is the model class for collection "claims".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $company_registration_docs
 * @property mixed $authorized_director_citizen_cards
 * @property mixed $attorney_citizen_cards
 * @property mixed $authority_docs
 * @property mixed $business_id
 * @property mixed $user_id
 * @property mixed $request_status
 * @property mixed $remarks
 * @property mixed $request_date
 * @property mixed $lastEdit_date
 * @property mixed $approve_date
 * @property mixed $claim_type
 */
class Claims extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static $approve_datequery;
    
    public static function collectionName()
    {
        return ['matchlink', 'claims'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'company_registration_docs',
            'authorized_director_citizen_cards',
            'attorney_citizen_cards',
            'authority_docs',
            'business_id',
            'business_id_nameen',
            'business_id_nameth',
            'business_id_nameno',
            'user_id',
            'username',
            'firstname',
            'lastname',
            'request_status',
            'request_query',
            'remarks',
            'request_date',
            'lastEdit_date',
            'approve_date',
            'claim_type',
            'approve_datequery'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_registration_docs', 'authorized_director_citizen_cards', 'attorney_citizen_cards', 'authority_docs', 'business_id', 'user_id', 'request_status', 'remarks', 'request_date', 'lastEdit_date', 'approve_date', 'claim_type'], 'safe'],
            [['company_registration_docs', 'authorized_director_citizen_cards', 'user_id', 'business_id'],'required'],
            ['remarks', 'required', 'on' => 'update']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'company_registration_docs' => 'Company Registration Docs',
            'authorized_director_citizen_cards' => 'Authorized Director Citizen Cards',
            'attorney_citizen_cards' => 'Attorney Citizen Cards',
            'authority_docs' => 'Authority Docs',
            'business_id' => 'Business ID',
            'user_id' => 'User ID',
            'request_status' => 'Request Status',
            'remarks' => 'Remarks',
            'request_date' => 'Request Date',
            'lastEdit_date' => 'Last Edit Date',
            'approve_date' => 'Approve Date',
            'claim_type' => 'Claim Type',
        ];
    }

    public static function uploadCompanyRegistrationDocs($model, $api){
        if($model->company_registration_docs){
            return API::uploadBusinessClaimDocument($model->company_registration_docs, $api);
        }else{
            return $model->getOldAttribute('company_registration_docs');
        }
    }

    public static function uploadAuthorizedDirectorCitizenCards($model, $api){
        if($model->company_registration_docs){
            return API::uploadBusinessClaimDocument($model->authorized_director_citizen_cards, $api);
        }else{
            return $model->getOldAttribute('authorized_director_citizen_cards');
        }
    }
    public static function uploadAttorneyCitizenCards($model, $api){
        if($model->company_registration_docs){
            return API::uploadBusinessClaimDocument($model->attorney_citizen_cards, $api);
        }else{
            return $model->getOldAttribute('attorney_citizen_cards');
        }
    }

    public static function uploadAuthorityDocs($model, $api){
        if($model->company_registration_docs){
            return API::uploadBusinessClaimDocument($model->authority_docs, $api);
        }else{
            return $model->getOldAttribute('authority_docs');
        }
    }
}
