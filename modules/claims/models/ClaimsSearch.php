<?php

namespace app\modules\claims\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\claims\models\Claims;
use app\modules\claims\controllers\ClaimType;
use app\models\MongoDate;
use app\modules\users\models\Users;
use app\modules\businesses\models\Businesses;

/**
 * ClaimsSearch represents the model behind the search form about `app\modules\claims\models\Claims`.
 */
class ClaimsSearch extends Claims
{
    /**
     * @inheritdoc
     */
     public function rules()
    {
        return [
            [['_id', 'company_registration_docs', 'authorized_director_citizen_cards', 'attorney_citizen_cards', 'authority_docs', 'business_id', 
               'business_id_nameen','business_id_nameth','business_id_nameno','user_id','firstname','lastname', 'request_status', 'request_query','remarks', 'request_date', 'lastEdit_date', 'approve_date', 'claim_type','approve_datequery'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Claims::find()
                    ->where(['not', 'company_registration_docs' , ['$exists'=>0]])
                    ->where(['not', 'authorized_director_citizen_cards',['$exists'=>0]]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $user='';
        $RequestDate='';
        $ApproveDate='';
        $RequestStatus='';
        $minRequestDate = ''; 
        $maxRequestDate = '';
        $business_id=[];

        if(isset($params['ClaimsSearch']))
        {
                $userid=$this->user_id;   
                if(!empty($userid))
                {
                    
                    $usersloop=Users::find()
                            ->andwhere(['like','email',$userid])
                            ->all();

                        foreach ($usersloop as $key => $value) 
                        { 
                            $user[]=(String)$value['_id'];    
                        }
                }
                
               

                $request_status=$this->request_status;
            

                if(isset($request_status))
                {
                    if($request_status!=null)
                    {
                        if($request_status==2) 
                        {
                            $RequestStatus=[2,3];
                        }
                        else
                        {
                            $RequestStatus=(int)$request_status;      
                        }
                             
                    }
                }
                 $requestDate= explode('/', $this->request_date); 
                if(isset($requestDate[1]))
                {
                    
                    $year=substr($requestDate[2],0,4);
                    $requestDate[4]=(int)substr($requestDate[2],6,6)+1;

                    $minRequestDate = $requestDate[1].'/'.$requestDate[0].'/'.$year; 
                    $maxRequestDate = $requestDate[1].'/'.$requestDate[4].'/'.$year;
                 
                }

                $business= $this->business_id; 
                if(!empty($business))
                {
                    if (strlen($business) >5)
                    {
                        $search=$business;
                    }
                    else
                    {
                        $search='%'.$business.'%';
                    }

                        $businessloop=Businesses::find()
                                    ->andwhere(['like','name.en',$search])
                                    ->all();

                        foreach ($businessloop as $key => $value)
                        {
                            $business_id[]=(String)$value['_id'];
                        }

                }

                $approve_date=explode('/',$this->approve_date);
                if(isset($approve_date[1]))
                {
                    $year=substr($approve_date[2],0,4);
                    $ApproveDate = $approve_date[1].'/'.$approve_date[0].'/'.$year; 

                }  

                $approve_datequery=$this->approve_datequery;
                if(isset($approve_datequery))
                {
                   
                        $approve_datequery=(int)$approve_datequery;
                         
                }
                else
                {
                        $approve_datequery=1;
                }


                
                          
        }
       
        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
              ->andFilterWhere(['business_id'=>$business_id])
              ->andFilterWhere(['user_id'=>$user])
              ->andFilterWhere(['request_status'=>$RequestStatus])
              ->andFilterWhere(['>=', 'approve_date',MongoDate::timestampToMongoDate(strtotime($ApproveDate))])
              ->andFilterWhere(['>=', 'request_date', MongoDate::timestampToMongoDate(strtotime($minRequestDate))])
              ->andFilterWhere(['<', 'request_date', MongoDate::timestampToMongoDate(strtotime($maxRequestDate))]);
            
            return $dataProvider;
    }
}
