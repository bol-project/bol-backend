<?php

// use yii\helpers\Html;
// use yii\widgets\DetailView;
use app\models\MongoDate;
use app\modules\users\models\Users;
use app\modules\businesses\models\Businesses;
use app\modules\claims\controllers\RequestStatus;
use app\modules\claims\controllers\ClaimType;
use kartik\helpers\Html;
use kartik\detail\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\claims\models\Claims */

$this->title = "View Claims";
$this->params['breadcrumbs'][] = ['label' => 'Claims', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="claims-view">

    <h1><?= Html::encode($this->title) ?></h1>
   
    <div class="box box-danger">

    <div class="box-header with-border">
        <p>
            
            <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
            <?php /*
            <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ]) ;
            ?>
            <?= Html::a('Index', ['/claims'], ['class'=>'btn btn-success']) ?>
            */ ?>
        </p>
    </div>

    <div class="box box-danger">
            <?php
                echo DetailView::widget([
                    'model' => $model,
                    'condensed'=>true,
                    'hover'=>true,
                    'enableEditMode' => false,
                    'mode'=>DetailView::MODE_VIEW,
                    'attributes'=>[
                        'columns' => 
                            [
                                'group' => true,
                                'label' => 'Claimer Information',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [
                                'label' => 'Avatar',
                                'format'=>'raw',
                                'value' => isset(Users::findOne($model->user_id)->avatar) ? '<img src='.Users::findOne($model->user_id)->avatar.' width="60" height="60">' : null,
                            ],
                            [
                                'label' => 'E-Mail',
                                'value' => isset(Users::findOne($model->user_id)->email) ? Users::findOne($model->user_id)->email : null,
                            ],
                            [
                                'label' => 'Name',
                                'value' => isset(Users::findOne($model->user_id)->first_name) ? Users::findOne($model->user_id)->first_name.' '.Users::findOne($model->user_id)->last_name : null
                            ],
                            [
                                'label' => 'Mobile',
                                'value' => isset(Users::findOne($model->user_id)->mobile) ? Users::findOne($model->user_id)->mobile : null
                            ],
                            [
                                'label' => 'Gender',
                                'value' => isset(Users::findOne($model->user_id)->gender) ? Users::findOne($model->user_id)->gender : null
                            ],
                            //-------------------------------------------------------------------------------------------------------------
                            [
                                'group' => true,
                                'label' => 'Business Information',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [
                                'label' => 'Logo',
                                'format'=>'raw',
                                'value' => isset(Businesses::findOne($model->business_id)->logo) ? '<img src='.Businesses::findOne($model->business_id)->logo.' width="60" height="60">' : null,
                            ],
                            [
                                'label' => 'Business Name',
                                'value' => isset(Businesses::findOne($model->business_id)->name['en']) ? Businesses::findOne($model->business_id)->name['en'] : null,
                            ],
                            //-------------------------------------------------------------------------------------------------------------
                            [
                                'group' => true,
                                'label' => 'Status',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [   
                                'label' => 'Status', 
                                'format'=>'raw',
                                'value' => isset($model->request_status) ?  
                                    call_user_func(function($model){
                                        if($model->request_status == RequestStatus::$VERIFIED){
                                        return 
                                                '<small class="label bg-green">'.RequestStatus::toString($model->request_status).'</small>'; 
                                        } else if($model->request_status == RequestStatus::$PENDING) {
                                            return 
                                                '<small class="label bg-yellow">'.RequestStatus::toString($model->request_status).'</small>'; 
                                        } else if($model->request_status == RequestStatus::$EXPIRE) {
                                            return 
                                                '<small class="label bg-gray">'.RequestStatus::toString($model->request_status).'</small>'; 
                                        } else {
                                            return 
                                                '<small class="label bg-red">'.RequestStatus::toString($model->request_status).'</small>'; 
                                        }
                                    },$model)
                                                
                                : '<small class="label bg-red">'.RequestStatus::toString($model->request_status).'</small>'
                            ], 
                            //-------------------------------------------------------------------------------------------------------------
                            [
                                'group' => true,
                                'label' => 'Remark',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [   
                                'label' => 'Agent', 
                                'value' => isset($model->remarks['agent_id']) ? $model->remarks['agent_id'] : ''
                            ], 
                            [   
                                'label' => 'Remark', 
                                'value' => isset($model->remarks['remark']) ? $model->remarks['remark'] : ''
                            ], 
                            //-------------------------------------------------------------------------------------------------------------
                            [
                                'group' => true,
                                'label' => 'Date',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [   
                                'label' => 'Request Date', 
                                'value' => isset($model->request_date) ? MongoDate::mongoDateToString($model->request_date) : ''
                            ], 
                            [   
                                'label' => 'Approve Date', 
                                'value' => isset($model->approve_date) ? MongoDate::mongoDateToString($model->approve_date) : ''
                            ], 
                            [   
                                'label' => 'Last edit Date', 
                                'value' => isset($model->lastEdit_date) ? MongoDate::mongoDateToString($model->lastEdit_date) : ''
                            ], 
                    ]
                ]);
            ?>
        </div>

        <div class="box-body">
        <?php
            echo    '<span>
                        <label>Company Registration Docs</label>
                    </span>
                    <div style="background-color: white;border-radius: 4px;border: 1px;border-style: solid;border-color: #d2d6de;margin-bottom: 15px;min-height: 35px;line-height: 35px; padding:10px">';
            $items = array();
            if(sizeof($model->company_registration_docs)){
                foreach ($model->company_registration_docs as $item) {
                    $pic['url'] = $item;
                    $pic['src'] = $item;
                    $pic['options']['title'] = 'Company Registration Docs';
                    array_push($items,$pic);
                }
                echo dosamigos\gallery\Gallery::widget(['items' => $items]);
            }else{
                echo 'Not found Documents';
            }
            echo '</div>';
        ?>

        <?php
            echo    '<span>
                        <label>Company Registration Docs</label>
                    </span>
                    <div style="background-color: white;border-radius: 4px;border: 1px;border-style: solid;border-color: #d2d6de;margin-bottom: 15px;min-height: 35px;line-height: 35px; padding:10px">';
            $items = array();
            if(sizeof($model->authorized_director_citizen_cards)){
                foreach ($model->authorized_director_citizen_cards as $item) {
                    $pic['url'] = $item;
                    $pic['src'] = $item;
                    $pic['options']['title'] = 'Company Registration Docs';
                    array_push($items,$pic);
                }
                echo dosamigos\gallery\Gallery::widget(['items' => $items]);
            }else{
                echo 'Not found Documents';
            }
            echo '</div>';
        ?>

        <?php
            echo    '<span>
                        <label>Company Registration Docs</label>
                    </span>
                    <div style="background-color: white;border-radius: 4px;border: 1px;border-style: solid;border-color: #d2d6de;margin-bottom: 15px;min-height: 35px;line-height: 35px; padding:10px">';
            $items = array();
            if(sizeof($model->attorney_citizen_cards)){
                foreach ($model->attorney_citizen_cards as $item) {
                    $pic['url'] = $item;
                    $pic['src'] = $item;
                    $pic['options']['title'] = 'Company Registration Docs';
                    array_push($items,$pic);
                }
                echo dosamigos\gallery\Gallery::widget(['items' => $items]);
            }else{
                echo 'Not found Documents';
            }
            echo '</div>';
        ?>

        <?php
            echo    '<span>
                        <label>Company Registration Docs</label>
                    </span>
                    <div style="background-color: white;border-radius: 4px;border: 1px;border-style: solid;border-color: #d2d6de;margin-bottom: 15px;min-height: 35px;line-height: 35px; padding:10px">';
            $items = array();
            if(sizeof($model->authority_docs)){
                foreach ($model->authority_docs as $item) {
                    $pic['url'] = $item;
                    $pic['src'] = $item;
                    $pic['options']['title'] = 'Company Registration Docs';
                    array_push($items,$pic);
                }
                echo dosamigos\gallery\Gallery::widget(['items' => $items]);
            }else{
                echo 'Not found Documents';
            }
            echo '</div>';
        ?>


    
    </div>


</div>

<script type="text/javascript">
    $(function(){
        $('.gallery-item img').css({'width' : '100px','height' : '100px'});
        $('.blueimp-gallery').css({'display':'none'});
    })
</script>