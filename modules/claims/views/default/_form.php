<?php

//use yii\helpers\Html;
//use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use app\models\MongoDate;
use app\modules\users\models\Users;
use app\modules\businesses\models\Businesses;
use app\modules\claims\controllers\RequestStatus;
use app\modules\claims\controllers\ClaimType;
use kartik\select2\Select2;
use yii\web\JsExpression;
use dosamigos\datepicker\DatePicker;
use kartik\helpers\Html;
use kartik\detail\DetailView;


/* @var $this yii\web\View */
/* @var $model app\modules\claims\models\Claims */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="claims-form">

    
        <div class="box box-info">
            <?php
                echo DetailView::widget([
                    'model' => $model,
                    'condensed'=>true,
                    'hover'=>true,
                    'enableEditMode' => $model->isNewRecord ? true : false,
                    'mode'=>DetailView::MODE_EDIT,
                    'formOptions' => [
                        'options' => ['enctype' => 'multipart/form-data']
                    ],
                    'panel'=>[
                        'heading'=>'Claim' ,
                        'type'=>DetailView::TYPE_INFO,
                    ],
                    
                    'buttons1'=>'{update}',
                    'attributes'=>[
                        'columns' => 
                            [
                                'group' => true,
                                'label' => 'Claimer Information',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [
                                'label' => 'Avatar',
                                'format'=>'raw',
                                'displayOnly' => true,     
                                'rowOptions' => ['id' => 'avatar'],                                                                                                                           
                                'value' => isset(Users::findOne($model->user_id)->avatar) ? '<img src='.Users::findOne($model->user_id)->avatar.' width="60" height="60">' : null,
                            ],
                            [
                                'label' => 'E-Mail',
                                'attribute' => 'user_id',
                                'value' => isset(Users::findOne($model->user_id)->email) ? Users::findOne($model->user_id)->email : null,
                                'type'=> DetailView::INPUT_SELECT2,
                                'widgetOptions' => [
                                    'initValueText' => empty($model->user_id) ? '' : Users::findOne($model->user_id)->email, // set the initial display text
                                    'options' => ['placeholder' => 'Search for a email user ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'minimumInputLength' => 3,
                                        'language' => [
                                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                        ],
                                        'ajax' => [
                                            'url' => \yii\helpers\Url::to(['user-list']),
                                            'dataType' => 'json',
                                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                        ],
                                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                        'templateResult' => new JsExpression("function(user) { return user.text ? user.text : user.first_name +' '+user.last_name; }"),
                                        'templateSelection' => new JsExpression("function (user) { return user.text ? user.text : user.first_name +' '+user.last_name; }"),
                                    ],
                                   
                                ]
                            ],
                            [
                                'label' => 'Name',
                                'displayOnly' => true, 
                                'rowOptions' => ['id' => 'name'],                                                                                                                                                                                                                                                          
                                'value' => isset(Users::findOne($model->user_id)->first_name) && isset(Users::findOne($model->user_id)->last_name)  
                                                ? Users::findOne($model->user_id)->first_name.' '.Users::findOne($model->user_id)->last_name : null
                            ],
                            [
                                'label' => 'Mobile',
                                'rowOptions' => ['id' => 'mobile'],                                                                                                                                                                                                                                                                                          
                                'displayOnly' => true,                                                                                                
                                'value' => isset(Users::findOne($model->user_id)->mobile) ? Users::findOne($model->user_id)->mobile : null
                            ],
                            [
                                'label' => 'Gender',
                                'rowOptions' => ['id' => 'gender'],                                                                                                                                                                                                                                                                                                                          
                                'displayOnly' => true,                                                                                                                                
                                'value' => isset(Users::findOne($model->user_id)->gender) ? Users::findOne($model->user_id)->gender : null
                            ],
                            //-------------------------------------------------------------------------------------------------------------
                            [
                                'group' => true,
                                'label' => 'Business Information',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [
                                'label' => 'Logo',
                                'format'=>'raw',
                                'rowOptions' => ['id' => 'logo'],                                                                                                                                                                                                                                                                                                                          
                                'displayOnly' => true,                                                                                                                                
                                'value' => isset(Businesses::findOne($model->business_id)->logo) ? '<img src='.Businesses::findOne($model->business_id)->logo.' width="60" height="60">' : null,
                            ],
                            [
                                'label' => 'Business Name',
                                'attribute' => 'business_id',
                                'type'=> DetailView::INPUT_SELECT2,                                                                                                                                                               
                                'value' => isset(Businesses::findOne($model->business_id)->name['en']) ? Businesses::findOne($model->business_id)->name['en'] : null,
                                'widgetOptions' => [
                                    'initValueText' => empty(Businesses::findOne($model->business_id)->name['en']) ? '' : Businesses::findOne($model->business_id)->name['en'], // set the initial display text
                                    'options' => ['placeholder' => 'Search for business ...'],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                        'minimumInputLength' => 3,
                                        'language' => [
                                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                        ],
                                        'ajax' => [
                                            'url' => \yii\helpers\Url::to(['business-list']),
                                            'dataType' => 'json',
                                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                        ],
                                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                        'templateResult' => new JsExpression("function(business) { return business.text ? business.text : business.name.en; }"),
                                        'templateSelection' => new JsExpression("function (business) { return business.text ? business.text : business.name.en; }"),
                                    ],
                                        
                                ]
                            ],
                            //-------------------------------------------------------------------------------------------------------------
                            [
                                'group' => true,
                                'label' => 'Claim Status',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [   
                                'label' => 'Status', 
                                'format'=>'raw',
                                'displayOnly' => true,                                                                                                                                
                                'value' => call_user_func(function($model){
                                    if(isset($model->request_status) ){
                                        if($model->request_status == RequestStatus::$VERIFIED){
                                            return 
                                                '<small class="label bg-green">'.RequestStatus::toString($model->request_status).'</small>'; 
                                        } else if($model->request_status == RequestStatus::$PENDING) {
                                            return 
                                                '<small class="label bg-yellow">'.RequestStatus::toString($model->request_status).'</small>'; 
                                        } else if($model->request_status == RequestStatus::$EXPIRE) {
                                            return 
                                                '<small class="label bg-gray">'.RequestStatus::toString($model->request_status).'</small>'; 
                                        } else {
                                            return 
                                                '<small class="label bg-red">'.RequestStatus::toString($model->request_status).'</small>'; 
                                        }
                                    }else{
                                        return null;
                                    }
                                
                                    },$model)
                            ], 
                            //-------------------------------------------------------------------------------------------------------------
                            [
                                'group' => true,
                                'label' => 'Claim Remark',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [   
                                'label' => 'Agent', 
                                'displayOnly' => true,                                                                                                                                
                                'value' => isset($model->remarks['agent_id']) ? $model->remarks['agent_id'] : null
                            ],
                            /* 
                            [   
                                'label' => 'Remark', 
                                'value' => isset($model->remarks['remark']) ? $model->remarks['remark'] : ''
                            ], 
                            */
                            //-------------------------------------------------------------------------------------------------------------
                            [
                                'group' => true,
                                'label' => 'Claim Date',
                                'rowOptions' => ['class' => 'info'],
                            ],
                            [   
                                'label' => 'Request Date', 
                                'displayOnly' => true,                                                                                                                                                                
                                'value' => isset($model->request_date) ? MongoDate::mongoDateToString($model->request_date) : null
                            ], 
                            [   
                                'label' => 'Approve Date', 
                                'displayOnly' => true,                                                                                                                                                                
                                'value' => isset($model->approve_date) ? MongoDate::mongoDateToString($model->approve_date) : null
                            ], 
                            [   
                                'label' => 'Last edit Date', 
                                'displayOnly' => true,                                                                                                                                                                
                                'value' => isset($model->lastEdit_date) ? MongoDate::mongoDateToString($model->lastEdit_date) : null
                            ], 
                            [   
                                'label' => 'Company Registration Docs', 
                                'attribute' => 'company_registration_docs[]' ,                                
                                'format'=>'raw',                                                                                                                                                                                            
                                'type' => DetailView::INPUT_FILEINPUT,  
                                'value' => call_user_func(function($model){
                                   
                                    $items = array();
                                    if(sizeof($model->company_registration_docs)){
                                        foreach ($model->company_registration_docs as $item) {
                                            $pic['url'] = $item;
                                            $pic['src'] = $item;
                                            $pic['options']['title'] = 'Company Registration Docs';
                                            array_push($items,$pic);
                                        }
                                        return dosamigos\gallery\Gallery::widget(['items' => $items ]);
                                    }else{
                                        return 'Not found Documents';
                                    }
                                }, $model) ,
                                'options' => ['multiple' => true],
                                'model' => $model,
                                'widgetOptions' => [
                                    'pluginOptions' => [
                                        'showPreview' => true,
                                        'showCaption' => true,
                                        'showRemove' => false,
                                        'showUpload' => false,   
                                                        
                                        'fileActionSettings' => [
                                            'indicatorNew' => '',
                                        ]
                                    ]
                                ]

                            ], 
                            [   
                                'label' => 'Authorized Director Citizen Cards', 
                                'attribute' => 'authorized_director_citizen_cards[]' ,                                
                                'format'=>'raw',                                                                                                                                                                                            
                                'type' => DetailView::INPUT_FILEINPUT,  
                                'value' => call_user_func(function($model){
                                   
                                    $items = array();
                                    if(sizeof($model->authorized_director_citizen_cards)){
                                        foreach ($model->authorized_director_citizen_cards as $item) {
                                            $pic['url'] = $item;
                                            $pic['src'] = $item;
                                            $pic['options']['title'] = 'Authorized Director Citizen Cards';
                                            array_push($items,$pic);
                                        }
                                        return dosamigos\gallery\Gallery::widget(['items' => $items ]);
                                    }else{
                                        return 'Not found Documents';
                                    }
                                }, $model) ,
                                'options' => ['multiple' => true],
                                'model' => $model,
                                'widgetOptions' => [
                                    'pluginOptions' => [
                                        'showPreview' => true,
                                        'showCaption' => true,
                                        'showRemove' => false,
                                        'showUpload' => false,   
                                                        
                                        'fileActionSettings' => [
                                            'indicatorNew' => '',
                                        ]
                                    ]
                                ]

                            ], 
                            [   
                                'label' => 'Attorney Citizen Cards', 
                                'attribute' => 'attorney_citizen_cards[]' ,                                
                                'format'=>'raw',                                                                                                                                                                                            
                                'type' => DetailView::INPUT_FILEINPUT,  
                                'value' => call_user_func(function($model){
                                   
                                    $items = array();
                                    if(sizeof($model->attorney_citizen_cards)){
                                        foreach ($model->attorney_citizen_cards as $item) {
                                            $pic['url'] = $item;
                                            $pic['src'] = $item;
                                            $pic['options']['title'] = 'Attorney Citizen Cards';
                                            array_push($items,$pic);
                                        }
                                        return dosamigos\gallery\Gallery::widget(['items' => $items ]);
                                    }else{
                                        return 'Not found Documents';
                                    }
                                }, $model) ,
                                'options' => ['multiple' => true],
                                'model' => $model,
                                'widgetOptions' => [
                                    'pluginOptions' => [
                                        'showPreview' => true,
                                        'showCaption' => true,
                                        'showRemove' => false,
                                        'showUpload' => false,   
                                                        
                                        'fileActionSettings' => [
                                            'indicatorNew' => '',
                                        ]
                                    ]
                                ]

                            ], 
                            [   
                                'label' => 'Authority Docs', 
                                'attribute' => 'authority_docs[]' ,                                
                                'format'=>'raw',                                                                                                                                                                                            
                                'type' => DetailView::INPUT_FILEINPUT,  
                                'value' => call_user_func(function($model){
                                   
                                    $items = array();
                                    if(sizeof($model->authority_docs)){
                                        foreach ($model->authority_docs as $item) {
                                            $pic['url'] = $item;
                                            $pic['src'] = $item;
                                            $pic['options']['title'] = 'Authority Docs';
                                            array_push($items,$pic);
                                        }
                                        return dosamigos\gallery\Gallery::widget(['items' => $items ]);
                                    }else{
                                        return 'Not found Documents';
                                    }
                                }, $model) ,
                                'options' => ['multiple' => true],
                                'model' => $model,
                                'widgetOptions' => [
                                    'pluginOptions' => [
                                        'showPreview' => true,
                                        'showCaption' => true,
                                        'showRemove' => false,
                                        'showUpload' => false,   
                                                        
                                        'fileActionSettings' => [
                                            'indicatorNew' => '',
                                        ]
                                    ]
                                ]

                            ], 
                    ]
                ]);
            ?>
        </div>

     

        </div>
        

        <?php if(!$model->isNewRecord) { ?>
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
                <div class="box box-info">   
                    <div class="box-body">
                         <?= $form->field($model, 'remarks[remark]')->textarea(['placeholder' => 'Reason...'])->label('Reason') ?>
                    
                    </div>
                </div>

            <div class="text-right">
                <?= Html::submitButton('Approve', ['class'=>'btn btn-success']) ?>
                <?= Html::submitButton('Reject', ['class'=>'btn btn-danger','name' => 'reject']) ?>
            </div>
            <?php ActiveForm::end(); ?>

        <?php } ?>
  

        <?php if($model->isNewRecord) { ?>
            <div class="text-right">
                <?= Html::Button('Create', ['class'=>'btn btn-success', 'id' => 'create']) ?>        
                <?= Html::a('Cancel', ['/claims'], ['class'=>'btn btn-danger']) ?>
            </div>
        <?php } ?>
   
   
</div>

<script type="text/javascript">
    $(function(){
        $('.gallery-item img').css({'width' : '100px','height' : '100px', 'border': '#d2d6de 1px solid'});
        $('.blueimp-gallery').css({'display':'none'});
    })

    $(document).ready(function(){
        setTimeout(function() {
            
         $('.select2-selection__arrow').css('height', '92%');
        }, 100);        

        $('#create').click(function(){
            $('.kv-btn-save').click();
        });
        
    });
</script>



<?php $this->registerCssFile('@web/css/claims/claims.css'); ?>
<?php $this->registerCssFile('@web/css/general_style.css'); ?>

<script>    
    $('#claims-user_id').on('select2:select', function (evt) {
        callAjaxUser(evt.params.data);
    });
    $('#claims-business_id').on('select2:select', function (evt) {
        callAjaxBusiness(evt.params.data);
        // console.log(evt.params.data);
    });

    $('.kv-btn-save').click(function(){
        
       var company_registration_docs =  $('#company_registration_docs').val();
       $("#x").val(company_registration_docs );
        
        // postAjaxToActionCreate();
    });

    function callAjaxUser(data) {
            $.ajax({
            method: "GET",
            url: "<?php echo Yii::$app->getUrlManager()->createUrl('claims/default/user-info')  ; ?>",
            data: {
                email: data.text
            },
            success: function (data) {
                var user = JSON.parse(data);
                console.log(user);
                $('#avatar td .kv-form-attribute').html('<img src="'+user.avatar+'" width=60 height=60>');
                $('#name td .kv-form-attribute').html(user.first_name+' '+user.last_name);
                $('#mobile td .kv-form-attribute').html(user.mobile);
                $('#gender td .kv-form-attribute').html(user.gender);
            
            },
            error: function (exception) {
                console.log(exception);
            }
        });
    }
    function callAjaxBusiness(data) {
            $.ajax({
            method: "GET",
            url: "<?php echo Yii::$app->getUrlManager()->createUrl('claims/default/business-info')  ; ?>",
            data: {
                business_id: data.id
            },
            success: function (data) {
                var business = JSON.parse(data);
                $('#logo td .kv-form-attribute').html('<img src="'+business.logo+'" width=60 height=60>');

            },
            error: function (exception) {
                console.log(exception);
            }
        });
    }

    // function postAjaxToActionCreate() {
    //         $.ajax({
    //         method: "POST",
    //         url: "<?php echo Yii::$app->getUrlManager()->createUrl('claims/default/create')  ; ?>",
    //         data: {
    //             company_registration_docs: $('#company_registration_docs').val(),
    //         },
    //         success: function (data) {
    //             console.log(data);
    //             // var business = JSON.parse(data);
    //             // $('#logo td .kv-form-attribute').html('<img src="'+business.logo+'" width=60 height=60>');

    //         },
    //         error: function (exception) {
    //             console.log(exception);
    //         }
    //     });
    // }

</script>

<?php $this->registerJsFile('@web/js/claims/claims.js'); ?>

