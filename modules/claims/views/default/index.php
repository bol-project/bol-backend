<?php

use app\models\MongoDate;
use yii\helpers\Html;
use yii\grid\GridView;
use app\modules\claims\controllers\RequestStatus;
use app\modules\claims\controllers\ClaimType;
use yii\helpers\ArrayHelper;
use app\modules\users\models\Users;
use app\modules\businesses\models\Businesses;
use jino5577\daterangepicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\claims\models\ClaimsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Claims';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="claims-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div class="box box-danger">

   

    <div class="box-header with-border">
    <p>
        <?= Html::a('Create Claims', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
    
   

     <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
                        'firstPageLabel'=>'<<',   
                        'prevPageLabel' =>'<', 
                        'nextPageLabel' =>'>',   
                        'lastPageLabel' =>'>>'
            ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => 'E-Mail',
                'attribute' => 'user_id',
                'format'=>'html',
                'value' => function($model){
                    $user = Users::findOne($model->user_id);
                    if(isset($user)){
                        $name = $user['email'];
                        return $name;
                    }else{
                        return null;
                    }
                }
            ],
            [
                'label' => 'Business',
                'attribute' => 'business_id',
                'format'=>'html',
                'value' => function($model){
                    $business = Businesses::findOne($model->business_id);
                    if(isset($business)){
                        $name = $business['name']['en'];
                        return $name;
                    }else{
                        return null;
                    }

                }
            ],
            [
                'attribute' => 'request_status',
                'format'=>'html',
                'filter' => Html::activeDropDownList($searchModel, 'request_status', ArrayHelper::map(RequestStatus::getStatus(), 'value','label'),
                                                                        ['class'=>'form-control','prompt' => 'Select Status']
                                                ),
                'value' => function($model){
                    if(isset($model->request_status)){
                        if($model->request_status == RequestStatus::$VERIFIED){
                           return 
                                '<small class="label bg-green">'.RequestStatus::toString($model->request_status).'</small>'; 
                        } else if($model->request_status == RequestStatus::$PENDING) {
                            return 
                                '<small class="label bg-yellow">'.RequestStatus::toString($model->request_status).'</small>'; 
                        } else if($model->request_status == RequestStatus::$EXPIRE) {
                            return 
                                '<small class="label bg-gray">'.RequestStatus::toString($model->request_status).'</small>'; 
                        } else {
                            return 
                                '<small class="label bg-red">'.RequestStatus::toString($model->request_status).'</small>'; 
                        }
                    }
                    else return null;
                }
            ],
            [
                'attribute' => 'request_date',
                'format'=>'html',
                'filter'=>DateRangePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'request_date',
                            'pluginOptions' => [
                                'autoUpdateInput' => false,
                                'locale' => [
                                    'format' => 'DD/MM/YYYY'
                                ]
                            ]
                ]),
                'value' => function($model){
                    return MongoDate::mongoDateToString($model->request_date);
                }
            ],
            [
                'attribute' => 'approve_date',
                'format'=>'html',
                'filterInputOptions' => [
                'class'=>'form-control',
                'id' => 'approve_date'
                ],
                'value' => function($model){
                    return $model->approve_date ? MongoDate::mongoDateToString($model->approve_date) : ' ';
                }
            ],
            // [
            //     'attribute' => 'lastEdit_date',
            //     'format'=>'html',
            //     'value' => function($model){
            //         return MongoDate::mongoDateToString($model->lastEdit_date);
            //     }
            // ],
        
            // [
            //     'attribute' => 'claim_type',
            //     'format'=>'html',
            //     'value' => function($model){
            //         if(isset($model->claim_type))
            //             return ClaimType::toString($model->claim_type);
            //         else return null;
            //     }
            // ],
            //'_id',
            // 'company_registration_docs',
            // 'authorized_director_citizen_cards',
            // 'attorney_citizen_cards',
            // 'authority_docs',
            //'business_id',
            //'user_id',
            //'request_status',
            // 'remarks',
            //'request_date',
            //'lastEdit_date',
            //'approve_date',
            //'claim_type',

            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{update}'],
        ],
    ]); ?>
    </div>

    </div>
</div>


<script type="text/javascript">
    $(function () {
        $("#approve_date").datepicker({format: "dd/mm/yyyy"}).datepicker();
    });
</script>
