<?php

namespace app\modules\dashboard\controllers;

use yii\web\Controller;
use app\modules\users\models\Users;
use app\modules\claims\models\Claims;
use app\modules\claims\controllers\ClaimType;
use app\controllers\AdminController;
/**
 * Default controller for the `dashboard` module
 */
class DefaultController extends AdminController
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function queryGenderAmount($gender)
    {
        $count = Users::find()
                ->where(['gender' => $gender])
                ->count();
        return $count;
    }

    public function queryGraphAgeData()
    {
          

        $choice1 = 0; $choice2 = 0; $choice3 = 0; $choice4 = 0; $choice5 = 0;

        $rows = Users::find()
                ->select(['birthday'])
                ->all();

        foreach($rows as $row)
        {
            $birthDate = (string) $row['birthday']; 


            if(!$birthDate){
                continue;
            
            } 

            if (strpos($birthDate, 'BE'))
            {
                $date = str_replace("BE","","$birthDate");
                $yearBE = substr($date, -5);
                $yearAC = ((int) substr($date, -5)) - 543;
                $date = str_replace($yearBE,$yearAC,"$date");
                $birthDate = date('m/d/Y', strtotime($date));
            }
         
           if($birthDate)
           {
                if(preg_match('/[ก-๙]/u',$birthDate))
                {   
                  $date= $this->mapMonth($birthDate);
                }
                else
                {
                  $data = date('m/d/Y', strtotime($birthDate));
                 list($day,$month,$year) = explode('/' , $data);
                 if($year>date('Y'))
                 {
                  $date=$this->Calculateyears($month,$day,$year);
                 }
                 else
                 {
                   $date = date('m/d/Y', strtotime($data));
                 }
               }   
          }
       
          $date = new \DateTime($data, new \DateTimeZone('UTC'));
               
          $now = new \DateTime();
          $interval = $now->diff($date);

           $age = $interval->y;

          if($age < 15) $choice1 += 1;
          else if($age < 20) $choice2 += 1;
          else if($age < 30) $choice3 += 1;
          else if($age < 40) $choice4 += 1;
          else if($age > 40) $choice5 += 1;
  
       
        }
        

        return [$choice1,$choice2,$choice3,$choice4,$choice5];
    }

    public function mapMonth($_str)
    {
     
      $thai_month_arr=array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน", "กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
     
      list($day,$month,$year) = explode(' ',$_str);
      foreach ($thai_month_arr as $key => $value) {
        if ( $value == $month) {
          $month = $key + 1;
        }
      }

      mktime(0,0,0,(int)$month,(int)$day,(int)$year);

    return $this->Calculateyears($month,$day,$year);
      
    }

    public function Calculateyears($month,$day,$year)
    {

      if($year>date('Y'))
      {
          $years=$year-543;
          return date('m/d/y',  mktime(0,0,0,(int)$month,(int)$day,(int)$year));
      }
      else
      {
         return date('m/d/y',  mktime(0,0,0,(int)$month,(int)$day,(int)$year));
        
      }
    }

    public function queryGraphOccupation()
    {
        $owner = 0; $gov = 0; $emp = 0; $freelance = 0; $students = 0; $unemp = 0;
        
        $rows = Users::find()
                ->select(['occupation'])
                ->all();

        foreach($rows as $row)
        {
            $occupation = $row['occupation'];
            if($occupation == 'Owner') $owner += 1;
            else if($occupation == 'Gov. Officer') $gov += 1;
            else if($occupation == 'Employee') $emp += 1;
            else if($occupation == 'Self-Employed/Freelance') $freelance += 1;
            else if($occupation == 'Students') $students += 1;
            else if($occupation == 'Umemployed/Retired') $unemp += 1;
        }

        return [$owner,$gov,$emp,$freelance,$students,$unemp];
    }

    public function queryUserStatus($status)
    {
        $count = Users::find()
                    ->where(['status' => $status])
                    ->count();
        return $count;
    }

    public function queryClaimsStatus($status)
    {
        if($status!=2)
        $st=(int)$status;
        else
        $st=[0,2];

        $count = Claims::find()
                    ->where(['request_status' =>$st])
                    ->andwhere(['claim_type' => (int)ClaimType::$CLAIMER]) 
                    ->orWhere(['claim_type' => (string)ClaimType::$CLAIMER])
                    ->count();
        return $count;
    }

    // public function queryAprove($oporator){
    //     $count = Claims::find()
    //                 ->where([$oporator,'aprove_date',null])
    //                 ->andwhere(['claim_type' => (int)ClaimType::$CLAIMER]) 
    //                 ->orWhere(['claim_type' => (string)ClaimType::$CLAIMER])
    //                 ->count();
    //     return $count;
    //  }

    // public function queryExpired($oporator){
    //     $count = Claims::find()
    //                 ->where([$oporator,'request_status',3])
    //                 ->andwhere(['claim_type' => (int)ClaimType::$CLAIMER]) 
    //                 ->orWhere(['claim_type' => (string)ClaimType::$CLAIMER])
    //                 ->count();
    //     return $count;
    // }
}

