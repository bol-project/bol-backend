<?php 
use miloschuman\highcharts\Highcharts; 
use yii\helpers\Html;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="dashboard-default-index row">
    <div class="col-md-3">
        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">Gender</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-male"></i></span>
                        <div class="info-box-content">
                        <span class="info-box-text">Male</span>
                        <span class="info-box-number"><?=Html::a($this->context->queryGenderAmount('Male'),['/users/default/index',"gender" => "Male"]); ?></span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->

                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-female"></i></span>
                        <div class="info-box-content">
                        <span class="info-box-text">Female</span>
                        <span class="info-box-number"><?=Html::a($this->context->queryGenderAmount('Female'),['/users/default/index',"gender" => "Female"]); ?></span>
                        </div><!-- /.info-box-content -->
                    </div><!-- /.info-box -->
            </div><!-- /.box-body -->
        </div>

        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">Age</h3>
            </div><!-- /.box-header -->
            <?php echo Highcharts::widget([ 
                
                'options' =>  
                [ 
                    'title' => ['text' => ''], 
                    'chart' => ['type' => 'column' ],
                    'xAxis' => [ 'categories' => ['<15', '15-20', '21-30', '30-40', '>40'] ], 
                    'yAxis' => [ 'title' => ['text' => 'Quantity'] ], 
                    'series' => [ 
                                    ['name' => 'Age', 'data' =>$this->context->queryGraphAgeData() ]
                                ] 
                ] 
            ]); ?>
        </div>



        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">Occupation</h3>
            </div><!-- /.box-header -->
            <?php echo Highcharts::widget([ 
                
                'options' =>  
                [ 
                    'title' => ['text' => ''], 
                    'chart' => ['type' => 'column' ],
                    'xAxis' => [ 'categories' => ['Owner', 'Gov. Officer', 'Employee', 'Self-Employee/Freelance', 'Students', 'Umemployed/Retired'] ], 
                    'yAxis' => [ 'title' => ['text' => 'Quantity'] ], 
                    'series' => [ 
                                    ['name' => 'Occupation', 'data' => $this->context->queryGraphOccupation() ]
                                ] 
                ] 
            ]); ?>
        </div>
    </div>

    <div class="col-md-3">
        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">User</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-ok"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Active</span>
                    <span class="info-box-number"><?= Html::a($this->context->queryUserStatus(1),['/users/default/index',"status" => 1]); ?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-remove"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Inactive</span>
                    <span class="info-box-number"><?=Html::a($this->context->queryUserStatus(0),['/users/default/index',"status" => 0]); ?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.box-body -->
        </div>

        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">Claims</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-ok"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Claimed</span>
                    <span class="info-box-number"><?=Html::a($this->context->queryClaimsStatus(1),['/claims/default/index','request_status'=>1]); ?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-remove"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Unclaim</span>
                    <span class="info-box-number"><?=Html::a($this->context->queryClaimsStatus(2),['/claims/default/index','request_status'=>2]); ?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.box-body -->
        </div>

         <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">Aprove</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-ok"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Aprove</span>
                    <span class="info-box-number"><?=Html::a($this->context->queryClaimsStatus(1),['/claims/default/index','request_status'=>1]);?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-remove"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">UnAprove</span>
                    <span class="info-box-number"><?=Html::a($this->context->queryClaimsStatus(0),['/claims/default/index','request_status'=>0]);?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.box-body -->
        </div>

        <div class="box box-solid box-danger">
            <div class="box-header">
                <h3 class="box-title">Expired</h3>
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-ok"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">Expired</span>
                    <span class="info-box-number"><?=Html::a($this->context->queryClaimsStatus(3),['/claims/default/index','request_status'=>3]);?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
                <div class="info-box">
                    <span class="info-box-icon bg-red"><i class="glyphicon glyphicon-remove"></i></span>
                    <div class="info-box-content">
                    <span class="info-box-text">InExpired</span>
                    <span class="info-box-number"><?=Html::a($this->context->queryClaimsStatus(1),['/claims/default/index','request_status'=>1]);?></span>
                    </div><!-- /.info-box-content -->
                </div><!-- /.info-box -->
            </div><!-- /.box-body -->
        </div>

        
    </div>
    

</div>
