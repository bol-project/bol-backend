<?php

namespace app\modules\marketing\modules\gallery\models;

use app\models\MongoDate;
use app\models\GenerateFileName;
use app\modules\marketing\config\config;

use Yii;

/**
 * This is the model class for collection "gallery".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $url
 * @property mixed $name
 * @property mixed $extension
 * @property mixed $mimetype
 * @property mixed $createdBy
 * @property mixed $createdAt
 * @property mixed $updatedBy
 * @property mixed $updatedAt
 */
class Gallery extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['matchlink', 'gallery'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'url',
            'name',
            'extension',
            'mimetype',
            'createdBy',
            'createdAt',
            'updatedBy',
            'updatedAt',
            'media'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url', 'name', 'extension', 'mimetype', 'createdBy', 'createdAt', 'updatedBy', 'updatedAt', 'media'], 'safe'],
            [['name'], 'required'],
            [['media'], 'required', 'on' => 'create'],
            [['media'], 'file', 'extensions' => Config::getImageExtensions().','.Config::getVideoExtensions()]
        ];
    }

    public function upload()
    {
        if (!file_exists('uploads/gallery')) {
            mkdir('uploads/gallery');
        }
        $this->media->saveAs('uploads/gallery/' . $this->name . '.' . $this->media->extension);
        return true;
    }

    public function renameMedia()
    {
        $oldMedia = "uploads/gallery/".$this->getOldAttributes()["name"].".".$this->extension;
        $newMedia = "uploads/gallery/".$this->name.".".$this->extension;
        rename($oldMedia, $newMedia);
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            // $this->updatedAt = Mongodate::now();
            if ($insert) { // insert
                $this->createdAt = Mongodate::now();
                $this->createdBy = $_SESSION["user"]["email"];
            }
            else { //update
                $this->updatedAt = Mongodate::now();
                $this->updatedBy = $_SESSION["user"]["email"];
            }
            
            $media = $this->media;
            if (!!$media) {
                $model1 = new Gallery();
                $model1->media = $media;
                $model1->name = $this->name;
                $model1->upload();

                $this->url = "http://".$_SERVER['HTTP_HOST']."/web/uploads/gallery/".$this->name.".".$this->media->extension;
                $this->mimetype = $this->media->type;
                $this->extension = $this->media->extension;
            }
            else {
                // $oldUrl = $this->getOldAttributes()["url"];
                // $extension = strrchr($oldUrl, ".");
                $this->url = "http://".$_SERVER['HTTP_HOST']."/web/uploads/gallery/".$this->name.".".$this->extension;
                $this->renameMedia();
            }
            
            unset($this->media);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'url' => 'Url',
            'name' => 'Name',
            'extension' => 'Extension',
            'mimetype' => 'Mimetype',
            'createdBy' => 'Created By',
            'createdAt' => 'Created At',
            'updatedBy' => 'Updated By',
            'updatedAt' => 'Updated At',
        ];
    }
}
