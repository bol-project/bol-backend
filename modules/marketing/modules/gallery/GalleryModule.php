<?php

namespace app\modules\marketing\modules\gallery;

/**
 * gallery module definition class
 */
class GalleryModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\marketing\modules\gallery\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
