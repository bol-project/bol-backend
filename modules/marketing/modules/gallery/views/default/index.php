<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\MongoDate;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\marketing\modules\gallery\models\GallerySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div class="box box-danger">

    <div class="box-header with-border">
    <p>
        <?= Html::a('Create Gallery', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    </div>
    
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'url',
            'name',
            'extension',
            'mimetype',
            'createdBy',
            [
                'attribute' => 'createdAt',
                'label' => 'Create Date',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return Mongodate::mongoDateToString($model->createdAt);
                }
            ],
            'updatedBy',
            [
                'attribute' => 'updatedAt',
                'label' => 'Update Date',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return Mongodate::mongoDateToString($model->updatedAt);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
    
    </div>
</div>
<script type="text/javascript">
    $('.content-header h1').css('font-size','36px');
</script>
