<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\modules\gallery\models\Gallery */

$this->title = 'Update Gallery: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'Galleries', 'url' => ['/marketing/gallery']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="gallery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
