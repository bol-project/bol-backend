<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\MongoDate;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\modules\gallery\models\Gallery */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'Galleries', 'url' => ['/marketing/gallery']];
$this->params['breadcrumbs'][] = $this->title;
function getAttributes($model){
    $attributes = [
        'url',
        'name',
        'extension',
        'mimetype',
        [
            'attribute' => 'createdAt',
            'format'=>'html',
            'value'=>Mongodate::mongoDateToString($model->createdAt)
        ],
        'createdBy',
    ];
    if (!!$model->updatedAt) {
        array_push($attributes, 
            [
                'attribute' => 'updatedAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->updatedAt)
            ],
            'updatedBy'
        );
    }
    $media = strpos($model->mimetype, 'image') !== false ? 
                "<img src='".$model->url."' width='200px'/>"
                :
                "<video width='320' height='240' controls>
                    <source src='".$model->url."' type='".$model->mimetype."'>
                    Your browser does not support the video tag.
                </video>";
    array_push($attributes, 
        [
            'label' => 'Media',
            'format'=>'raw',
            'value'=> strpos($model->mimetype, 'image') !== false ? 
                "<img src='".$model->url."' width='200px'/>"
                :
                "<video width='320' height='240' controls>
                    <source src='".$model->url."' type='".$model->mimetype."'>
                    Your browser does not support the video tag.
                </video>"
        ]
    );
    return $attributes;
}
?>
<div class="gallery-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="box box-info">
    
    <div class="box-header with-border">
    <p>
        <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>

    <div class="box-body">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => getAttributes($model)
    ]) ?>
    </div>

    </div>
</div>
