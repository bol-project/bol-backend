<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\marketing\modules\gallery\models\Gallery */

$this->title = 'Create Gallery';
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'Galleries', 'url' => ['/marketing/gallery']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="gallery-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>

</div>
