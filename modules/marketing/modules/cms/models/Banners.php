<?php

namespace app\modules\marketing\modules\cms\models;

use app\models\MongoDate;
use app\models\GenerateFileName;
use app\modules\marketing\config\services;
use app\modules\marketing\config\config;

use Yii;

/**
 * This is the model class for collection "banners".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $title
 * @property mixed $status
 * @property mixed $type_id
 * @property mixed $link
 * @property mixed $content
 * @property mixed $createdAt
 * @property mixed $updatedAt
 * @property mixed $media
 * @property mixed $upload
 */
class Banners extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['Content', 'banners'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'title',
            'status',
            'type_id',
            'link',
            'content',
            'createdAt',
            'updatedAt',
            'approvedAt',
            'createdBy',
            'updatedBy',
            'approvedBy',
            'effectiveDate',
            'online',
            'media',
            'startDate',
            'endDate',
            'submitType'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'status', 'type_id', 'link', 'content', 'createdAt', 'updatedAt', 'approvedAt', 'createdBy', 'updatedBy', 'approvedBy', 'effectiveDate', 'online', 'media', 'startDate', 'endDate', 'submitType'], 'safe'],
            [['title', 'type_id', 'content', 'media'], 'required'],
            [['media'], 'url', 'defaultScheme' => 'http'],
            [['media'], 'validateImageUrl', 'on' => 'introduction'],
            [['media'], 'validateVideoUrl', 'on' => 'video']
            // [['upload'], 'file', 'extensions' => 'png, jpg', 'on' => 'introduction'],
            // [['upload'], 'file', 'extensions' => 'mp4, avi, mov', 'on' => 'video'],
        ];
    }

    public function validateImageUrl($attribute, $params)
    {
        $imageExtensions = Config::getImageExtensions();
        $imageUrl = $this->media;
        $imageExtension = substr(strrchr($imageUrl, "."), 1);
        if (!strpos($imageExtensions, $imageExtension)) {
            $this->addError('media', 'The url of Banner type introduction must be either ".jpg" or ".png"');
        }
    }

    public function validateVideoUrl($attribute, $params)
    {
        $videoExtensions = Config::getVideoExtensions();
        $videoUrl = $this->media;
        $videoExtension = substr(strrchr($videoUrl, "."), 1);
        if (!strpos($videoExtensions, $videoExtension)) {
            $this->addError('media', 'The url of Banner type video must be ".mp4", ".avi" or "mov"');
        }
    }

    // public function upload()
    // {
    //     if (!file_exists('uploads/banners')) {
    //         mkdir('uploads/banners');
    //     }
    //     $this->upload->saveAs('uploads/banners/' . $this->title . '.' . $this->upload->extension);
    //     return true;
    // }

    public function getDateAndUser($insert)
    {
        if ($this->submitType === "save") {
            if ($insert) {
                $this->createdAt = Mongodate::now();
                $this->createdBy = $_SESSION["user"]["email"];
            }
            else {
                $this->updatedAt = Mongodate::now();
                $this->updatedBy = $_SESSION["user"]["email"];
                if (!!$this->approvedAt) {
                    $this->approvedAt = null;
                    $this->approvedBy = null;
                }
            }
        }
        if ($this->submitType === "approve") {
            $this->approvedAt = Mongodate::now();
            $this->approvedBy = $_SESSION["user"]["email"];
        }
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->online = Services::stingToBoolean($this->online);            
            $this->effectiveDate = Services::getEffectiveDate($this);
            $this->getDateAndUser($insert);
            $this->status = Services::getStatus($this->submitType);
            // $this->updatedAt = Mongodate::now();
            // $this->status = $this->status ? true : false;
            // if (!!$this->upload) {
            //     $upload = $this->upload;
            //     $model1 = new Banners();
            //     $model1->upload = $upload;
            //     $model1->title = GenerateFileName::random();
            //     $media = (object) array(
            //         'url' => "http://".$_SERVER['HTTP_HOST']."/web/uploads/banners/".$model1->title.".".$model1->upload->extension,
            //         'auto' => false,
            //         'mimetype' => $upload->type
            //     );
            //     $this->media = $media;
            //     $model1->upload();
            //     if ($insert) { // insert
            //         $this->createdAt = Mongodate::now();
            //     }
            //     else { //update
            //         $oldMedia = $this->getOldAttributes()["media"]["url"];
            //         $oldMediaPath = 'uploads/banners/'.strrchr($oldMedia, '/');
            //         if (file_exists($oldMediaPath))
            //             unlink($oldMediaPath);
            //     }
            // }
            // else {
            //     $this->media = $this->getOldAttributes()["media"];
            // }
            if (empty($this->link)) {
                $this->link = null;
            }
            unset($this->submitType);
            unset($this->startDate);
            unset($this->endDate);
            return true;
        } else {
            return false;
        }
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'title' => 'Title',
            'status' => 'Status',
            'type_id' => 'Type ID',
            'link' => 'Link',
            'content' => 'Content',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'media' => 'Media',
        ];
    }
}
