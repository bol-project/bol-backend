<?php

namespace app\modules\marketing\modules\cms\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\marketing\modules\cms\models\Contents;

/**
 * ContentsSearch represents the model behind the search form about `app\modules\marketing\modules\cms\models\Contents`.
 */
class ContentsSearch extends Contents
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['_id', 'title', 'slug', 'status', 'image', 'url', 'content', 'ranking', 'views', 'likes', 'shares', 'createdAt', 'updatedAt', 'approvedAt', 'createdBy', 'updatedBy', 'approvedBy', 'effectiveDate', 'online', 'upload'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $slug = null)
    {
        $query = Contents::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', '_id', $this->_id])
            ->andFilterWhere(['like', 'title', $this->title])
            // ->andFilterWhere(['like', 'slug', $slug])
            ->andFilterWhere(['slug' => $slug])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'url', $this->url]) 
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'ranking', $this->ranking])
            ->andFilterWhere(['like', 'views', $this->views])
            ->andFilterWhere(['like', 'likes', $this->likes])
            ->andFilterWhere(['like', 'shares', $this->shares])
            ->andFilterWhere(['like', 'createdAt', $this->createdAt])
            ->andFilterWhere(['like', 'updatedAt', $this->updatedAt])
            ->andFilterWhere(['like', 'approvedAt', $this->approvedAt])
            ->andFilterWhere(['like', 'createdBy', $this->createdBy])
            ->andFilterWhere(['like', 'updatedBy', $this->updatedBy])
            ->andFilterWhere(['like', 'approvedBy', $this->approvedBy])
            ->andFilterWhere(['like', 'effectiveDate', $this->effectiveDate])
            ->andFilterWhere(['like', 'online', $this->online]);
        if (gettype($this->slug) != "array") {
            $query->andFilterWhere(['like', 'slug', $this->slug]);
        }

        return $dataProvider;
    }
}
