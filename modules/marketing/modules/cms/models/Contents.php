<?php

namespace app\modules\marketing\modules\cms\models;

use app\models\MongoDate;
use app\models\GenerateFileName;
use app\modules\marketing\config\services;
use app\modules\marketing\config\config;

use Yii;

/**
 * This is the model class for collection "contents".
 *
 * @property \MongoDB\BSON\ObjectID|string $_id
 * @property mixed $title
 * @property mixed $slug
 * @property mixed $status
 * @property mixed $image
 * @property mixed $url
 * @property mixed $content
 * @property mixed $ranking
 * @property mixed $views
 * @property mixed $likes
 * @property mixed $shares
 * @property mixed $createdAt
 * @property mixed $updatedAt
 * @property mixed $approvedAt
 * @property mixed $createdBy
 * @property mixed $updatedBy
 * @property mixed $approvedBy
 * @property mixed $effectiveDate
 * @property mixed $online
 */
class Contents extends \yii\mongodb\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return ['Content', 'contents'];
    }

    /**
     * @inheritdoc
     */
    public function attributes()
    {
        return [
            '_id',
            'title',
            'slug',
            'status',
            'image',
            'url',
            'content',
            'ranking',
            'views',
            'likes',
            'shares',
            'createdAt',
            'updatedAt',
            'approvedAt',
            'createdBy',
            'updatedBy',
            'approvedBy',
            'effectiveDate',
            'online',
            'detail',
            'source',
            'date',
            'link',
            'upload',
            'startDate',
            'endDate',
            'submitType'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'slug', 'status', 'image', 'url', 'content', 'ranking', 'views', 'likes', 'shares', 'createdAt', 'updatedAt', 'approvedAt', 'createdBy', 'updatedBy', 'approvedBy', 'effectiveDate', 'online', 'detail', 'source', 'date', 'link', 'upload', 'startDate', 'endDate', 'submitType'], 'safe'],
            [['title', 'slug','content'], 'required'],
            [['views', 'likes', 'shares'], 'number','min'=>0],
            [['upload'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf', 'on' => 'createManual'],
            [['image', 'detail'], 'required', 'on' => ['createNews', 'updateNews']],
            [['image'], 'url', 'defaultScheme' => 'http', 'on' => ['createNews', 'updateNews']],
            [['image'], 'validateImageUrl', 'on' => ['createNews', 'updateNews']]
        ];
    }

    public function validateImageUrl($attribute, $params)
    {
        $imageExtensions = config::getImageExtensions();
        $imageUrl = $this->image;
        $imageExtension = substr(strrchr($imageUrl, "."), 1);
        if (!strpos($imageExtensions, $imageExtension)) {
            $this->addError('media', 'The url of main picture must be either ".jpg" or ".png"');
        }
    }

    // public function upload()
    // {
    //     if (!file_exists('uploads/contents')) {
    //         mkdir('uploads/contents');
    //     }
    //     // print_r(dirname(getcwd()));
    //     // return false;
    //     // boolean/var/www/html
    //     // ./boolean/var/www/html/uploads/
    //     // if ($this->validate()) {
    //         // substr(str_shuffle(MD5(microtime())), 0, 10);
    //         $this->image->saveAs('uploads/contents/' . $this->title . '.' . $this->image->extension);
    //         return true;
    //     // } else {
    //     //     return false;
    //     // }
    // }

    public function getDateAndUser($insert)
    {
        if ($this->submitType === "save") {
            if ($insert) {
                $this->createdAt = Mongodate::now();
                $this->createdBy = $_SESSION["user"]["email"];
            }
            else {
                $this->updatedAt = Mongodate::now();
                $this->updatedBy = $_SESSION["user"]["email"];
                if (!!$this->approvedAt) {
                    $this->approvedAt = null;
                    $this->approvedBy = null;
                }
            }
        }
        if ($this->submitType === "approve") {
            $this->approvedAt = Mongodate::now();
            $this->approvedBy = $_SESSION["user"]["email"];
        }
    }

    public function uploadManual()
    {
        if (!file_exists('uploads/manuals')) {
            mkdir('uploads/manuals');
        }
        $this->upload->saveAs('uploads/manuals/' . $this->title . '.' . $this->upload->extension);
        return true;
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->online = Services::stingToBoolean($this->online);            
            $this->effectiveDate = Services::getEffectiveDate($this);
            $this->getDateAndUser($insert);
            $this->status = Services::getStatus($this->submitType);
            if ($this->slug == "news") {
                if (!!$this->date) {
                    $date = str_replace('/', '-', $this->date);
                    $date = strtotime($date);
                    $this->date = Mongodate::timestampToMongoDate($date);
                }
                $fields = ["source", "date", "link"];
                array_map(function($field)
                {
                    if (empty($this->$field)) {
                        unset($this->$field);
                    }
                }, $fields);
                // if (!!$this->image) {
                //     $image = $this->image;
                //     $model1 = new Contents();
                //     $model1->image = $image;
                //     $model1->title = GenerateFileName::random();
                //     $this->image = "http://".$_SERVER['HTTP_HOST']."/web/uploads/contents/".$model1->title.".".$model1->image->extension;
                //     $model1->upload();
                //     if ($insert) { // insert
                //         $this->createdAt = Mongodate::now();
                //     }
                //     else { //update
                //         $oldImage = $this->getOldAttributes()["image"];
                //         $oldImagePath = 'uploads/contents/'.strrchr($oldImage, '/');
                //         if (file_exists($oldImagePath))
                //             unlink($oldImagePath);
                //     }
                // }
                // else {
                //     $this->image = $this->getOldAttributes()["image"];
                // }
            }
            else if ($this->slug == "tutorial") {
                if ($insert) { // insert
                    // $this->createdAt = Mongodate::now();
                    $this->views = 0;
                    $this->likes = 0;
                    $this->shares = 0;
                }
            }
            else if ($this->slug == "faq") {
                // $this->status = $this->getStatus();
            }
            else if ($this->slug == "manual") {
                $manual = $this->upload;
                if (!!$manual) {
                    $model1 = new Contents();
                    $model1->upload = $manual;
                    $model1->title = GenerateFileName::random();
                    $this->url = "http://".$_SERVER['HTTP_HOST']."/web/uploads/manuals/".$model1->title.".".$model1->upload->extension;
                    $model1->uploadManual();
                }
                if ($insert) { // insert
                    $this->createdAt = Mongodate::now();
                }
                unset($this->status);
                unset($this->online);
                unset($this->effectiveDate);
                unset($this->approvedAt);
                unset($this->approvedBy);
            }
            unset($this->submitType);
            unset($this->startDate);
            unset($this->endDate);
            unset($this->upload);
            // echo '<pre>';
            // print_r($this);
            // echo '</pre>';
            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'title' => 'Title',
            'slug' => 'Slug',
            'status' => 'Status',
            'image' => 'Image',
            'url' => 'Url',
            'content' => 'Content',
            'ranking' => 'Ranking',
            'views' => 'Views',
            'likes' => 'Likes',
            'shares' => 'Shares',
            'createdAt' => 'Created At',
            'updatedAt' => 'Updated At',
            'approvedAt' => 'Approved At',
            'createdBy' => 'Created By',
            'updatedBy' => 'Updated By',
            'approvedBy' => 'Approved By',
            'effectiveDate' => 'Effective Date',
            'online' => 'Online',
            'startDate' => 'From',
            'endDate' => 'To'
        ];
    }
}
