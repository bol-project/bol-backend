<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\modules\marketing\modules\cms\models\Contents;
use dosamigos\datepicker\DatePicker;
use app\models\MongoDate;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Contents */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="contents-form">


    <?php 
        echo "<strong>Type: </strong>"."How To";
        if (!$model->isNewRecord) {
            echo "<strong> Topic: </strong>".$model->title;
        }
        echo "<br>";
        if (!$model->isNewRecord) {
            echo "<strong>Create Date: </strong>".Mongodate::mongoDateToString($model->createdAt, true);
            $createdBy = $model->createdBy;
        }
        else {
            echo "<strong>Create Date: </strong>".date("d/m/Y");
            $createdBy = $_SESSION["user"]["email"];
        }
        echo "<strong> Create by: </strong>".$createdBy;
        echo "<br>";
        if (!!$model->approvedAt) {
            echo "<strong>Approve Date: </strong>".Mongodate::mongoDateToString($model->approvedAt, true);
            echo "<strong> Approve by: </strong>".$model->approvedBy;
            echo "<br>";
        }
        
    ?>

    <hr style="border-color:#888">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
        if (!$model->isNewRecord && $model->status == 2 && in_array($_SESSION["role"], ["manager","admin"])) {
            echo '<button type="submit" id="approveButton" class="btn btn-success">Approve</button> ';
            echo '<button type="submit" id="rejectButton" class="btn btn-danger">Reject</button>';
        }
    ?>
    
    <?php echo $form->field($model, 'title')->label("Topic") ?>

    <!--<?php echo $form->field($model, 'content')->textInput(['placeholder'=>'e.g. LOxyIHUdwiE'])->label("YouTube ID") ?>-->

    <div class="form-group field-contents-content required">
        <label class="control-label" for="contents-content">YouTube ID</label>
        <div class="input-group">
            <span class="input-group-addon" id="basic-addon3">https://www.youtube.com/watch?v=</span>
            <input type="text" class="form-control" id="contents-content" name="Contents[content]" placeholder="e.g. LOxyIHUdwiE" value="<?=  $model->content?>">
        </div>
        <div class="help-block"></div>
    </div>

    <button type="button" class="btn btn-default" id="previewButton">Preview</button>
    <br><br>
    <iframe style="display:none" src="" id="previewYoutube" frameborder='0' allowfullscreen></iframe>

    <?php 
        if ($model->isNewRecord || $model->online) {
            $active1 = "active";
            $active2 = "";
            $checked1 = "checked";
            $checked2 = "";
        }
        else {
            $active1 = "";
            $active2 = "active";
            $checked1 = "";
            $checked2 = "checked";
        }
        
        echo '<hr style="border-color:#888">';
        echo '<strong>Setting :</strong><br>';
        echo '<strong>Effective</strong><br>';
        echo '
            <span class="field-contents-startdate">
                <label class="control-label" for="contents-startdate">From</label>
                <div style="display:inline-block;width:200px">
                    <div class="input-group date">
                        <div class="input-group-addon" id="addonStartDate">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="contents-startdate" class="form-control" name="Contents[startDate]" style="width:auto">
                    </div>
                </div>
            </span>';
        echo '
            <span class="field-contents-enddate">
                <label class="control-label" for="contents-enddate">To</label>
                <div style="display:inline-block;width:200px">
                    <div class="input-group date">
                        <div class="input-group-addon" id="addonEndDate">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="contents-enddate" class="form-control" name="Contents[endDate]" style="width:auto"">
                    </div>
                </div>
            </span>';
        echo '
            <div class="form-group field-contents-online">
                <label class="control-label" style="display:block">Online Status</label>
                <input type="hidden" name="Contents[online]" value="">
                <div id="contents-online" class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default '.$active1.'" style="color:#000">
                        Yes
                        <input type="radio" name="Contents[online]" value="1" '.$checked1.'>
                    </label>
                    <label class="btn btn-default '.$active2.'" style="color:#000">
                        No
                        <input type="radio" name="Contents[online]" value="0" '.$checked2.'>
                    </label>
                </div>

                <div class="help-block"></div>
            </div>';
    ?>

    <div class="form-group">
        <input type="hidden" id="submitType" name="Contents[submitType]" value="">
        <button type="submit" id="saveButton" class="btn btn-primary">Save</button>
        <?php
            $disabled = !$model->isNewRecord && $model->status == 4 ? '' : 'disabled';
            echo '<button type="submit" id="submitButton" class="btn btn-success" '.$disabled.'>Submit</button>';
        ?>
        <?= Html::a('Cancel', ['/marketing/cms/howto'], ['class'=>'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    $(function () {
        <?php
            $startDate = null;
            $endDate = null;
            if (!!$model->effectiveDate) {
                $startDate = !!$model->effectiveDate['from'] ? Mongodate::mongoDateToString($model->effectiveDate['from'], true) : null;
                $endDate = !!$model->effectiveDate['to'] ? Mongodate::mongoDateToString($model->effectiveDate['to'], true) : null;
            }
            echo '$("#contents-startdate").datepicker({format: "dd/mm/yyyy"}).datepicker("setDate", "'.$startDate.'");';
            echo '$("#contents-enddate").datepicker({format: "dd/mm/yyyy"}).datepicker("setDate", "'.$endDate.'");';
        ?>
        $('#addonStartDate').click(function(){
            $('#contents-startdate').datepicker('show');
        });
        $('#addonEndDate').click(function(){
            $('#contents-enddate').datepicker('show');
        });
        $('#saveButton').click(function(){
            $('#submitType').val("save");
        });
        $('#submitButton').click(function(){
            $('#submitType').val("submit");
        });
        $('#approveButton').click(function(){
            $('#submitType').val("approve");
        });
        $('#rejectButton').click(function(){
            $('#submitType').val("reject");
        });
        $('#previewButton').click(function(){
            var youtube_id = $('#contents-content').val();
            if (youtube_id.trim() !== "") {
                $('#previewYoutube').attr('src', 'https://www.youtube.com/embed/' + youtube_id).css("display","");
            }
            else {
                $('#previewYoutube').css("display","none");
            }
        });
    });
</script>
    