<?php

namespace app\modules\marketing\modules\cms\modules\howto\controllers;

use Yii;
use app\modules\marketing\modules\cms\models\Contents;
use app\modules\marketing\modules\cms\models\ContentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\businesses\models\Businesses;
use app\controllers\AdminController;
/**
 * DefaultController implements the CRUD actions for Contents model.
 */
class DefaultController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contents models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "tutorial");
        return $this->render('..\..\..\views\index.php', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'How to'
        ]);
    }

    /**
     * Displays a single Contents model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        // $business = $this->findBusinessesModel($model->children);
        // print_r($model);
        // echo "<br/><br/><br/><br/>";
        if ($model->slug == "explore") {
            $name = $this->getBusinessName($this->findBusinessesModel($model->children));
            $model->children = $name;
        }
        // print_r($name);
        return $this->render('..\..\..\views\view.php', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Contents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contents();
        if ($model->load(Yii::$app->request->post())) {
            $model->slug = "tutorial";
            $submitType = $model->submitType;
            $validateSubmit = true;
            if ($submitType === "submit" && $model->status !== 4) {
                $validateSubmit = false;
            }
            if ($validateSubmit && $model->validate() && $model->save()) {
                return $this->redirect(['update', 'id' => (string)$model->_id]);
            }
            else {
                $errors = $model->errors;
                echo '<pre>';
                print_r($errors);
                echo '</pre>';
                echo '<pre>';
                print_r($model);
                echo '</pre>';
            }
        }
        else {
            return $this->render('..\..\..\views\create.php', [
                'model' => $model,
                'slug' => "tutorial"
            ]);
        }
    }

    /**
     * Updates an existing Contents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else {
                $errors = $model->errors;
                echo '<pre>';
                print_r($errors);
                echo '</pre>';
                echo '<pre>';
                print_r($model);
                echo '</pre>';
            }
        } else {
            return $this->render('..\..\..\views\update.php', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing Contents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        //return $this->redirect(['/marketing/cms/Howto']);
    }

    /**
     * Finds the Contents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Contents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}