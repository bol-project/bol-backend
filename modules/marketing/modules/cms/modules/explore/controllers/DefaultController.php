<?php

namespace app\modules\marketing\modules\cms\modules\explore\controllers;

use Yii;
use app\modules\marketing\modules\cms\models\Contents;
use app\modules\marketing\modules\cms\models\ContentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\modules\businesses\models\Businesses;
use app\controllers\AdminController;
/**
 * DefaultController implements the CRUD actions for Contents model.
 */
class DefaultController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contents models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "explore");

        
        // $dataProvider->models = array_filter(
        //     array_map(
        //         function($model) {
        //             if ($model->attributes["slug"] === "explore") {
        //                 return $model;
        //             }
        //         }, 
        //         $dataProvider->models
        //     )
        // );
        return $this->render('..\..\..\views\index.php', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Explore'
        ]);
    }

    /**
     * Displays a single Contents model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        // $business = $this->findBusinessesModel($model->ranking);
        // print_r($model);
        // echo "<br/><br/><br/><br/>";
        if ($model->slug == "explore") {
            $name = $this->getBusinessName($this->findBusinessesModel($model->ranking));
            $model->ranking = $name;
        }
        // print_r($name);
        return $this->render('..\..\..\views\view.php', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Contents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contents();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Contents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => (string)$model->_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Contents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Contents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Contents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function findBusinessesModel($ids)
    {
        if (($businesses = Businesses::find()->where(['in', '_id', $ids])->all()) !== null) {
            return $businesses;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function getBusinessName($businesses)
    {
        $name = [];
        foreach ($businesses as $business) {
            array_push($name, [$business->_id, $business->name]);
        }
        return $name;
        // if ($name = $business->name !== null) {
        //     return $name;
        // } else {
        //     throw new NotFoundHttpException('The requested page does not exist.');
        // }
    }
}