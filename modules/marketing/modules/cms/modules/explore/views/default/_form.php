<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Contents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contents-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="box box-danger">

    <div class="box-body">

    <?= $form->field($model, 'slug')->dropDownList([1 => 'news', 2 => 'tutorial'],['prompt'=>'select slug']) ?>
    
    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'image') ?>

    <?= $form->field($model, 'content') ?>

    <?= $form->field($model, 'ranking[0]') ?>

    <?= $form->field($model, 'views') ?>

    <?= $form->field($model, 'likes') ?>

    <?= $form->field($model, 'shares') ?>

    <?= $form->field($model, 'createdAt') ?>

    <?= $form->field($model, 'updatedAt') ?>
    </div>

    <div class="box-footer">

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
         <?= Html::a('Cancel', ['/marketing/cms/explore'], ['class'=>'btn btn-danger']) ?>
 
    </div>
    
    </div>

    </div>
    <?php ActiveForm::end(); ?>

</div>
<script src="/web/assets/334062a/jquery.js"></script>
<script type="text/javascript">
    // alert($('#contents-slug').val());
    $('#contents-slug').on('change',function(){
        alert($(this).val());
    });
</script>