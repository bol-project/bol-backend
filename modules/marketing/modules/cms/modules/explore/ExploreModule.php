<?php

namespace app\modules\marketing\modules\cms\modules\explore;

/**
 * explore module definition class
 */
class ExploreModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\marketing\modules\cms\modules\explore\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
