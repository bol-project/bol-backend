<?php

namespace app\modules\marketing\modules\cms\modules\support\controllers;

use Yii;
use app\modules\marketing\modules\cms\models\Contents;
use app\modules\marketing\modules\cms\models\ContentsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\controllers\AdminController;
/**
 * DefaultController implements the CRUD actions for Contents model.
 */
class DefaultController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Contents models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ContentsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, ["faq", "manual"]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Support'
        ]);
    }

    /**
     * Displays a single Contents model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Contents model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Contents();

        $slug = Yii::$app->getRequest()->getQueryParam('slug');
        switch ($slug) {
            case "faq":
                // $model->setScenario('createFaq');
                break;
            case "manual":
                $model->setScenario('createManual');
                break;
            default:
                # code...
                break;
        }
        if ($model->load(Yii::$app->request->post())) {
            $model->slug = $slug;
            // echo '<pre>';
            // print_r($model);
            // echo '</pre>';
            $model->upload = UploadedFile::getInstance($model, 'upload');
            if ($model->validate()) {
                $submitType = $model->submitType;
                $validateSubmit = true;
                if ($submitType === "submit" && $model->status !== 4) {
                    $validateSubmit = false;
                }
                if ($validateSubmit && $model->save()) {
                    if ($slug === "faq") {
                        return $this->redirect(['update', 'id' => (string)$model->_id]);
                    }
                    else {
                        return $this->redirect(['view', 'id' => (string)$model->_id]);
                    }
                    // if ($slug === "faq" && $submitType === "save") {
                    //     $model->isNewRecord = false;
                    //     return $this->render('create', [
                    //         'model' => $model,
                    //         'slug' => $slug
                    //     ]);
                    // }
                    // else {
                    //     return $this->redirect(['view', 'id' => (string)$model->_id]);
                    // }
                }
                //     return $this->redirect(['view', 'id' => (string)$model->_id]);
                else {
                    $errors = "submit error";
                    echo '<pre>';
                    print_r($errors);
                    echo '</pre>';
                }
            } else {
                $errors = $model->errors;
                echo '<pre>';
                print_r($errors);
                echo '</pre>';
                echo '<pre>';
                print_r($model);
                echo '</pre>';
            }
        } else {
            return $this->render('create', [
                'model' => $model,
                'slug' => $slug
            ]);
        }
    }

    /**
     * Updates an existing Contents model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $model->upload = UploadedFile::getInstance($model, 'upload');
                if ($model->save())
                    return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else {
                $errors = $model->errors;
                echo '<pre>';
                print_r($errors);
                echo '</pre>';
                echo '<pre>';
                print_r($model);
                echo '</pre>';
            }
        } else {
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    /**
     * Deletes an existing Contents model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/marketing/cms/support']);
    }

    /**
     * Finds the Contents model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Contents the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Contents::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
