<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\MongoDate;
use app\modules\marketing\config\config;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\modules\cms\models\Contents */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'CMS'];
$this->params['breadcrumbs'][] = ['label' => 'Support', 'url' => ['/marketing/cms/support']];
$this->params['breadcrumbs'][] = $model->slug;

function getAttributes($model) {
    $attributes = [
        'title',
        'slug'
    ];
    if ($model->slug === 'faq') {
        if (!!$model->status) {
            $status = Config::getStatus();
            array_push($attributes, 
                [
                    'attribute' => 'status',
                    'format'=>'raw',
                    'value'=>is_int($model->status) ? ucfirst($status[$model->status]) : null
                ]
            );
        }
        array_push($attributes, 
            [
                'attribute' => 'online',
                'format'=>'raw',
                'value'=>is_bool($model->online) ? $model->online ? 
                                "<span style='color:green'>Yes</span>"
                                : 
                                "<span style='color:red'>No</span>"
                        :
                        null
            ],
            [
                'attribute' => 'Effective from',
                'format'=>'raw',
                'value'=>Mongodate::mongoDateToString($model->effectiveDate['from'], true)
            ],
            [
                'attribute' => 'Effective to',
                'format'=>'raw',
                'value'=>Mongodate::mongoDateToString($model->effectiveDate['to'], true)
            ]
        );
    }
    if ($model->slug === 'manual') {
        array_push($attributes, 'url');
    }
    array_push($attributes, 
        [
            'attribute' => 'content',
            'format'=>'raw',
            'value'=>$model->content
        ]
    );
    if (!!$model->createdAt) {
        array_push($attributes, 
            [
                'attribute' => 'createdAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->createdAt)
            ],
            'createdBy'
        );
    }
    if (!!$model->updatedAt) {
        array_push($attributes, 
            [
                'attribute' => 'updatedAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->updatedAt)
            ],
            'updatedBy'
        );
    }
    if (!!$model->approvedAt) {
        array_push($attributes, 
            [
                'attribute' => 'approvedAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->approvedAt)
            ],
            'approvedBy'
        );
    }
    return $attributes;
}
?>
<div class="contents-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="box box-danger">

    <div class="box-header with-border">
    
    <p>
        <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    </div>
    
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => getAttributes($model)
    ]) ?>

    </div>

    </div>

    </div>

</div>
