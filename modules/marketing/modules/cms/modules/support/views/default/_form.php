<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use app\models\MongoDate;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\modules\cms\models\Contents */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="contents-form">

    <?php 
        echo "<strong>Type: </strong>".$slug;
        if (!$model->isNewRecord) {
            echo "<strong> Topic: </strong>".$model->title;
        }
        echo "<br>";
        if (!$model->isNewRecord) {
            echo "<strong>Create Date: </strong>".Mongodate::mongoDateToString($model->createdAt, true);
            $createdBy = $model->createdBy;
        }
        else {
            echo "<strong>Create Date: </strong>".date("d/m/Y");
            $createdBy = $_SESSION["user"]["email"];
        }
        echo "<strong> Create by: </strong>".$createdBy;
        echo "<br>";
        if (!!$model->approvedAt) {
            echo "<strong>Approve Date: </strong>".Mongodate::mongoDateToString($model->approvedAt, true);
            echo "<strong> Approve by: </strong>".$model->approvedBy;
            echo "<br>";
        }
        
    ?>

    <hr style="border-color:#888">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php 
        if (!$model->isNewRecord && $model->status == 2 && in_array($_SESSION["role"], ["manager","admin"])) {
            echo '<button type="submit" id="approveButton" class="btn btn-success">Approve</button> ';
            echo '<button type="submit" id="rejectButton" class="btn btn-danger">Reject</button>';
        }
    ?>

    <?= $form->field($model, 'title')->textInput(['placeholder' => 'Enter Your Topic'])->label("Topic") ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'full',
        
    ]) ?>

    <?php 
        if ($model->slug == "manual" || $slug == "manual") {
            echo $form->field($model, 'upload')->fileInput()->label("File");
        }
    ?>

    <!--<?= $form->field($model, 'online')->radioList([true => "Yes", false => "No"], [
        "item"=>function($index, $label, $name, $checked, $value){
            $active = $checked ? 'active' : '';
            $checked = $checked ? 'checked' : '';
            // $color = $index==0?'#00a65a':'#dd4b39';
            // return '<label class="btn btn-default '.$active.'" style="color:'.$color.'">'.$label.'<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'></label>';
            // $labelClass = $index==0?'success':'danger';
            // return '<label class="btn btn-'.$labelClass.' '.$active.'" style="color:#fff">'.$label.'<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'></label>';
            return '<label class="btn btn-default '.$active.'" style="color:#000">'.$label.'<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'></label>';
        },"class"=>"btn-group","data-toggle"=>"buttons"])->label('Online Status', ["style"=>"display:block"]) ?>-->
    <?php 
        if ($model->slug === "faq"  || $slug === "faq"){
            if ($model->isNewRecord || $model->online) {
                $active1 = "active";
                $active2 = "";
                $checked1 = "checked";
                $checked2 = "";
            }
            else {
                $active1 = "";
                $active2 = "active";
                $checked1 = "";
                $checked2 = "checked";
            }
            
            echo '<hr style="border-color:#888">';
            echo '<strong>Setting :</strong><br>';
            echo '<strong>Effective</strong><br>';
            echo '
                <span class="field-contents-startdate">
                    <label class="control-label" for="contents-startdate">From</label>
                    <div style="display:inline-block;width:200px">
                        <div class="input-group date">
                            <div class="input-group-addon" id="addonStartDate">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" id="contents-startdate" class="form-control" name="Contents[startDate]" style="width:auto">
                        </div>
                    </div>
                </span>';
            echo '
                <span class="field-contents-enddate">
                    <label class="control-label" for="contents-enddate">To</label>
                    <div style="display:inline-block;width:200px">
                        <div class="input-group date">
                            <div class="input-group-addon" id="addonEndDate">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" id="contents-enddate" class="form-control" name="Contents[endDate]" style="width:auto"">
                        </div>
                    </div>
                </span>';
            // echo $form->field($model, 'startDate')->textInput(['class'=>'form-control datepicker','style'=>'position: relative; z-index: 3;']);
            // echo $form->field($model, 'endDate')->textInput(['class'=>'form-control datepicker','style'=>'position: relative; z-index: 3;']);
            echo '
                <div class="form-group field-contents-online">
                    <label class="control-label" style="display:block">Online Status</label>
                    <input type="hidden" name="Contents[online]" value="">
                    <div id="contents-online" class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default '.$active1.'" style="color:#000">
                            Yes
                            <input type="radio" name="Contents[online]" value="1" '.$checked1.'>
                        </label>
                        <label class="btn btn-default '.$active2.'" style="color:#000">
                            No
                            <input type="radio" name="Contents[online]" value="0" '.$checked2.'>
                        </label>
                    </div>

                    <div class="help-block"></div>
                </div>';
        }
    ?>
    <div class="form-group">
        <input type="hidden" id="submitType" name="Contents[submitType]" value="">
        <?php /* Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) */?>
        <button type="submit" id="saveButton" class="btn btn-primary">Save</button>
        <?php
            if ($model->slug === "faq"  || $slug === "faq") {
                $disabled = !$model->isNewRecord && $model->status == 4 ? '' : 'disabled';
                echo '<button type="submit" id="submitButton" class="btn btn-success" '.$disabled.'>Submit</button>';
            }
        ?>
        <?= Html::a('Cancel', ['/marketing/cms/support'], ['class'=>'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    $(function () {
        <?php
            $startDate = null;
            $endDate = null;
            if (!!$model->effectiveDate) {
                $startDate = !!$model->effectiveDate['from'] ? Mongodate::mongoDateToString($model->effectiveDate['from'], true) : null;
                $endDate = !!$model->effectiveDate['to'] ? Mongodate::mongoDateToString($model->effectiveDate['to'], true) : null;
            }
            echo '$("#contents-startdate").datepicker({format: "dd/mm/yyyy"}).datepicker("setDate", "'.$startDate.'");';
            echo '$("#contents-enddate").datepicker({format: "dd/mm/yyyy"}).datepicker("setDate", "'.$endDate.'");';
        ?>
        $('#addonStartDate').click(function(){
            $('#contents-startdate').datepicker('show');
        });
        $('#addonEndDate').click(function(){
            $('#contents-enddate').datepicker('show');
        });
        $('#saveButton').click(function(){
            $('#submitType').val("save");
        });
        $('#submitButton').click(function(){
            $('#submitType').val("submit");
        });
        $('#approveButton').click(function(){
            $('#submitType').val("approve");
        });
        $('#rejectButton').click(function(){
            $('#submitType').val("reject");
        });
    });
</script>