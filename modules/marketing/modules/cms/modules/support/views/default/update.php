<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\modules\cms\models\Contents */

$this->title = 'Update '.$model->slug.': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'CMS'];
$this->params['breadcrumbs'][] = ['label' => 'Support', 'url' => ['/marketing/cms/support']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contents-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
                'slug' => $model->slug
            ]) ?>
        </div>
    </div>

</div>
