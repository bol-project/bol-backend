<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\MongoDate;
use app\modules\marketing\config\config;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\marketing\modules\cms\models\ContentsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
$this->params['breadcrumbs'] = ["Marketing", "CMS", $this->title];
?>
<div class="contents-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    
    <div class="box box-danger">

    <div class="box-header with-border">

    <?= '<div class="dropdown">
        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Create Support
            <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
            <li><a href="javascript:location+=\'/default/create&slug=faq\'">Faq</a></li>
            <li><a href="javascript:location+=\'/default/create&slug=manual\'">Manual</a></li>
        </ul>
    </div>' ?>

    </div>

    <div class="box-body">
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => ["style"=>"text-align:center"],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'title',
                'label' => 'Topic'
            ],
            [
                'attribute' => 'createdAt',
                'label' => 'Create Date',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return Mongodate::mongoDateToString($model->createdAt, true);
                }
            ],
            [
                'attribute' => 'approvedAt',
                'label' => 'Approve Date',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return Mongodate::mongoDateToString($model->approvedAt, true);
                }
            ],
            [
                'attribute' => 'approvedBy',
                'label' => 'Approve by'
            ],
            [
                'label' => 'Effective From',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return gettype($model->effectiveDate) == "array" ?
                        Mongodate::mongoDateToString($model->effectiveDate['from'], true)
                        :
                        null;
                }
            ],
            [
                'label' => 'Effective To',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return gettype($model->effectiveDate) == "array" ?
                        Mongodate::mongoDateToString($model->effectiveDate['to'], true)
                        :
                        null;
                }
            ],
            [
                'attribute' => 'slug',
                'options'=>["style"=>"width:80px"],
            ],
            [
                'attribute' => 'status',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    // return $model->status ? 
                    //         "<span style='color:green'>enable</span>"
                    //         : 
                    //         "<span style='color:red'>disable</span>";
                    if (!!$model->status && is_int($model->status)) {
                        $status = Config::getStatus();
                        return ucfirst($status[$model->status]);
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'online',
                'format'=>'html',
                'options'=>["style"=>"width:50px"],
                'value'=>function($model, $key, $index, $column){
                    if (is_bool($model->online)) {
                        return $model->online ? 
                                "<span style='color:green'>Yes</span>"
                                : 
                                "<span style='color:red'>No</span>";
                    }
                    return null;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    </div>
    
    </div>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.content-header h1').css('font-size','36px');
    });
</script>