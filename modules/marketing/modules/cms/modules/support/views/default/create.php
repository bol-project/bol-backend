<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\marketing\modules\cms\models\Contents */

$this->title = 'Create '.$slug;
$this->params['breadcrumbs'][] = ['label' => 'Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contents-create">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="panel panel-default">
        <div class="panel-body">
            <?= $this->render('_form', [
                'model' => $model,
                'slug' => $slug
            ]) ?>
        </div>
    </div>
</div>
