<?php

namespace app\modules\marketing\modules\cms\modules\support;

/**
 * support module definition class
 */
class SupportModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\marketing\modules\cms\modules\support\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
