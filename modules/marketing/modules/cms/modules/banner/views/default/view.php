<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\MongoDate;
use app\modules\marketing\config\config;
/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Banners */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'CMS'];
$this->params['breadcrumbs'][] = ['label' => 'Banner', 'url' => ['/marketing/cms/banner']];

function getAttributes($model) {
    $status = config::getStatus();
    $type = ["Introduction", "Video"];
    switch ($model->type_id) {
        case 0:{
            $media = "<img style='width:250px;height:auto;' src='".$model->media."'/>";
            break;
        }
        case 1:{
            $media = 
                "<video width='320' height='240' controls>
                    <source src='".$model->media."'>
                    Your browser does not support the video tag.
                </video>";
            break;
        }
        default:
            break;
    }
    $attributes = [
        [
            'attribute' => 'title',
            'label' => 'Topic'
        ],
        [
            'attribute' => 'type',
            'format'=>'html',
            'value'=>$type[$model->type_id]
        ],
        "link",
        "content",
        [
            'label' => 'Banner',
            'format'=>'raw',
            'value'=>$media
        ]
    ];
    
    if (gettype($model->status) === "integer") {
        array_push($attributes, 
            [
                'attribute' => 'status',
                'format'=>'raw',
                'value'=>ucfirst($status[$model->status])
            ]
        );
    }
    array_push($attributes, 
            [
                'attribute' => 'type',
                'format'=>'html',
                'value'=>$type[$model->type_id]
            ],
            "link",
            "content"
        );
    if (!!$model->createdAt) {
        array_push($attributes, 
            [
                'attribute' => 'createdAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->createdAt)
            ],
            'createdBy'
        );
    }
    if (!!$model->updatedAt) {
        array_push($attributes, 
            [
                'attribute' => 'updatedAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->updatedAt)
            ],
            'updatedBy'
        );
    }
    if (!!$model->approvedAt) {
        array_push($attributes, 
            [
                'attribute' => 'approvedAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->approvedAt)
            ],
            'approvedBy'
        );
    }
    // switch ($model->type_id) {
    //     case 0:{
    //         $data[count($data)-3]['value'] = "<img style='width:250px;height:auto;' src='".$model->media["url"]."'/>";
    //         break;
    //     }
    //     case 1:{
    //         $data[count($data)-3]['value'] = 
    //             "<video width='320' height='240' controls>
    //                 <source src='".$model->media["url"]."' type='".$model->media["mimetype"]."'>
    //                 Your browser does not support the video tag.
    //             </video>";
    //         break;
    //     }
    //     default:
    //         break;
    // }
    return $attributes;
}
?>
<div class="banners-view">

    <h1> <?= Html::encode($this->title) ?> </h1>
    
    <div class="box box-danger">
    
    <div class="box-header with-border">
    <p>
        <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    </div>
    
    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => getAttributes($model)
    ]) ?>
    
    </div>
</div>
