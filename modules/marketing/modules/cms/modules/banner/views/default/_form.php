<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\MongoDate;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Banners */
/* @var $form yii\widgets\ActiveForm */
$type = ["Introduction", "Video"];
?>

<div class="banners-form">

    <?php 
        echo "<strong>Type: </strong>".$type[$model->type_id];
        if (!$model->isNewRecord) {
            echo "<strong> Topic: </strong>".$title;
        }
        echo "<br>";
        if (!$model->isNewRecord) {
            echo "<strong>Create Date: </strong>".Mongodate::mongoDateToString($model->createdAt, true);
            $createdBy = $model->createdBy;
        }
        else {
            echo "<strong>Create Date: </strong>".date("d/m/Y");
            $createdBy = $_SESSION["user"]["email"];
        }
        echo "<strong> Create by: </strong>".$createdBy;
        echo "<br>";
        if (!!$model->approvedAt) {
            echo "<strong>Approve Date: </strong>".Mongodate::mongoDateToString($model->approvedAt, true);
            echo "<strong> Approve by: </strong>".$model->approvedBy;
            echo "<br>";
        }
        
    ?>
    <hr style="border-color:#888">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
    
    <?php 
        if (!$model->isNewRecord && $model->status == 2 && in_array($_SESSION["role"], ["manager","admin"])) {
            echo '<button type="submit" id="approveButton" class="btn btn-success">Approve</button> ';
            echo '<button type="submit" id="rejectButton" class="btn btn-danger">Reject</button>';
        }
    ?>
    
    <?= $form->field($model, 'title')->label("Topic") ?>            
    
    <?= $form->field($model, 'content') ?>

    <?php
        $placeholder = 'e.g. http://192.168.99.100:5000/web/uploads/gallery/';
        $placeholder .= $model->type_id === 0 ? "intro_1.jpg" : "testvideo1.mp4";
        echo $form->field($model, 'media')->textInput(['placeholder'=>$placeholder])->label("Banner")
    ?>

    <?= $form->field($model, 'link')->textInput(["placeholder"=>"e.g. http://www.matichon.co.th/news/438719"])->label("URL") ?>

    <?php 
        if ($model->isNewRecord || $model->online) {
            $active1 = "active";
            $active2 = "";
            $checked1 = "checked";
            $checked2 = "";
        }
        else {
            $active1 = "";
            $active2 = "active";
            $checked1 = "";
            $checked2 = "checked";
        }
        
        echo '<hr style="border-color:#888">';
        echo '<strong>Setting :</strong><br>';
        echo '<strong>Effective</strong><br>';
        echo '
            <span class="field-banners-startdate">
                <label class="control-label" for="banners-startdate">From</label>
                <div style="display:inline-block;width:30%">
                    <div class="input-group date">
                        <div class="input-group-addon" id="addonStartDate">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="banners-startdate" class="form-control" name="Banners[startDate]">
                    </div>
                </div>
            </span>';
        echo '
            <span class="field-banners-enddate">
                <label class="control-label" for="banners-enddate">To</label>
                <div style="display:inline-block;width:30%">
                    <div class="input-group date">
                        <div class="input-group-addon" id="addonEndDate">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input type="text" id="banners-enddate" class="form-control" name="Banners[endDate]">
                    </div>
                </div>
            </span>';
        echo '
            <div class="form-group field-banners-online">
                <label class="control-label" style="display:block">Online Status</label>
                <input type="hidden" name="Banners[online]" value="">
                <div id="banners-online" class="btn-group" data-toggle="buttons">
                    <label class="btn btn-default '.$active1.'" style="color:#000">
                        Yes
                        <input type="radio" name="Banners[online]" value="1" '.$checked1.'>
                    </label>
                    <label class="btn btn-default '.$active2.'" style="color:#000">
                        No
                        <input type="radio" name="Banners[online]" value="0" '.$checked2.'>
                    </label>
                </div>

                <div class="help-block"></div>
            </div>';
    ?>

    <div class="form-group">
        <input type="hidden" id="submitType" name="Banners[submitType]" value="">
        <button type="submit" id="saveButton" class="btn btn-primary">Save</button>
        <?php
            $disabled = !$model->isNewRecord && $model->status == 4 ? '' : 'disabled';
            echo '<button type="submit" id="submitButton" class="btn btn-success" '.$disabled.'>Submit</button>';
        ?>
        <?= Html::a('Cancel', ['/marketing/cms/banner'], ['class'=>'btn btn-danger']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<script type="text/javascript">
    $(function () {
        <?php
            $startDate = null;
            $endDate = null;
            if (!!$model->effectiveDate) {
                $startDate = !!$model->effectiveDate['from'] ? Mongodate::mongoDateToString($model->effectiveDate['from'], true) : null;
                $endDate = !!$model->effectiveDate['to'] ? Mongodate::mongoDateToString($model->effectiveDate['to'], true) : null;
            }
            echo '$("#banners-startdate").datepicker({format: "dd/mm/yyyy"}).datepicker("setDate", "'.$startDate.'");';
            echo '$("#banners-enddate").datepicker({format: "dd/mm/yyyy"}).datepicker("setDate", "'.$endDate.'");';
        ?>
        $('#addonStartDate').click(function(){
            $('#banners-startdate').datepicker('show');
        });
        $('#addonEndDate').click(function(){
            $('#banners-enddate').datepicker('show');
        });
        $('#saveButton').click(function(){
            $('#submitType').val("save");
        });
        $('#submitButton').click(function(){
            $('#submitType').val("submit");
        });
        $('#approveButton').click(function(){
            $('#submitType').val("approve");
        });
        $('#rejectButton').click(function(){
            $('#submitType').val("reject");
        });
    });
</script>