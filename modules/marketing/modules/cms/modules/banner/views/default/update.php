<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Banners */
$title = $model->title;
if (strlen($title) > 15) {
    $title = substr($model->title, 0, 15).'...';
}
$this->title = 'Update Banner: ' . $title;
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'CMS'];
$this->params['breadcrumbs'][] = ['label' => 'Banner', 'url' => ['/marketing/cms/banner']];
$this->params['breadcrumbs'][] = ['label' => $title, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contents-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-lg-6">
                <?= $this->render('_form', [
                    'model' => $model,
                    'title' => $title
                ]) ?>
            </div>
            <div class="col-lg-6" style="border-left:1px dashed #888">
                <?= $this->render('..\..\..\views\preview.php', [
                    'model' => $model,
                    'hide_header' => $model->type_id === 0
                ]) ?>
            </div>            
        </div>
    </div>

</div>
