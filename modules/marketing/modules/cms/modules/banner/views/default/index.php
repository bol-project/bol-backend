<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\MongoDate;
use app\modules\marketing\config\config;
use app\modules\marketing\config\services;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\marketing\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
// $this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'] = ["Marketing", "CMS", $this->title];
?>
<div class="contents-index">
    <section>
        <h1 style="margin:0 0 10px 0;"><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </section>

    <div class="box box-danger">
    
    <div class="box-header with-border">
    <p>
        <?php 
            if ($title == "Banner")
                echo '<div class="dropdown">
                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Create Banner
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="javascript:location+=\'/default/create&type_id=0\'">Introduction</a></li>
                            <li><a href="javascript:location+=\'/default/create&type_id=1\'">Video</a></li>
                        </ul>
                    </div>';
            else
                echo Html::a('Create '.$title, ['create'], ['class' => 'btn btn-success']);
        ?>
    </p>
    </div>

    <div class="box-body">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'pager' => [
                        'firstPageLabel'=>'<<',   
                        'prevPageLabel' =>'<', 
                        'nextPageLabel' =>'>',   
                        'lastPageLabel' =>'>>'
            ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'title',
                'label' => 'Topic'
            ],
            [
                'attribute' => 'createdAt',
                'label' => 'Create Date',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return Mongodate::mongoDateToString($model->createdAt, true);
                }
            ],
            [
                'attribute' => 'approvedAt',
                'label' => 'Approve Date',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return Mongodate::mongoDateToString($model->approvedAt, true);
                }
            ],
            [
                'attribute' => 'approvedBy',
                'label' => 'Approve by'
            ],
            [
                'label' => 'Effective From',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return gettype($model->effectiveDate) === "array" ?
                        Mongodate::mongoDateToString($model->effectiveDate['from'], true)
                        :
                        null;
                }
            ],
            [
                'label' => 'Effective To',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return gettype($model->effectiveDate) === "array" ?
                        Mongodate::mongoDateToString($model->effectiveDate['to'], true)
                        :
                        null;
                }
            ],
            [
                'attribute' => 'type_id',
                'label' => 'Type',
                'value'=>function($model){
                    $type = ["Introduction", "Video"];
                    return $type[$model->type_id];
                }
            ],
            [
                'attribute' => 'status',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    if (!!$model->status && is_int($model->status)) {
                        $status = config::getStatus();
                        return ucfirst($status[$model->status]);
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'online',
                'format'=>'html',
                'options'=>["style"=>"width:50px"],
                'value'=>function($model, $key, $index, $column){
                    // if (is_bool($model->online)) {
                    //     return $model->online ? 
                    //             "<span style='color:green'>Yes</span>"
                    //             : 
                    //             "<span style='color:red'>No</span>";
                    // }
                    if (is_array($model->effectiveDate) && is_int($model->status) && is_bool($model->online)) {
                        $startDate = Mongodate::mongoDateToString($model->effectiveDate['from'], true);
                        $endDate = Mongodate::mongoDateToString($model->effectiveDate['to'], true);
                        // Services::isDateInRange($startDate, $endDate);
                        // echo "<script>";
                        // echo "console.warn('end: ".$endDate."');";
                        // echo "</script>";
                        if ($model->status === 1 && Services::isDateInRange($startDate, $endDate) && $model->online) {
                            return "<span style='color:green'>Yes</span>";
                        }
                        else {
                            return "<span style='color:red'>No</span>";
                        }
                    }
                    else {
                        return null;
                    }
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ]
    ]); ?>
    </div>
    </div>
</div>
<script type="text/javascript">
    $('.content-header h1').css('font-size','36px');
</script>