<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Banner */

$this->title = 'Create Banner';
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'CMS'];
$this->params['breadcrumbs'][] = ['label' => 'Banner', 'url' => ['/marketing/cms/banner']];
$this->params['breadcrumbs'][] = 'Create';
// echo "<pre>";
//     print_r($model);
// echo "</pre>";
?>
<div class="banners-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="panel panel-default">
        <div class="panel-body">
            <div class="col-lg-6">
                <?= $this->render('_form', [
                    'model' => $model
                ]) ?>
            </div>
            <div class="col-lg-6" style="border-left:1px dashed #888">
                <?= $this->render('..\..\..\views\preview.php', [
                    'model' => $model,
                    'hide_header' => $model->type_id === 0
                ]) ?>
            </div>
        </div>
    </div>

</div>
