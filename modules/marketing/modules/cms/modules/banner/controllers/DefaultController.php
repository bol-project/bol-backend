<?php

namespace app\modules\marketing\modules\cms\modules\banner\controllers;

use Yii;
use app\modules\marketing\modules\cms\models\Banners;
use app\modules\marketing\modules\cms\models\BannersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\controllers\AdminController;
/**
 * DefaultController implements the CRUD actions for Banners model.
 */
class DefaultController extends AdminController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Banners models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new BannersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => 'Banner'
        ]);
    }

    /**
     * Displays a single Banners model.
     * @param integer $_id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view', [
            'model' => $model
        ]);
    }

    /**
     * Creates a new Banners model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Banners();
        $type_id = (int)Yii::$app->getRequest()->getQueryParam('type_id');
        switch ($type_id) {
            case 0:
                $model->setScenario('introduction');
                break;
            case 1:
                $model->setScenario('video');
                break;
            default:
                # code...
                break;
        }
        $model->type_id = $type_id;
        if ($model->load(Yii::$app->request->post())) {
            $submitType = $model->submitType;
            $validateSubmit = true;
            if ($submitType === "submit" && $model->status !== 4) {
                $validateSubmit = false;
            }
            if ($validateSubmit && $model->validate()) {
                // $model->upload = UploadedFile::getInstance($model, 'upload');
                if ($model->save()) {
                    return $this->redirect(['update', 'id' => (string)$model->_id]);
                    // return $this->redirect(['view', 'id' => (string)$model->_id]);
                }
            } else {
                $errors = $model->errors;
                // debug
                // echo '<pre>';
                // print_r($errors);
                // echo '</pre>';
                // echo '<pre>';
                // print_r($model);
                // echo '</pre>';
            }
        }
        // else {
        //     return $this->render('create', [
        //         'model' => $model
        //     ]);
        // }
        return $this->render('create', [
            'model' => $model
        ]);
    }

    /**
     * Updates an existing Banners model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        switch ($model->type_id) {
            case 0:
                $model->setScenario('introduction');
                break;
            case 1:
                $model->setScenario('video');
                break;
            default:
                # code...
                break;
        }
        if (is_array($model->media)) {
            $model->media = "";
        }
        
        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate() && $model->save()) {
                return $this->redirect(['view', 'id' => (string)$model->_id]);
            } else {
                $errors = $model->errors;
                // debug
                // echo '<pre>';
                // print_r($errors);
                // echo '</pre>';
                // echo '<pre>';
                // print_r($model);
                // echo '</pre>';
            }
        }
        // else {
        //     return $this->render('update', [
        //         'model' => $model
        //     ]);
        // }
        return $this->render('update', [
            'model' => $model
        ]);
    }

    /**
     * Deletes an existing Banners model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $_id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['/marketing/cms/banner']);
    }

    /**
     * Finds the Banners model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $_id
     * @return Banners the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Banners::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}