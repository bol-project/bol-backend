<?php

namespace app\modules\marketing\modules\cms\modules\banner;

/**
 * banner module definition class
 */
class BannerModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\marketing\modules\cms\modules\banner\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
