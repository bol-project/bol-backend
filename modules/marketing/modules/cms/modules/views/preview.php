<?php
    // ["hide", "tabIcon", "intro"]
    $bottom_panel_height_ref = ["0%", "7.25%", "9.5%"];
    // ["hide", "show"]
    $header_panel_height_ref = ["0%", "7.35%"];
    function getBottomPanelType($model){
        if(isset($hide_bottom)) {
            return 0;
        }
        else {
            if(!isset($model->slug)) {
                if($model->type_id === 0) {
                    return 2;
                }
                else {
                    return 1;
                }
            }
        }
        return 1;
    }

    function getHeaderPanelType($model, $hide_header){
        if($hide_header) {
            return 0;
        }
        return 1;
    }

    $header_panel_type = getHeaderPanelType($model, $hide_header);
    $header_panel_height = $header_panel_height_ref[$header_panel_type];

    $bottom_panel_type = getBottomPanelType($model);
    $bottom_panel_height = $bottom_panel_height_ref[$bottom_panel_type];
?>
<div id="preview-warpper">
    <div id="status-bar">
        <div style="align-items: center;display: flex;">
            <i class="fa fa-circle" aria-hidden="true"></i>
            <i class="fa fa-circle" aria-hidden="true"></i>
            <i class="fa fa-circle" aria-hidden="true"></i>
            <i class="fa fa-circle" aria-hidden="true"></i>
            <i class="fa fa-circle" aria-hidden="true"></i>
            <span style="margin-left: 2px;">AIS</span>
        </div>
        <span>
            <?= date('h:i A'); ?>
        </span>
        <div>
            100%
            <i class="fa fa-battery-full" aria-hidden="true"></i>
        </div>
    </div>
    <div id="header-panel">
        <?php
            if(isset($model->slug) && $model->slug === "news") {
                echo 
                    '<span id="left" style="font-weight: 300;">
                        Back
                    </span>
                    <span id="title">
                        News
                    </span>
                    <span id="right" style="font-weight: 300;">
                        Share
                    </span>';
            }
            else {
                echo 
                    '<div id="left" style="height:55%">
                        <div style="background-color:#dddddd;height: 100%; border-radius:50%;display: flex;justify-content: center;align-items: center;">
                            <span class="icon-icon-avatar" style="color:#c0c0c0">
                            </span>
                        </div>
                    </div>
                    <span id="title">
                        <img src="images/logo.png"/>
                        SMELink
                    </span>
                    <span id="right">
                        <i class="fa fa-search" aria-hidden="true" style="font-size: 20px;"></i>
                    </span>';
            }
        ?>
    </div>
    <div id="main-screen">
        <?php
            if (isset($model->type_id)) {
                switch ($model->type_id) {
                    case 0:
                        echo "<img id='preview-banner' src='' style='width:100%'/>";
                        break;
                    case 1:
                        echo "<video id='preview-banner' autoplay loop style='height:100%;position:relative;left:-100%'>
                            <source src=''>
                            Your browser does not support the video tag.
                        </video>";
                        break;
                    default:
                        break;
                }
            }
        ?>
        <div id="top-layer">
            <?php
                switch ($bottom_panel_type) {
                    case 2:
                        echo '<div id="preview-title"></div>
                            <div id="preview-content"></div>';
                        break;
                    
                    default:
                        # code...
                        break;
                }
            ?>
            <!--<div id="preview-title"></div>
            <div id="preview-content"></div>-->
        </div>
    </div>
    <div id="bottom-panel">
        <?php
            switch ($bottom_panel_type) {
                case 1:
                    echo '<div id="tabIcons">
                        <img src="images/icon_home_inactive.png"/>
                        <img src="images/icon_message_active.png"/>
                        <img src="images/icon_card_inactive.png"/>
                        <img src="images/icon_link_inactive.png"/>
                        <img src="images/icon_more_inactive.png"/>
                    </div>';
                    break;
                case 2:
                    echo '<button style="margin-right: 6px;">Join now</button>
                        <button>Login</button>';
                    break;
                default:
                    # code...
                    break;
            }
        ?>
        <!--<button style="margin-right: 6px;">Join now</button>
        <button>Login</button>-->
        <!--<div id="tabIcons">
            <img src="images/icon_home_inactive.png"/>
            <img src="images/icon_message_active.png"/>
            <img src="images/icon_card_inactive.png"/>
            <img src="images/icon_link_inactive.png"/>
            <img src="images/icon_more_inactive.png"/>
        </div>-->
    </div>
</div>
<script type="text/javascript">
    $('#preview-warpper').css({"width": 375 + "px", "height": 667 +"px"});
    $(".fa-circle").css({"font-size": "0.5em", "margin": "0 1px"});
    var header_height = "<?= $header_panel_height ?>";
    var bottom_height = "<?= $bottom_panel_height ?>";
    var statusBar_height = "20px";
    $("#status-bar").css({"height": statusBar_height});
    $("#main-screen").css({"height": "calc(100% - "+statusBar_height+" - "+header_height+" - "+bottom_height+")"});
    $("#header-panel").css({"height": header_height, "padding": "0 "+$("#header-panel").width()*0.035+"px"});
    $('#header-panel #left div').css({"width": $('#header-panel #left div').height()});
    $('#header-panel #left div span').css({"font-size": $('#header-panel #left div').height()*2/3})
    if (header_height === "0%"){
        $("#header-panel").hide();
    }
    $("#bottom-panel").css({"height": bottom_height});
    $("#tabIcons img").css({ "max-height": "50%"});
    // $("#tabIcons img").css({ "max-height": "50%", "width": "31.25px"});
    $("#banners-media").on("change", function(){
        previewMedia($(this).val());
    });
    $("#banners-title").on("keyup", function(){
        previewTitle($(this).val());
    });
    $("#banners-content").on("keyup", function(){
        previewContent($(this).val());
    });
    <?php
        use app\modules\marketing\config\config;
        if (isset($model->type_id)) {
            switch ($model->type_id) {
                case 0:
                    $extensions = Config::getImageExtensions();
                    break;
                case 1:
                    $extensions = Config::getVideoExtensions();
                    break;
                default:
                    # code...
                    break;
            }
        }
        if (isset($model->slug) && $model->slug === "news") {
            $extensions = Config::getImageExtensions();
        }
    ?>
    var extensions = "<?= $extensions ?>";
    function previewMedia(media){
        if (media.trim() !== "") {
            var extension = media.substring(media.lastIndexOf(".")+ 1);
            // console.warn("extension:",extension, extensions);
            if (extension.length < 5 && extensions.indexOf(extension) > -1) {
                $("#preview-banner").show().attr("src", media);
            }
            else {
                $("#preview-banner").hide();
                // console.warn("wrong media type");
            }
        }
        else {
            $("#preview-banner").hide();
        }
    }
    function previewTitle(title){
        $("#preview-title").text(title);
    }
    function previewContent(content){
        $("#preview-content").text(content);
    }
    var initMedia = $("#banners-media").val();
    var initTitle = $("#banners-title").val();
    var initContent = $("#banners-content").val();
    previewMedia(initMedia);
    previewTitle(initTitle);
    previewContent(initContent);
</script>
<style>
    @font-face {
        font-family: icomoon;
        src: url(fonts/icomoon.ttf);
    }
    #preview-warpper {
        font-family: montserrat;
        border: 1px solid #bbb;
        margin: 0 auto;
    }
    #status-bar {
        color: #fff;
        background-color: #757575;
        display: flex;
        align-items: center;
        padding: 0 5px;
        justify-content: space-between;
    }
    #main-screen {
        overflow: hidden;
        position: relative;
    }
    #top-layer {
        width: 100%;
        height: 100%;
        top: 0;
        z-index: 1;
        position: absolute;
    }
    #header-panel {
        display: flex;
        justify-content: space-between;
        align-items: center;
        color: #fff;
        background-color: #f5463d;
    }
    #header-panel #left div {

    }
    #header-panel #title img {
        height: 15px;
    }
    #bottom-panel {
        background-color: #ececec;
        display: flex;
        justify-content: center;
        align-items: center;
    }
    #bottom-panel button {
        background-color: #ef342e;
        border: none;
        color: #fff;
        font-size: 1.1em;
        border-radius: 6px;
        width: 45%;
        height: 50%;
    }
    #tabIcons {
        display: flex;
        justify-content: space-around;
        align-items: center;
        width: 100%;
        height: 100%;
    }
    #tabIcons img {
        max-height: 50%;
    }
    #preview-title, #preview-content {
        color: #fff;
        text-align: center;
    }
    #preview-title {
        position: absolute;
        top: 55%;
        left: 0;
        right: 0;
        margin: auto;
        font-size: 2.2em;
        font-weight: bold;
        line-height: 1.25em;
        padding: 0 10px;
    }
    #preview-content {
        border-top: 1px solid;
        width: 80%;
        position: absolute;
        top: 88%;
        left: 0;
        right: 0;
        margin: auto;
        font-size: 1em;
        line-height: 1.8em;
    }
</style>