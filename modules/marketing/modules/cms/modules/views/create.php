<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Contents */

$title = $slug === "tutorial" ? "how to" : $slug;
$this->title = 'Create '.ucfirst($title);
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'CMS'];
$this->params['breadcrumbs'][] = ['label' => ucfirst($title), 'url' => ['/marketing/cms/'.$title]];
$this->params['breadcrumbs'][] = $this->title;
// echo "<pre>";
//     print_r($model);
// echo "</pre>";
?>
<div class="contents-create">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel panel-default">
        <div class="panel-body">

            <?php
                echo $this->render('../'.preg_replace('/\s+/', '', $title).'/views/default/_form', [
                    'model' => $model,
                    'slug' => $slug
                ]);
            ?>
        </div>
    </div>
</div>