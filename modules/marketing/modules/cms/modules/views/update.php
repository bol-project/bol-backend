<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Contents */

$this->title = 'Update '.ucfirst($model->slug).': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'CMS'];
$this->params['breadcrumbs'][] = ['label' => ucfirst($model->slug), 'url' => ['/marketing/cms/'.$model->slug]];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => (string)$model->_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="contents-update">

    <h1><?= Html::encode($this->title) ?></h1>
    <div class="panel panel-default">
        <div class="panel-body">
            <?php
                $pathname = $model->slug === "tutorial" ? "howto" : $model->slug;
                echo $this->render('../'.$pathname.'/views/default/_form', [
                    'model' => $model
                ]);
            ?>
        </div>
    </div>

</div>
