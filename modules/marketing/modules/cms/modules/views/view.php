<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\MongoDate;
use app\modules\marketing\config\config;
/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Contents */
$slug = $model->slug == "tutorial" ? "how to" : $model->slug;
$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Marketing'];
$this->params['breadcrumbs'][] = ['label' => 'CMS'];
$this->params['breadcrumbs'][] = ['label' => ucfirst($slug), 'url' => ['/marketing/cms/'.preg_replace('/\s+/', '', $slug)]];
// print_r($model->children[0][1]["en"]);
// echo "<br/>";
// print_r($model->children[1][1]["en"]);
// echo "<br/>";
// $final = implode(",<br/> ", array_map(function($v){return $v[1]["en"];},$model->children));
// print_r($final);
// echo "<br/>";

function getAttributes($model) {
    $status = Config::getStatus();
    $attributes = [
        [
            'attribute' => 'title',
            'label' => 'Topic'
        ]
    ];
    if (gettype($model->status) === "integer") {
        array_push($attributes, 
            [
                'attribute' => 'status',
                'format'=>'raw',
                'value'=>ucfirst($status[$model->status])
            ]
        );
    }
    // if ($model->slug == "cardbackground" || $model->slug == "news" || $model->slug == "explore") {
    //     array_push($attribute, 
    //         [
    //             'attribute' => 'status',
    //             'format'=>'html',
    //             'value'=>$model->status ? 
    //                 "<span style='color:green'>enable</span>" 
    //                 : 
    //                 "<span style='color:red'>disable</span>"

    //         ]
    //     );
    // }

    // if ($model->slug == "tutorial" ){
    //     array_push($attribute, 
    //         [
    //             'attribute' => 'status',
    //             'format'=>'html',
    //             'value'=>$model->status ? 
    //                 "<span style='color:green'>enable</span>" 
    //                 : 
    //                 "<span style='color:red'>disable</span>"

    //         ]
    //     );
    // }

    if ($model->slug == "cardbackground" || $model->slug == "news" || $model->slug == "explore") {
        $label = $model->slug === "news" ? "Main Picture" : "Image";
        array_push($attributes, 
            [
                'label' => $label,
                'format'=>'html',
                'value'=>!!$model->image ? 
                    "<img style='width:250px;height:auto;' src='".$model->image."'/>" 
                    : 
                    null
            ]
        );
    }
    if ($model->slug == "news" || $model->slug == "explore") {
        array_push($attributes, 
            [
                'attribute' => 'content',
                'format'=>'raw',
                'value'=>$model->content
            ]
        );
    }

    if ($model->slug === "news") {
        if (!!$model->detail) {
            array_push($attributes, 
                [
                    'label' => 'Brief detail',
                    'value'=>$model->detail
                ]
            );
        }
        if (!!$model->source) {
            array_push($attributes, 
                'source'
            );
        }
        if (!!$model->date) {
            array_push($attributes, 
                [
                    'attribute' => 'date',
                    'format'=>'html',
                    'value'=>Mongodate::mongoDateToString($model->date)
                ]
            );
        }
        if (!!$model->link) {
            array_push($attributes, 
                'link'
            );
        }
    }

    if ($model->slug == "tutorial") {
        array_push($attributes, 
            [
                'attribute' => 'Youtube',
                'format'=>'raw',
                'value'=>$model->slug == "tutorial" ? 
                    "<iframe src='https://www.youtube.com/embed/".$model->content."' frameborder='0' allowfullscreen></iframe>" 
                    : 
                    $model->content
            ]
        );
    }

    if ($model->slug == "explore") {
        array_push($attributes, 
            [
                'attribute' => 'ranking',
                'format'=>'html',
                'value'=>count($model->ranking) > 0 ? 
                    implode(",<br/> ", 
                        array_map(function($v) {
                            return $v[1]["en"];
                        }, $model->ranking))
                    : 
                    null
            ]
        );
    }
    if ($model->slug == "tutorial") {
        array_push($attributes, 
            'views',
            'likes',
            'shares'
        );
    }
    if (!!$model->createdAt) {
        array_push($attributes, 
            [
                'attribute' => 'createdAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->createdAt)
            ],
            'createdBy'
        );
    }
    if (!!$model->updatedAt) {
        array_push($attributes, 
            [
                'attribute' => 'updatedAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->updatedAt)
            ],
            'updatedBy'
        );
    }
    if (!!$model->approvedAt) {
        array_push($attributes, 
            [
                'attribute' => 'approvedAt',
                'format'=>'html',
                'value'=>Mongodate::mongoDateToString($model->approvedAt)
            ],
            'approvedBy'
        );
    }
    return $attributes;
}
?>
<div class="contents-view">

    <h1><?= Html::encode($this->title) ?></h1>
    
    <div class="box box-danger">

    <div class="box-header with-border">

    <p>
        <?= Html::a('Update', ['update', 'id' => (string)$model->_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => (string)$model->_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    </div>

    <div class="box-body">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => getAttributes($model)
    ]) ?>

    </div>

    </div>

</div>
