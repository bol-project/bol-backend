<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\MongoDate;
use app\modules\marketing\config\config;
/* @var $this yii\web\View */
/* @var $searchModel app\modules\marketing\models\ContentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $title;
// $this->params['breadcrumbs'][] = $this->title;
$this->params['breadcrumbs'] = ["Marketing", "CMS", $this->title];
?>
<div class="contents-index">
    <section>
        <h1 style="margin:0 0 10px 0;"><?= Html::encode($this->title) ?></h1>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    </section>
    
    <div class="box box-danger">

    <div class="box-header with-border">
    <p>
        <?= Html::a('Create '.$title, ['create'], ['class' => 'btn btn-success']); ?>
    </p>
    </div>
    
    <div class="box-body">

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'title',
                'label' => 'Topic'
            ],
            [
                'attribute' => 'createdAt',
                'label' => 'Create Date',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return Mongodate::mongoDateToString($model->createdAt, true);
                }
            ],
            [
                'attribute' => 'approvedAt',
                'label' => 'Approve Date',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return Mongodate::mongoDateToString($model->approvedAt, true);
                }
            ],
            [
                'attribute' => 'approvedBy',
                'label' => 'Approve by'
            ],
            [
                'label' => 'Effective From',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return gettype($model->effectiveDate) == "array" ?
                        Mongodate::mongoDateToString($model->effectiveDate['from'], true)
                        :
                        null;
                }
            ],
            [
                'label' => 'Effective To',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    return gettype($model->effectiveDate) == "array" ?
                        Mongodate::mongoDateToString($model->effectiveDate['to'], true)
                        :
                        null;
                }
            ],
            [
                'attribute' => 'status',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                    // return $model->status ? 
                    //         "<span style='color:green'>enable</span>"
                    //         : 
                    //         "<span style='color:red'>disable</span>";
                    if (!!$model->status && gettype($model->status) === "integer") {
                        $status = Config::getStatus();
                        return ucfirst($status[$model->status]);
                    }
                    return null;
                }
            ],
            [
                'attribute' => 'online',
                'format'=>'html',
                'options'=>["style"=>"width:50px"],
                'value'=>function($model, $key, $index, $column){
                    if (gettype($model->online) === "boolean") {
                        return $model->online ? 
                                "<span style='color:green'>Yes</span>"
                                : 
                                "<span style='color:red'>No</span>";
                    }
                    return null;
                }
            ],


            ['class' => 'yii\grid\ActionColumn'],
        ]
    ]); ?>
    </div>
    </div>

</div>
<script type="text/javascript">
    $('.content-header h1').css('font-size','36px');
</script>

