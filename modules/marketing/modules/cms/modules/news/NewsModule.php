<?php

namespace app\modules\marketing\modules\cms\modules\news;

/**
 * news module definition class
 */
class NewsModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\marketing\modules\cms\modules\news\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
