<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use app\models\MongoDate;

/* @var $this yii\web\View */
/* @var $model app\modules\marketing\models\Contents */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="contents-form">

    <?php 
        echo "<strong>Type: </strong>"."News";
        if (!$model->isNewRecord) {
            echo "<strong> Topic: </strong>".$model->title;
        }
        echo "<br>";
        if (!$model->isNewRecord) {
            echo "<strong>Create Date: </strong>".Mongodate::mongoDateToString($model->createdAt, true);
            $createdBy = $model->createdBy;
        }
        else {
            echo "<strong>Create Date: </strong>".date("d/m/Y");
            $createdBy = $_SESSION["user"]["email"];
        }
        echo "<strong> Create by: </strong>".$createdBy;
        echo "<br>";
        if (!!$model->approvedAt) {
            echo "<strong>Approve Date: </strong>".Mongodate::mongoDateToString($model->approvedAt, true);
            echo "<strong> Approve by: </strong>".$model->approvedBy;
            echo "<br>";
        }
        
    ?>
    <hr style="border-color:#888">
    
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?php 
        if (!$model->isNewRecord && $model->status == 2 && in_array($_SESSION["role"], ["manager","admin"])) {
            echo '<button type="submit" id="approveButton" class="btn btn-success">Approve</button> ';
            echo '<button type="submit" id="rejectButton" class="btn btn-danger">Reject</button>';
        }
    ?>
    
    <?= $form->field($model, 'title')->label("Topic") ?>

    <?= $form->field($model, 'detail')->textInput(['placeholder'=>'ส่วนที่จะแสดงในหน้า Homepage ก่อน click เข้าไปดูรายละเอียดของข่าว'])->label("Brief detail") ?>

    <?= $form->field($model, 'image')->textInput(['placeholder'=>'e.g. http://192.168.99.100:5000/web/uploads/gallery/intro_1.jpg'])->label("Main Picture") ?>

    <?= $form->field($model, 'source')->textInput(['placeholder'=>'ที่มาของข่าว']) ?>

    <?php
        echo '
            <div class="field-contents-date" style="display:inline-block;width:20vw">
                <label class="control-label" for="contents-date">Date</label>
                <div style="display:inline-block;width:200px;vertical-align:bottom">
                    <div class="input-group date">
                        <div class="input-group-addon" id="addonDate">
                            <i class="fa fa-calendar"></i>
                        </div>
                        <input placeholder="วันที่ของข่าว" type="text" id="contents-date" class="form-control" name="Contents[date]" style="width:auto">
                    </div>
                </div>
            </div>';
        echo '
            <div class="form-group field-contents-link" style="display:inline-block;width:50vw">
                <label class="control-label" for="contents-link">Link URL</label>
                <input style="display:inline;width:300px" type="text" id="contents-link" class="form-control" name="Contents[link]" placeholder="ที่มาของข่าว e.g. http://www.">
            </div>';
    ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['rows' => 4],
        'preset' => 'full'
    ]) ?>

    <hr style="border-color:#888">
    <!--<div class="col-lg-6">-->
        <?php 
            if ($model->isNewRecord || $model->online) {
                $active1 = "active";
                $active2 = "";
                $checked1 = "checked";
                $checked2 = "";
            }
            else {
                $active1 = "";
                $active2 = "active";
                $checked1 = "";
                $checked2 = "checked";
            }
            
            echo '<strong>Setting :</strong><br>';
            echo '<strong>Effective</strong><br>';
            echo '
                <span class="field-contents-startdate">
                    <label class="control-label" for="contents-startdate">From</label>
                    <div style="display:inline-block;width:200px">
                        <div class="input-group date">
                            <div class="input-group-addon" id="addonStartDate">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" id="contents-startdate" class="form-control" name="Contents[startDate]" style="width:auto">
                        </div>
                    </div>
                </span>';
            echo '
                <span class="field-contents-enddate">
                    <label class="control-label" for="contents-enddate">To</label>
                    <div style="display:inline-block;width:200px">
                        <div class="input-group date">
                            <div class="input-group-addon" id="addonEndDate">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" id="contents-enddate" class="form-control" name="Contents[endDate]" style="width:auto"">
                        </div>
                    </div>
                </span>';
            echo '
                <div class="form-group field-contents-online">
                    <label class="control-label" style="display:block">Online Status</label>
                    <input type="hidden" name="Contents[online]" value="">
                    <div id="contents-online" class="btn-group" data-toggle="buttons">
                        <label class="btn btn-default '.$active1.'" style="color:#000">
                            Yes
                            <input type="radio" name="Contents[online]" value="1" '.$checked1.'>
                        </label>
                        <label class="btn btn-default '.$active2.'" style="color:#000">
                            No
                            <input type="radio" name="Contents[online]" value="0" '.$checked2.'>
                        </label>
                    </div>

                    <div class="help-block"></div>
                </div>';
        ?>

        <div class="form-group">
            <input type="hidden" id="submitType" name="Contents[submitType]" value="">
            <button type="submit" id="saveButton" class="btn btn-primary">Save</button>
            <?php
                $disabled = !$model->isNewRecord && $model->status == 4 ? '' : 'disabled';
                echo '<button type="submit" id="submitButton" class="btn btn-success" '.$disabled.'>Submit</button>';
            ?>
            <?= Html::a('Cancel', ['/marketing/cms/howto'], ['class'=>'btn btn-danger']) ?>
        </div>
    <!--</div>-->
    <!--<div class="col-lg-6" style="border-left:1px dashed #888">
        <?php /* $this->render('..\..\..\views\preview.php', [
            'model' => $model,
            'hide_header' => false
        ])*/ ?>
    </div>-->

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    $(function () {
        <?php
            $startDate = null;
            $endDate = null;
            if (!!$model->effectiveDate) {
                $startDate = !!$model->effectiveDate['from'] ? Mongodate::mongoDateToString($model->effectiveDate['from'], true) : null;
                $endDate = !!$model->effectiveDate['to'] ? Mongodate::mongoDateToString($model->effectiveDate['to'], true) : null;
            }
            echo '$("#contents-startdate").datepicker({format: "dd/mm/yyyy"}).datepicker("setDate", "'.$startDate.'");';
            echo '$("#contents-enddate").datepicker({format: "dd/mm/yyyy"}).datepicker("setDate", "'.$endDate.'");';
            $date = null;
            if (!!$model->date) {
                $date = Mongodate::mongoDateToString($model->date, true);
            }
            echo '$("#contents-date").datepicker({format: "dd/mm/yyyy"}).datepicker("setDate", "'.$date.'");';
        ?>
        $('#addonStartDate').click(function(){
            $('#contents-startdate').datepicker('show');
        });
        $('#addonEndDate').click(function(){
            $('#contents-enddate').datepicker('show');
        });
        $('#saveButton').click(function(){
            $('#submitType').val("save");
        });
        $('#submitButton').click(function(){
            $('#submitType').val("submit");
        });
        $('#approveButton').click(function(){
            $('#submitType').val("approve");
        });
        $('#rejectButton').click(function(){
            $('#submitType').val("reject");
        });
        $('#addonDate').click(function(){
            $('#contents-date').datepicker('show');
        });
    });
</script>