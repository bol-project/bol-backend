<?php

namespace app\modules\marketing\modules\cms\modules\template;

/**
 * template module definition class
 */
class TemplateModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\marketing\modules\cms\modules\template\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
