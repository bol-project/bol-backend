<?php

namespace app\modules\marketing\modules\contact;

/**
 * contact module definition class
 */
class ContactModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\marketing\modules\contact\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
