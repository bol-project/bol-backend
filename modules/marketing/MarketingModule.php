<?php

namespace app\modules\marketing;

session_start();
// $_SESSION["role"] = "member";
//$_SESSION["role"] = "manager";
// $_SESSION["role"] = "admin";
// $_SESSION["user"]["email"] = "kawinc@bol.co.th";
$_SESSION["user"]["email"] = "crism@bol.co.th";

/**
 * marketing module definition class
 */
class MarketingModule extends \yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'app\modules\marketing\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->modules = [
            'cms' => [
                'class' => 'app\modules\marketing\modules\cms\CMSModule',
            ],
            'gallery' => [
                'class' => 'app\modules\marketing\modules\gallery\GalleryModule',
            ],
            'contact' => [
                // 'class' => 'app\modules\marketing\modules\contact\ContactModule',
                'class' => 'app\modules\contactUs_mkt\ContactUs_mktModule',
            ],
        ];
        // custom initialization code goes here
    }
}
