<?php

namespace app\modules\marketing\config;

class Config
{
    public static function getStatus()
    {
        return ['reject', 'approved', 'pending', 'expire', 'created'];
    }

    public static function getImageExtensions()
    {
        return 'png, jpg';
    }

    public static function getVideoExtensions()
    {
        return 'mp4, avi, mov';
    }
}

?>