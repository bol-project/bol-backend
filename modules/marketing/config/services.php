<?php

namespace app\modules\marketing\config;

use app\models\MongoDate;
use yii\validators\Validator;

class Services extends Validator
{
    public static function getEffectiveDate($model)
    {
        $from = str_replace('/', '-', $model->startDate);
        $from = strtotime($from);
        $to = str_replace('/', '-', $model->endDate);
        $to = strtotime($to);
        $effectiveDate = (object) array(
            'from' => $from ? Mongodate::timestampToMongoDate($from): null,
            'to' => $to ? Mongodate::timestampToMongoDate($to): null
        );
        return $effectiveDate;
    }
    
    public static function stingToBoolean($string)
    {
        return $string === '1' ? true : false;
    }

    public static function getStatus($submitType)
    {
        $status = ["reject", "approve", "submit", "expire", "save"];
        return array_search($submitType, $status);
    }

    public static function isDateInRange($startDate, $endDate, $date = null)
    {
        // Convert to timestamp
        $startDate = strtotime(str_replace('/', '-', $startDate));
        $endDate = strtotime(str_replace('/', '-', $endDate));
        if ($date !== null) {
            $date = strtotime($date);
        }
        else {
            $date = strtotime('now');
        }

        //if does not set start or end date
        if ($startDate === false) {
            $startDate = ~PHP_INT_MAX;
        }
        if ($endDate === false) {
            $endDate = PHP_INT_MAX;
        }
        return (($date >= $startDate) && ($date <= $endDate));
    }
}

?>