<?php

$dev = 'http://smelink.animation-genius.com:3002';
$devChat = 'http://smelink.animation-genius.com:3003';

$production = 'https://www.matchlink.asia/v2';
$productionChat = 'http://www.matchlink.asia';

$dev_config = [
        'api' => $dev,
        'apiChat' => $devChat,
        'apiClaim' => $dev.'/api/business/claim/'
];
$production_config = [
        'api' => $production,
        'apiChat' => $devChat,
        'apiClaim' => $production.'/api/business/claim/'
];
// return $dev_config;
return [
    'adminEmail' => 'admin@example.com',
    'config' => $production_config 
];
